/*
 * RestaurantController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import domain.Comment;
import domain.Customer;
import domain.DeliveryMan;
import domain.Dish;
import domain.Evaluator;
import domain.Restaurant;
import forms.EditCustomerForm;
import forms.EditRestaurantForm;
import forms.RegisterRestaurantForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CommentService;
import services.CustomerService;
import services.DeliveryManService;
import services.DishService;
import services.EvaluatorService;
import services.RestaurantService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {

    // Constructors -----------------------------------------------------------
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private DishService dishService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ActorService actorService;
    @Autowired
    private EvaluatorService evaluatorService;
    @Autowired
    private DeliveryManService deliveryManService;

    public ActorController() {
        super();
    }


    @RequestMapping(value = "/restaurant/edit")
    public ModelAndView restaurantEdit() {
        ModelAndView result;
        
        Restaurant restaurant = restaurantService.findOne(actorService.findByPrincipal().getId());
        EditRestaurantForm editRestaurantForm = restaurantService.construct(restaurant);
        
        result = new ModelAndView("restaurant/edit");
        result.addObject("editRestaurantForm", editRestaurantForm);
        result.addObject("requestURI", "/actor/restaurant/save.do");
        return result;
    }

    @RequestMapping(value = "/restaurant/save", method = RequestMethod.POST, params = "save")
    public ModelAndView restaurantSave(@Valid EditRestaurantForm editRestaurantForm,
                             BindingResult result) {
        ModelAndView res = new ModelAndView("restaurant/edit");
        res.addObject("editRestaurantForm", editRestaurantForm);
        if (!result.hasErrors()) {
            try {
                Restaurant restaurant = restaurantService
                        .reconstructEdit(editRestaurantForm);
                restaurantService.save(restaurant);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops) {
            	res = new ModelAndView("redirect:/#");
            }

        }
        return res;
    }
    
    @RequestMapping(value = "/customer/edit")
    public ModelAndView customerEdit() {
        ModelAndView result;
        
        Customer restaurant = customerService.findOne(actorService.findByPrincipal().getId());
        EditCustomerForm editCustomerForm = customerService.construct(restaurant);
        
        result = new ModelAndView("customer/edit");
        result.addObject("editCustomerForm", editCustomerForm);
        result.addObject("action", "actor/customer/save.do");
        return result;
    }

    @RequestMapping(value = "/customer/save", method = RequestMethod.POST, params = "save")
    public ModelAndView customerSave(@Valid EditCustomerForm editCustomerForm,
                             BindingResult result) {
        ModelAndView res = new ModelAndView("customer/edit");
        res.addObject("editCustomerForm", editCustomerForm);
        res.addObject("action", "actor/customer/save.do");
        if (!result.hasErrors()) {
            try {
                Customer customer = customerService
                        .reconstructEdit(editCustomerForm);
                customerService.save(customer);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops) {
            	res = new ModelAndView("redirect:/#");
            }

        }
        return res;
    }
    
    @RequestMapping(value = "/evaluator/edit")
    public ModelAndView evaluatorEdit() {
        ModelAndView result;
        
        Evaluator restaurant = evaluatorService.findOne(actorService.findByPrincipal().getId());
        EditCustomerForm editCustomerForm = evaluatorService.construct(restaurant);
        
        result = new ModelAndView("evaluator/edit");
        result.addObject("editCustomerForm", editCustomerForm);
        result.addObject("action", "actor/evaluator/save.do");
        return result;
    }

    @RequestMapping(value = "/evaluator/save", method = RequestMethod.POST, params = "save")
    public ModelAndView evaluatorSave(@Valid EditCustomerForm editCustomerForm,
                             BindingResult result) {
        ModelAndView res = new ModelAndView("evaluator/edit");
        res.addObject("editCustomerForm", editCustomerForm);
        res.addObject("action", "actor/evaluator/save.do");
        if (!result.hasErrors()) {
            try {
                Evaluator evaluator = evaluatorService
                        .reconstructEdit(editCustomerForm);
                evaluatorService.save(evaluator);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops) {
            	res = new ModelAndView("redirect:/#");
            }

        }
        return res;
    }
    
    @RequestMapping(value = "/deliveryman/edit")
    public ModelAndView deliveryManEdit() {
        ModelAndView result;
        
        DeliveryMan deliveryman = deliveryManService.findOne(actorService.findByPrincipal().getId());
        EditCustomerForm editCustomerForm = deliveryManService.construct(deliveryman);
        
        result = new ModelAndView("delivery/edit");
        result.addObject("editCustomerForm", editCustomerForm);
        result.addObject("action", "actor/deliveryman/save.do");
        return result;
    }

    @RequestMapping(value = "/deliveryman/save", method = RequestMethod.POST, params = "save")
    public ModelAndView deliverymanSave(@Valid EditCustomerForm editCustomerForm,
                             BindingResult result) {
        ModelAndView res = new ModelAndView("delivery/edit");
        res.addObject("editCustomerForm", editCustomerForm);
        res.addObject("action", "actor/deliveryman/save.do");
        if (!result.hasErrors()) {
            try {
                DeliveryMan deliveryman = deliveryManService
                        .reconstructEdit(editCustomerForm);
                deliveryManService.save(deliveryman);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops) {
            	res = new ModelAndView("redirect:/#");
            }

        }
        return res;
    }

}
