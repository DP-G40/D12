/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CustomerService;
import domain.Customer;
import forms.RegisterCustomerForm;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	// Constructors -----------------------------------------------------------
	@Autowired
	private CustomerService customerService;

	public CustomerController() {
		super();
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register() {
		ModelAndView res = new ModelAndView("customer/register");
		RegisterCustomerForm registerCustomerForm = new RegisterCustomerForm();
		res.addObject("registerCustomerForm", registerCustomerForm);
		res.addObject("action","customer/register.do");
		return res;

	}

	@RequestMapping(value = "register", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid RegisterCustomerForm registerCustomerForm,
							 BindingResult result) {
		ModelAndView res = new ModelAndView("customer/register");
		res.addObject("registerCustomerForm", registerCustomerForm);
		res.addObject("action","customer/register.do");
		if (!result.hasErrors()) {
			try {
				Customer customer = customerService.reconstruct(registerCustomerForm);
				customerService.save(customer);
				res = new ModelAndView("redirect:/security/login.do");
			} catch (Throwable oops) {
				if (oops.getMessage().equals("register.error.password")) {
					res.addObject("message", "register.error.password");
				} else if (oops.getMessage().equals("register.error.username")) {
					res.addObject("message", "register.error.username");
				}
			}

		}
		return res;
	}
}
