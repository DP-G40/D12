/*
 * DishController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.DishService;
import domain.Comment;
import domain.Dish;

@Controller
@RequestMapping("/dish")
public class DishController extends AbstractController {

	// Constructors -----------------------------------------------------------
	@Autowired
	private DishService dishService;

	public DishController() {
		super();
	}


	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam Integer dishId) {
		ModelAndView result;
		try {
			result = new ModelAndView("dish/view");

			Dish dish = dishService.findOne(dishId);
			result.addObject("dish", dish);

			Collection<Comment> comments = dish.getComments();
			result.addObject("comments", comments);

		} catch (Throwable oops) {
			result = new ModelAndView("redirect:/#");
		}
		return result;
	}

}
