/*
 * RestaurantController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import domain.*;
import forms.RegisterRestaurantForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import security.UserAccount;
import services.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/restaurant")
public class RestaurantController extends AbstractController {

    // Constructors -----------------------------------------------------------
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private DishService dishService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ActorService actorService;
    
    @Autowired
    private BannerService bannerService;

    @Autowired
    private OrderService orderService;


    public RestaurantController() {
        super();
    }

    @RequestMapping(value="/search",method = RequestMethod.POST, params = "search")
    public ModelAndView searchRestaurant(@RequestParam String keyword){
        ModelAndView res;
        Collection<Restaurant> restaurants;
        restaurants = restaurantService.searchRestaurant(keyword);

        res = new ModelAndView("restaurant/list");
        res.addObject("restaurant", restaurants);
        res.addObject("requestURI", "/restaurant/list.do");

        return res;
    }



    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView result;
        Collection<Restaurant> restaurants = restaurantService.findAllActives();
        result = new ModelAndView("restaurant/list");
        result.addObject("restaurant", restaurants);
        result.addObject("requestURI", "/restaurant/list.do");
        
		try {
			List<Banner> banners = bannerService.getAnnouncements(new Date());
			if(banners.size()>0) {
				int choice =  (int) (Math.random() * banners.size());
				if(choice>banners.size()) {
					choice=banners.size()-1;
				}
				result.addObject("bannerAnnouncement", banners.get(choice));
			}
		}catch(Exception e) {
			
		}
        
        return result;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer restaurantId) {
        ModelAndView result;
        try {
            result = new ModelAndView("restaurant/view");

            Restaurant restaurant = restaurantService.findOne(restaurantId);
            if(restaurant==null){
                restaurant = (Restaurant) actorService.findByUsseAcount(restaurantId);
                restaurantId = restaurant.getId();
            }
            Assert.notNull(restaurant);
            Assert.isTrue(restaurant.getBanned()==false);

            result.addObject("restaurant", restaurant);


            Collection<Dish> dishes = dishService.findbyRestaurant(restaurantId);
            result.addObject("dishes", dishes);
            result.addObject("requestURI", "/dish/list.do");
            
            Collection<Schedule> schedules = scheduleService.findByRestaurant(restaurantId);
            result.addObject("schedules", schedules);

            Collection<Comment> comments = commentService.findCommentforRestaurant(restaurantId);
            result.addObject("comments", comments);

            result.addObject("postals", restaurant.getPostalAddresses());


        } catch (Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        
        try {
			List<Banner> banners = bannerService.getAnnouncements(new Date());
			if(banners.size()>0) {
				int choice =  (int) (Math.random() * banners.size());
				if(choice>banners.size()) {
					choice=banners.size()-1;
				}
				result.addObject("bannerAnnouncement", banners.get(choice));
			}
		}catch(Exception e) {
			
		}
        
        return result;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView register() {
        ModelAndView res = new ModelAndView("restaurant/register");
        RegisterRestaurantForm registerRestaurantForm = new RegisterRestaurantForm();
        res.addObject("registerRestaurantForm", registerRestaurantForm);
        return res;

    }

    @RequestMapping(value = "register", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid RegisterRestaurantForm registerRestaurantForm,
                             BindingResult result) {
        ModelAndView res = new ModelAndView("restaurant/register");
        res.addObject("registerRestaurantForm", registerRestaurantForm);
        if (!result.hasErrors()) {
            try {
                Restaurant restaurant = restaurantService
                        .reconstruct(registerRestaurantForm);
                restaurantService.save(restaurant);
                res = new ModelAndView("redirect:/security/login.do");
            } catch (Throwable oops) {
                if (oops.getMessage().equals("register.error.password")) {
                    res.addObject("message", "register.error.password");
                } else if (oops.getMessage().equals(
                        "register.error.restaurantname")) {
                    res.addObject("message", "register.error.restaurantname");
                }
            }

        }
        return res;
    }

}
