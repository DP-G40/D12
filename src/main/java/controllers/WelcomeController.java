/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Offer;
import domain.Video;
import services.OfferService;
import services.VideoService;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public WelcomeController() {
		super();
	}
	
	@Autowired
    private VideoService videoService;
	
	@Autowired
    private OfferService offerService;

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/index")
	public ModelAndView index(@RequestParam(required = false, defaultValue = "John Doe") final String name) {
		ModelAndView result;
		SimpleDateFormat formatter;
		String moment;

		formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		moment = formatter.format(new Date());

		result = new ModelAndView("welcome/index");
		result.addObject("name", name);
		result.addObject("moment", moment);
		
		Collection<Video> videoC = videoService.getByDates(new Date());
		List<Video> videos = new ArrayList<Video>();
		videos.addAll(videoC);
		
		String youtubeURL = "";
		try {
			if(videos.size()>0) {
				int choice =  (int) (Math.random() * videos.size());
				if(choice>videos.size()) {
					choice=videos.size()-1;
				}
				youtubeURL = videos.get(choice).getUrl();
			}
			result.addObject("youtubeURL", "https://www.youtube.com/embed/"+youtubeURL.split("v=")[1]);
		}catch(Exception e) {
			
		}
		
		try {
			Collection<Offer> offersActives = offerService.getPromotionsActive();
			result.addObject("offersActives", offersActives);
		}catch(Exception e) {
			
		}

		return result;
	}
	
	
}
