/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Calendar;
import java.util.Date;

import javax.validation.Valid;

import domain.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.ConfigurationService;
import services.OfferService;
import services.RestaurantService;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private AdministratorService	administratorService;
	
	@Autowired
	private ConfigurationService	configurationService;
	
	@Autowired
	private OfferService	offerService;
	
	@Autowired
	private RestaurantService	restaurantService;

	//DASHBOARD
	
	@RequestMapping("/dashboard")
	public ModelAndView dashboard(){
		
		ModelAndView result;
		result = new ModelAndView("administrator/dashboard");
		
		//fecha mes anterior
		Date[] fechas = this.getLastAndFirstDateLastMonth();
		
		result.addObject("Q1", administratorService.Q1());
		result.addObject("Q2", administratorService.Q2(new PageRequest(0, 5)));
		result.addObject("Q3", administratorService.Q3(new PageRequest(0, 5)));
		result.addObject("Q4", administratorService.Q4(new PageRequest(0, 5)));
		result.addObject("Q5", administratorService.Q5());
		result.addObject("Q6", administratorService.Q6());
		result.addObject("Q7", administratorService.Q7());
		result.addObject("Q7_2", administratorService.Q7_2());
		result.addObject("Q8", administratorService.Q8());
		result.addObject("Q7_2", administratorService.Q8_2());
		result.addObject("Q9", administratorService.Q9(new PageRequest(0, 1)));
		result.addObject("Q10", administratorService.Q10(fechas[0], fechas[1], new PageRequest(0, 1)));
		result.addObject("Q11", administratorService.Q11(fechas[0], fechas[1], new PageRequest(0, 1)));
		result.addObject("Q12", administratorService.Q12(fechas[0], fechas[1], new PageRequest(0, 1)));

		
		return result;
	}
	
	@RequestMapping("/controlPanel")
	public ModelAndView controlPanel(){
		
		ModelAndView result;
		result = new ModelAndView("administrator/controlPanel");
		
		result.addObject("configuration", configurationService.findAll().iterator().next());
		result.addObject("restaurantsBanned", restaurantService.getRestaurantsBanned());
		result.addObject("offers", offerService.getPromotions());
		result.addObject("activepromotions", offerService.getPromotionsActive());
		Object[] restaurantsAndAverages = restaurantService.getRestaurantsAndMedia();
		result.addObject("restaurantsNotBanned", restaurantsAndAverages[0]);
		result.addObject("restaurantsAverages", restaurantsAndAverages[1]);
		
		return result;
	}
	
	@RequestMapping(value = "/saveConfig",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid Configuration configuration, BindingResult result){
        ModelAndView res = new ModelAndView("administrator/controlPanel");
        if(!result.hasErrors()){
            try{
            	configurationService.save(configuration);
            	return this.controlPanel();
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
            res.addObject("configuration", configuration);
            res.addObject("restaurantsBanned", restaurantService.getRestaurantsBanned());
            res.addObject("offers", offerService.getPromotions());
            Object[] restaurantsAndAverages = restaurantService.getRestaurantsAndMedia();
    		res.addObject("restaurantsNotBanned", restaurantsAndAverages[0]);
    		res.addObject("restaurantsAverages", restaurantsAndAverages[1]);
        }
        return res;
    }
	
	private Date[] getLastAndFirstDateLastMonth(){
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		Date firstDateOfPreviousMonth = aCalendar.getTime();

		// set actual maximum date of previous month
		aCalendar.set(Calendar.DATE,     aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//read it
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		
		Date[] res = new Date[]{firstDateOfPreviousMonth,lastDateOfPreviousMonth};
		return res;
	}

}
