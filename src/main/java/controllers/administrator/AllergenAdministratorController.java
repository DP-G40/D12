/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import javax.validation.Valid;

import domain.Allergen;
import domain.Configuration;
import domain.Restaurant;
import domain.Template;
import forms.OfferForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.AllegenService;
import services.ConfigurationService;
import services.RestaurantService;
import services.TemplateService;

@Controller
@RequestMapping("/allergen/administrator")
public class AllergenAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AllergenAdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private AllegenService	allergenService;

	@RequestMapping(value = "/all")
    public ModelAndView all() {
		ModelAndView res = new ModelAndView("allergen/all");
		
		res.addObject("allergens", allergenService.findAll());
		res.addObject("allergenNew", allergenService.create());
		
		return res;
    }
	
	@RequestMapping(value = "/save",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid @ModelAttribute("allergen") Allergen allergenEdit, BindingResult result){
        ModelAndView res = new ModelAndView("allergen/all");
        if(!result.hasErrors()){
            try{
            	Allergen allergenBD = allergenService.findOne(allergenEdit.getId());
            	if(allergenBD.getDishes().size()==0) {
            		allergenService.save(allergenEdit);
            		res = new ModelAndView("redirect:/allergen/administrator/all.do");
            	}else {
            		throw new IllegalArgumentException();
            	}
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res.clear();
        	res = new ModelAndView("allergen/all");
        	res.addObject("allergens", allergenService.findAll());
        	res.addObject("allergenNew", allergenService.create());
        	String message = "";
        	for(ObjectError e: result.getAllErrors()) {
        		FieldError fe = (FieldError) e;
        		message = message + fe.getField() + ": ";
        		message = message + e.getDefaultMessage() + ". <br>";
        	}
        	res.addObject("errorMessage", message);
        }
        return res;
    }
	
	@RequestMapping(value = "/saveNew",method = RequestMethod.POST,params = "save")
    public ModelAndView saveNew(@Valid @ModelAttribute("allergenNew") Allergen allergenNew, BindingResult result){
        ModelAndView res = new ModelAndView("allergen/all");
        if(!result.hasErrors()){
            try{
            	allergenService.save(allergenNew);
            	res = new ModelAndView("redirect:/allergen/administrator/all.do");
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("allergen/all");
        	res.addObject("allergens", allergenService.findAll());
        	res.addObject("allergenNew", allergenNew);
        }
        return res;
    }
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView ban(@RequestParam Integer allegenId) {
        ModelAndView res;
        res = new ModelAndView("redirect:/allergen/administrator/all.do");
        
        try{
        	Allergen allergen = allergenService.findOne(allegenId);
        	if(allergen.getDishes().size()==0) {
        		allergenService.delete(allergen);
        	}else {
        		throw new IllegalArgumentException();
        	}
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }
	
}
