/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Calendar;
import java.util.Date;

import javax.validation.Valid;

import domain.Configuration;
import domain.Restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.BillService;
import services.ConfigurationService;
import services.RestaurantService;

@Controller
@RequestMapping("/bill/administrator")
public class BillAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public BillAdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private BillService	billService;

	@RequestMapping(value = "/generate")
    public ModelAndView ban() {
        ModelAndView res;
        res = new ModelAndView("redirect:/administrator/controlPanel.do");
        
        try{
        	Date[] fechas = this.getLastAndFirstDateLastMonth();
        	if(billService.getBillByDates(fechas[0], fechas[1]).size()==0) {
	        	billService.generateBill(fechas[0],fechas[1]);
        	}else {
        		res.addObject("message", "administrator.controlPanel.alreadyGenerateBill");
        	}
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/administrator/controlPanel.do");
        }

        return res;
    }
	
	private Date[] getLastAndFirstDateLastMonth(){
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		Date firstDateOfPreviousMonth = aCalendar.getTime();

		// set actual maximum date of previous month
		aCalendar.set(Calendar.DATE,     aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//read it
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		
		Date[] res = new Date[]{firstDateOfPreviousMonth,lastDateOfPreviousMonth};
		return res;
	}
	
}
