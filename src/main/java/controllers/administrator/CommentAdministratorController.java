/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import javax.validation.Valid;

import domain.Allergen;
import domain.Comment;
import domain.Configuration;
import domain.Restaurant;
import domain.Template;
import forms.OfferForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.AllegenService;
import services.CommentService;
import services.ConfigurationService;
import services.RestaurantService;
import services.TemplateService;

@Controller
@RequestMapping("/comment/administrator")
public class CommentAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public CommentAdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private CommentService	commentService;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
    public ModelAndView all(@RequestParam Integer restaurantId) {
		ModelAndView res = new ModelAndView("comment/all");
		
		res.addObject("comments", commentService.findByRestaurant(restaurantId));
		
		return res;
    }
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView ban(@RequestParam Integer commentId) {
        ModelAndView res;
        
        Comment comment = commentService.findOne(commentId);
        res = new ModelAndView("redirect:/comment/administrator/all.do?commentId="+comment.getCommentable().getId());
        
        try{
        	commentService.delete(comment);
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }
	
}
