package controllers.administrator;

import controllers.AbstractController;
import domain.Evaluator;
import domain.Review;
import forms.RegisterCustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.EvaluatorService;
import services.ReviewService;

import javax.validation.Valid;

@Controller
@RequestMapping("/evaluator/administrator")
public class EvaluatorAdministratorController extends AbstractController {

    @Autowired
    private EvaluatorService evaluatorService;
    
    @Autowired
    private ReviewService reviewService;

    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public ModelAndView register(){
        ModelAndView res = new ModelAndView("evaluator/register");
        try {
            RegisterCustomerForm registerCustomerForm = new RegisterCustomerForm();
            res.addObject("registerCustomerForm", registerCustomerForm);
            res.addObject("action", "evaluator/administrator/register.do");
        } catch(Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


    @RequestMapping(value = "/register",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid RegisterCustomerForm registerCustomerForm, BindingResult result){
        ModelAndView res = new ModelAndView("evaluator/register");
        res.addObject("registerCustomerForm", registerCustomerForm);
        res.addObject("action", "evaluator/administrator/register.do");
        if (!result.hasErrors()){
            try {
                Evaluator evaluator = evaluatorService.reconstruct(registerCustomerForm);
                evaluatorService.save(evaluator);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops){
                if (oops.getMessage().equals("register.error.password")) {
                    res.addObject("message", "register.error.password");
                } else if (oops.getMessage().equals("register.error.username")) {
                    res.addObject("message", "register.error.username");
                }
            }
        }
        return res;

    }
    
    @RequestMapping(value = "/list")
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("evaluator/list");
        try {
            res.addObject("actors", evaluatorService.findAll());
            res.addObject("requestUri", "evaluator/administrator/list.do");
        } catch(Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
    
    @RequestMapping(value = "/view",method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer evaluatorId){
        ModelAndView res = new ModelAndView("evaluator/view");
        try {
            Evaluator x = evaluatorService.findOne(evaluatorId);
            Assert.notNull(x);
            res.addObject("actors", x);
            res.addObject("requestUri", "evaluator/administrator/view.do");
            res.addObject("reviews", reviewService.findByEvaluatorId(evaluatorId));
        } catch(Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
    
    @RequestMapping(value = "/adminDeleteeleteReview",method = RequestMethod.GET)
    public ModelAndView deleteReview(@RequestParam Integer reviewId){
        ModelAndView res = new ModelAndView("evaluator/view");
        try {
        	Review review = reviewService.findOne(reviewId);
        	Integer evaluatorId = review.getEvaluator().getId();
        	reviewService.delete(review);
            res.addObject("redirect:/evaluator/administrator/view?evaluatorId"+evaluatorId);
        } catch(Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
    
}