/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import javax.validation.Valid;

import domain.Configuration;
import domain.Restaurant;
import forms.OfferForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.ConfigurationService;
import services.OfferService;
import services.RestaurantService;

@Controller
@RequestMapping("/promotion/administrator")
public class PromotionAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public PromotionAdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private AdministratorService	administratorService;
	
	@Autowired
	private ConfigurationService	configurationService;
	
	@Autowired
	private OfferService	offerService;
	
	@Autowired
	private RestaurantService	restaurantService;;

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView ban(@RequestParam String promotion) {
        ModelAndView res;
        res = new ModelAndView("redirect:/administrator/controlPanel.do");
        
        try{
        	offerService.deletePromotion(promotion);
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }
	
	@RequestMapping(value = "/create")
    public ModelAndView create() {
        ModelAndView res;
        
        res = new ModelAndView("offer/createPromotion");
        res.addObject("offer", new OfferForm());
        res.addObject("actionURL", "promotion/administrator/savePromotion.do");

        return res;
    }
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam String promotion) {
        ModelAndView res;
        
        res = new ModelAndView("offer/createPromotion");
        res.addObject("offer", offerService.reconstructFormAdmin(offerService.getPromotionById(promotion)));
        res.addObject("actionURL", "promotion/administrator/saveEditPromotion.do");

        return res;
    }
	
	@RequestMapping(value = "/savePromotion",method = RequestMethod.POST,params = "save")
    public ModelAndView savePromotion(@Valid @ModelAttribute("offer") OfferForm offer, BindingResult result){
        ModelAndView res = new ModelAndView("administrator/controlPanel");
        if(!result.hasErrors()){
            try{
                Assert.isTrue(offer.getStartDate().before(offer.getEndDate()),"offer.error.date");
            	offerService.savePromotion(offer);
            	res = new ModelAndView("redirect:/administrator/controlPanel.do");
            } catch (Throwable oops){
            	if (oops.getMessage().equals("offer.error.date")){
            	    res.addObject("message","offer.error.date");
                }
            }
        }else{
        	res = new ModelAndView("offer/createPromotion");
        	res.addObject("offer", offer);
        	res.addObject("actionURL", "promotion/administrator/savePromotion.do");
        }
        return res;
    }
	
	@RequestMapping(value = "/saveEditPromotion",method = RequestMethod.POST,params = "save")
    public ModelAndView saveEditPromotion(@Valid @ModelAttribute("offer") OfferForm offer, BindingResult result){
        ModelAndView res = new ModelAndView("administrator/controlPanel");
        if(!result.hasErrors()){
            try{
            	offerService.saveEditPromotion(offer);
            	res = new ModelAndView("redirect:/administrator/controlPanel.do");
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("offer/createPromotion");
        	res.addObject("offer", offer);
        	res.addObject("actionURL", "promotion/administrator/saveEditPromotion.do");
        }
        return res;
    }
	
}
