/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import javax.validation.Valid;

import domain.Configuration;
import domain.Restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.ConfigurationService;
import services.RestaurantService;

@Controller
@RequestMapping("/restaurant/administrator")
public class RestaurantAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public RestaurantAdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private RestaurantService	restaurantService;

	@RequestMapping(value = "/ban", method = RequestMethod.GET)
    public ModelAndView ban(@RequestParam int restaurantId) {
        ModelAndView res;
        res = new ModelAndView("redirect:/administrator/controlPanel.do");
        
        try{
        	restaurantService.banRestaurant(restaurantId);
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/administrator/controlPanel.do");
        }

        return res;
    }
	
	@RequestMapping(value = "/unban", method = RequestMethod.GET)
    public ModelAndView unban(@RequestParam int restaurantId) {
        ModelAndView res;
        res = new ModelAndView("redirect:/administrator/controlPanel.do");
        
        try{
            restaurantService.unbanRestaurant(restaurantId);
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/administrator/controlPanel.do");
        }

        return res;
    }
	
}
