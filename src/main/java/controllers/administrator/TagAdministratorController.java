package controllers.administrator;

import controllers.AbstractController;
import domain.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.TagService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/tag/administrator")
public class TagAdministratorController extends AbstractController {
	
	@Autowired
	private TagService tagService;

	public TagAdministratorController(){
		super();
	}

	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView res;
		Collection<Tag> tags;
		
		tags = tagService.findAll();
		res = new ModelAndView("tag/list");
		res.addObject("tags", tags);
		res.addObject("requestURI", "/tag/administrator/list.do");
		
		return res;
	}


	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res = new ModelAndView("tag/create");
		Tag t = tagService.create();
		res.addObject("tag", t);
		res.addObject("action", "tag/administrator/create.do");
		return res;
	}

	@RequestMapping(value="/edit", method=RequestMethod.GET)
	public ModelAndView edit(@RequestParam Integer tagId) {
		ModelAndView res = new ModelAndView("tag/edit");
		try {
			Tag t = tagService.findOne(tagId);
			res.addObject("tag", t);
			res.addObject("action", "tag/administrator/edit.do?tagId="+tagId);
		}catch(Throwable oops){
			res = new ModelAndView("redirect:/#");
		}
		return res;
	}




	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	private ModelAndView save(@Valid Tag tag, BindingResult result){
		ModelAndView res = new ModelAndView("tag/create");
		if(result.hasErrors()){
			res.addObject("tag", tag);
		}else {
			try{
				tagService.save(tag);
				res = new ModelAndView("redirect:/tag/administrator/list.do");
			}catch (Throwable oops){
				if(oops.getMessage().equalsIgnoreCase("tag.name.error")){
					res.addObject("message", "tag.name.error");
				} else {
					res.addObject("message", "commit.error");
				}
				res.addObject("tag", tag);

			}
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	private ModelAndView saveEdit(@Valid Tag tag, BindingResult result){
		ModelAndView res = new ModelAndView("tag/edit");
		if(result.hasErrors()){
			res.addObject("tag", tag);
		}else {
			try{
				tagService.save(tag);
				res = new ModelAndView("redirect:/tag/administrator/list.do");
			}catch (Throwable oops){
				if(oops.getMessage().equalsIgnoreCase("tag.name.error")){
					res.addObject("message", "tag.name.error");
				} else {
					res.addObject("message", "commit.error");
				}
				res.addObject("tag", tag);
			}
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	private ModelAndView delete(Tag tag, BindingResult result){
		ModelAndView res = new ModelAndView("tag/edit");
		if(result.hasErrors()){
			res.addObject("tag", tag);
		}else {
			try{
				tagService.delete(tag);
				res = new ModelAndView("redirect:/tag/administrator/list.do");
			}catch (Throwable oops){
				res.addObject("tag", tag);
				res.addObject("message", "commit.error");
			}
		}
		return res;
	}




	public ModelAndView createModelAndView(Tag tag) {
		ModelAndView res;

		res = createModelAndView(tag, null);

		return res;

	}

	private ModelAndView createModelAndView(Tag tag, String messageCode) {

		ModelAndView result;
		Date moment = new Date();
		result = new ModelAndView("tag/create");
		result.addObject("tag", tag);
		result.addObject("message", messageCode);
		result.addObject("moment", moment);

		return result;

	}

	public ModelAndView editModelAndView(Tag tag) {
		ModelAndView res;

		res = createModelAndView(tag, null);

		return res;

	}

	private ModelAndView editModelAndView(Tag tag, String messageCode) {

		ModelAndView result;

		result = new ModelAndView("tag/edit");
		result.addObject("survivalclass", tag);

		result.addObject("message", messageCode);

		return result;

	}
}
