/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import javax.validation.Valid;

import domain.Configuration;
import domain.Restaurant;
import domain.Template;
import forms.OfferForm;

import forms.TemplateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AdministratorService;
import services.ConfigurationService;
import services.RestaurantService;
import services.TemplateService;

import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/template/administrator")
public class TemplateAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public TemplateAdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private TemplateService	templateService;

	@RequestMapping(value = "/all")
    public ModelAndView all() {
		ModelAndView res = new ModelAndView("template/all");
		
		res.addObject("templates", templateService.findAll());
		
		return res;
    }
	
	@RequestMapping(value = "/save",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid @ModelAttribute("temlate") Template temlate, BindingResult result){
        ModelAndView res = new ModelAndView("template/all");
        if(!result.hasErrors()){
            try{
            	templateService.save(temlate);
            	res = new ModelAndView("redirect:/template/administrator/all.do");
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("template/all");
        	res.addObject("templates", templateService.findAll());
        	String message = "";
        	for(ObjectError e: result.getAllErrors()) {
        		FieldError fe = (FieldError) e;
        		message = message + fe.getField() + ": ";
        		message = message + e.getDefaultMessage() + ". <br>";
        	}
        	res.addObject("errorMessage", message);
        }
        return res;
    }

	@RequestMapping(value = "/activateTemplate", method = RequestMethod.GET)
	public ModelAndView activateTemplate(@RequestParam int templateId) {
		ModelAndView res = new ModelAndView("template/all");

		try {
			Template t = templateService.findOne(templateId);
			String tipe = t.getType();
			Template t2 = templateService.getTemplateActiveByType(tipe);

			t2.setActive(false);
			t.setActive(true);
			templateService.save(t2);
			templateService.save(t);

		}catch (Throwable oops){
			res = new ModelAndView("redirect:#");
		}

		res.addObject("templates", templateService.findAll());

		return res;
	}


	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res = new ModelAndView("template/create");
		TemplateForm form = new TemplateForm();
		res = new ModelAndView("template/create");
		res.addObject("templateForm", form);
		res.addObject("action", "template/administrator/create.do");

		return res;
	}

	@RequestMapping(value="/create", method=RequestMethod.POST, params = "save")
	public ModelAndView savecreate(@Valid TemplateForm templateForm, final BindingResult bindingResult) {

    	ModelAndView res;

    	if(bindingResult.hasErrors()){
			res = new ModelAndView("template/create");
			res.addObject("templateForm", templateForm);
			res.addObject("action", "template/administrator/create.do");
		}else {
    		try {
    			Template t = templateService.reconstruct(templateForm);
    			templateService.save(t);
    			res = all();
			}catch (Throwable oops){
				res = new ModelAndView("template/create");
				res.addObject("templateForm", templateForm);
				res.addObject("action", "template/administrator/create.do");
				res.addObject("message", "message.commit.error");
			}
		}
		return res;
	}
}
