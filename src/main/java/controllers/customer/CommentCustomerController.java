package controllers.customer;

import controllers.AbstractController;
import forms.CommentForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CommentService;
import services.OrderService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/comment/customer")
public class CommentCustomerController extends AbstractController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/comment", method = RequestMethod.GET)
    public ModelAndView comment(@RequestParam Integer commentableId) {
        ModelAndView res = new ModelAndView("comment/create");
        try {
            CommentForm commentForm = new CommentForm();
            commentForm.setCommentableId(commentableId);
            res.addObject("commentForm", commentForm);
            res.addObject("action", "comment/customer/comment.do");
            if (orderService.findOrderByCommentable(commentableId).isEmpty()){
                res.addObject("read",true);
                res.addObject("message","order.comment.error");
            } else {
                res.addObject("read",false);

            }
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST, params = "save")
    public ModelAndView saveComment(@Valid CommentForm commentForm, BindingResult result) {
        ModelAndView res = new ModelAndView("comment/create");
        res.addObject("commentForm", commentForm);
        res.addObject("action", "comment/customer/comment.do");
        if (!result.hasErrors()) {
            try {
                commentService.reconstruct(commentForm);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops) {

            }
        }
        return res;
    }
}
