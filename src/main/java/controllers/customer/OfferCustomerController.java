package controllers.customer;

import controllers.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.OfferService;

@Controller
@RequestMapping("/offer/customer")
public class OfferCustomerController extends AbstractController {

    @Autowired
    private OfferService offerService;

    @RequestMapping(value = "/listPromotion" , method = RequestMethod.GET)
    public ModelAndView listPromotion(){
        ModelAndView res = new ModelAndView("promotion/list");
        res.addObject("offers",offerService.getPromotionsActive());
        res.addObject("requestURI","/offer/customer/listPromotion");
        return res;
    }
}
