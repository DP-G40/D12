package controllers.customer;

import controllers.AbstractController;
import domain.Dish;
import domain.Order;
import forms.ConfirmOrderForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.DishService;
import services.OrderService;

import javax.validation.Valid;

@Controller
@RequestMapping("/order/customer/")
public class OrderCustomerController extends AbstractController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private DishService dishService;


    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView add(@RequestParam Integer dishId) {
        ModelAndView res;
        try {
            orderService.addDishToOrder(dishId);
            Dish dish = dishService.findOne(dishId);
            res = new ModelAndView("redirect:/restaurant/view.do?restaurantId="+dish.getRestaurant().getId());
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("order/list");
        try {
            res.addObject("drafts", orderService.getOrdersByReviewerAndStatus("DRAFT"));
            res.addObject("pendings", orderService.getOrdersByReviewerAndStatus("PENDING"));
            res.addObject("accepteds", orderService.getOrdersByReviewerAndStatus("ACCEPTED"));
            res.addObject("deliverings", orderService.getOrdersByReviewerAndStatus("DELIVERING"));
            res.addObject("completeds", orderService.getOrdersByReviewerAndStatus("COMPLETED"));
            res.addObject("rejecteds", orderService.getOrdersByReviewerAndStatus("REJECTED"));
            res.addObject("requestURI","order/customer/list.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer orderId){
        ModelAndView res = new ModelAndView("order/view");
        try {
            Order order = orderService.findOne(orderId);
            orderService.isMine(order);
            res.addObject("order",order);
            res.addObject("back","order/customer/list.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public ModelAndView confirm(@RequestParam Integer orderId) {
        ModelAndView res;
        try {
            orderService.confirm(orderId);
            res = new ModelAndView("order/confirm");
            ConfirmOrderForm confirmOrderForm = new ConfirmOrderForm();
            confirmOrderForm.setOrderId(orderId);
            res.addObject("confirmOrderForm", confirmOrderForm);
            res.addObject("action","order/customer/confirm.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST, params = "save")
    public ModelAndView saveConfirm(@Valid ConfirmOrderForm confirmOrderForm, BindingResult result) {
        ModelAndView res = new ModelAndView("order/confirm");
        res.addObject("confirmOrderForm", confirmOrderForm);
        if (!result.hasErrors()) {
            try {
                Order order = orderService.reconstruct(confirmOrderForm);
                orderService.calcularPrecioOrder(order);
                orderService.save(order);
                res = new ModelAndView("redirect:/order/customer/list.do");
            } catch (Throwable oops) {
                if (oops.getMessage().equals("creditcard.date.error")){
                    res.addObject("message","creditcard.date.error");
                }
                if (oops.getMessage().equals("order.postal.error")){
                    res.addObject("message","order.postal.error");
                }
                if (oops.getMessage().equals("order.offer.error")){
                    res.addObject("message","order.offer.error");
                }
            }
        }
        return res;

    }

    @RequestMapping(value = "/cancel",method = RequestMethod.GET)
    public ModelAndView cancel(@RequestParam Integer orderId){
        ModelAndView res = new ModelAndView("redirect:/order/customer/list.do");
        try{
            orderService.cancel(orderId);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public ModelAndView remove(@RequestParam Integer dishId, @RequestParam Integer orderId) {
        ModelAndView res;
        try {
            orderService.removeDishToOrder(dishId,orderId);
            Order order = orderService.findOne(orderId);
            if (order.getDishes().isEmpty()){
                res = new ModelAndView("redirect:/order/customer/list.do");
            } else {
                res = new ModelAndView("redirect:/order/customer/view.do?orderId=" + orderId);
            }
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


}
