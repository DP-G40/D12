/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.deliveryMan;

import javax.validation.Valid;

import domain.Allergen;
import domain.Configuration;
import domain.DeliveryMan;
import domain.Order;
import domain.Restaurant;
import domain.Template;
import forms.OfferForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import services.ActorService;
import services.AdministratorService;
import services.AllegenService;
import services.ConfigurationService;
import services.DeliveryManService;
import services.OrderService;
import services.RestaurantService;
import services.TemplateService;

@Controller
@RequestMapping("/order/deliveryMan")
public class OrderDeliveryManController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public OrderDeliveryManController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private OrderService	orderService;
	
	@Autowired
	private DeliveryManService	deliveryManService;
	
	@Autowired
	private ActorService	actorService;

	@RequestMapping(value = "/allOrders")
    public ModelAndView all() {
		ModelAndView res = new ModelAndView("deliveryMan/allOrders");
		
		DeliveryMan deliveryMan = deliveryManService.findOne(actorService.findByPrincipal().getId());
		
		res.addObject("allAcceptedOrders", orderService.getOrdersByRestaurantAndStatus(deliveryMan.getRestaurant().getId(), "READY"));
		res.addObject("myDealsOrder", orderService.getOrdersByDeliveryManAndStatus(deliveryMan.getId(), "DELIVERING"));
		res.addObject("myClompletedDealsOrder", orderService.getOrdersByDeliveryManAndStatus(deliveryMan.getId(), "COMPLETED"));
		
		return res;
    }
	

	@RequestMapping(value = "/assign", method = RequestMethod.GET)
    public ModelAndView assign(@RequestParam Integer orderId) {
        ModelAndView res;

        
        try{
        	
        	DeliveryMan deliveryMan = deliveryManService.findOne(actorService.findByPrincipal().getId());

        	if(orderService.getOrdersByDeliveryManAndStatus(deliveryMan.getId(), "DELIVERING").size()==0) {
        		Order o = orderService.findOne(orderId);
        		o.setDeliveryMan(deliveryMan);
				o.setStatus("DELIVERING");
        		orderService.save(o);
				if(o.getReviewer().getEmail() != null || o.getReviewer().getEmail() !="" || !o.getReviewer().getEmail().contains("@") ){
					orderService.correoDelivering(o);
				}
				res = all();
        	}else {
				res = all();
        		res.addObject("message", "deliveryMan.deals.onlyOne");
        	}
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }
	
	@RequestMapping(value = "/complete", method = RequestMethod.GET)
    public ModelAndView complete(@RequestParam Integer orderId) {
        ModelAndView res;
        
        try{
        	
        	DeliveryMan deliveryMan = deliveryManService.findOne(actorService.findByPrincipal().getId());
        	Order o = orderService.findOne(orderId);

        	Assert.isTrue(o.getDeliveryMan().getId()==deliveryMan.getId());
			o.setStatus("COMPLETED");
			orderService.save(o);
			if(o.getReviewer().getEmail() != null || o.getReviewer().getEmail() !="" || !o.getReviewer().getEmail().contains("@") ){
				orderService.correoCompletado(o);
			}
			res = all();

        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam Integer orderId){
		ModelAndView res = new ModelAndView("order/view");
		try {
			Order order = orderService.findOne(orderId);
			orderService.isMineDeliveryMan(order);
			res.addObject("order",order);
			res.addObject("back","order/deliveryman/list.do");
		} catch (Throwable oops){
			res = new ModelAndView("redirect:/#");
		}
		return res;
	}
	
}
