package controllers.evaluator;

import controllers.AbstractController;
import domain.Order;
import domain.Review;
import forms.ReviewForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.OrderService;
import services.ReviewService;

import javax.validation.Valid;

@Controller
@RequestMapping("/review/evaluator/")
public class ReviewEvaluatorController extends AbstractController {

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public ModelAndView createReview(){
        ModelAndView res = new ModelAndView("review/create");
        try {
            ReviewForm reviewForm = new ReviewForm();
            res.addObject("reviewForm",reviewForm);
            res.addObject("orders",orderService.findByEvaluatorWithNoReview());
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "create",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid ReviewForm reviewForm, BindingResult result){
        ModelAndView res = new ModelAndView("review/create");
        res.addObject("reviewForm",reviewForm);
        res.addObject("orders",orderService.findByEvaluatorWithNoReview());
        if (!result.hasErrors()){
            try {
                Review review = reviewService.reconstruct(reviewForm);
                reviewService.save(review);
                res = new ModelAndView("redirect:/review/evaluator/list.do");
            } catch (Throwable oops){
                System.out.println(oops);
            }
        }
        return res;
    }

    @RequestMapping(value = "list",method = RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("review/list");
        try {
            res.addObject("reviews",reviewService.findByEvaluator());
            res.addObject("requestURI","/review/evaluator/list.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
}
