package controllers.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import domain.Dish;
import domain.Restaurant;
import services.RestaurantService;

@RestController()
@RequestMapping("/rest")
public class DishRestController {
	
	@Autowired
    private RestaurantService restaurantService;
	
	@RequestMapping(value="/getRestaurantDishesJSON", produces="application/json; charset=UTF-8",
		    method=RequestMethod.GET)
    public @ResponseBody Collection<Dish> getRestaurantJSON(@RequestParam Integer restaurantId) {
        return restaurantService.findOne(restaurantId).getDishes();
    }
	
	@RequestMapping(value="/getRestaurantDishesXML",produces=MediaType.APPLICATION_XML_VALUE,
		    method=RequestMethod.GET)
    public @ResponseBody ListDish getRestaurantXML(@RequestParam Integer restaurantId) {
		ListDish ret = new ListDish();
		ret.setDishes(restaurantService.findOne(restaurantId).getDishes());
        return ret;
    }

}
