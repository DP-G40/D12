package controllers.rest;
 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlRootElement;

import domain.Dish;
 
@XmlRootElement (name="Dishes")
public class ListDish implements Serializable
{
    private static final long serialVersionUID = 1L;
     
    private Collection<Dish> dishes = new ArrayList<Dish>();
 
    public Collection<Dish> getDishes() {
        return dishes;
    }
 
    public void setDishes(Collection<Dish> collection) {
        this.dishes = collection;
    }
}