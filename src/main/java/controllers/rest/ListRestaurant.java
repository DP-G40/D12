package controllers.rest;
 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlRootElement;

import domain.Restaurant;
 
@XmlRootElement (name="Restaurants")
public class ListRestaurant implements Serializable
{
    private static final long serialVersionUID = 1L;
     
    private Collection<Restaurant> restaurants = new ArrayList<Restaurant>();
 
    public Collection<Restaurant> getRestaurants() {
        return restaurants;
    }
 
    public void setRestaurants(Collection<Restaurant> collection) {
        this.restaurants = collection;
    }
}