package controllers.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import domain.Restaurant;
import services.RestaurantService;

@RestController()
@RequestMapping("/rest")
public class RestaurantRestController {
	
	@Autowired
    private RestaurantService restaurantService;
	
	@RequestMapping(value="/getRestaurantsJSON", produces="application/json; charset=UTF-8",
		    method=RequestMethod.GET)
    public @ResponseBody Collection<Restaurant> getRestaurantJSON() {
		Collection<Restaurant> r = restaurantService.findAll();
        return r;
    }
	
	@RequestMapping(value="/getRestaurantsXML",produces=MediaType.APPLICATION_XML_VALUE,
		    method=RequestMethod.GET)
    public @ResponseBody ListRestaurant getRestaurantXML() {
		ListRestaurant ret = new ListRestaurant();
		ret.setRestaurants(restaurantService.findAll());
        return ret;
    }

}
