package controllers.restaurant;

import controllers.AbstractController;
import domain.Banner;
import domain.Offer;
import domain.Restaurant;
import domain.Video;
import forms.BannerForm;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.BannerService;
import services.OrderService;
import services.VideoService;

@Controller
@RequestMapping("/announcement/restaurant/")
public class AnnouncementRestaurantController extends AbstractController  {

    @Autowired
    private VideoService videoService;

    @Autowired
    private BannerService bannerService;
    
    @Autowired
    private ActorService actorService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("announcement/list");
        Restaurant r = (Restaurant) actorService.findByPrincipal();
        try {
        	res.addObject("videos", videoService.getAnnouncementsByRestaurant(r.getId()));
        	res.addObject("banners", bannerService.getAnnouncementsByRestaurant(r.getId()));
            res.addObject("requestURI","announcement/restaurant/list.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
    
    @RequestMapping(value = "/createVideo")
    public ModelAndView createVideo() {
        ModelAndView res;
        
        res = new ModelAndView("announcement/createVideo");
        res.addObject("video", videoService.create());

        return res;
    }
    
    @RequestMapping(value = "/createBanner")
    public ModelAndView createBanner() {
        ModelAndView res;
        
        res = new ModelAndView("announcement/createBanner");
        res.addObject("banner", new BannerForm());

        return res;
    }
    
    @RequestMapping(value = "/saveVideo",method = RequestMethod.POST,params = "save")
    public ModelAndView saveVideo(@Valid  @ModelAttribute("video") Video video, BindingResult result){
        ModelAndView res = new ModelAndView("announcement/createVideo");
        if(!result.hasErrors()){
            try{
            	if(video.getStartDate().after(video.getEndDate())) {
            		res = new ModelAndView("announcement/createVideo");
                	res.addObject("video", video);
                	res.addObject("message", "error.date.after");
            	}else {
            		videoService.save(video);
            		res = new ModelAndView("redirect:/announcement/restaurant/list.do");
            	}
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("announcement/createVideo");
        	res.addObject("video", video);
        }
        return res;
    }
    
    @RequestMapping(value = "/saveBanner",method = RequestMethod.POST,params = "save")
    public ModelAndView saveBanner(@Valid @ModelAttribute("banner") BannerForm banner, BindingResult result){
        ModelAndView res = new ModelAndView("announcement/createBanner");
        if(!result.hasErrors()){
            try{
            	if(banner.getFile().getSize()>0) {
            		if(!(banner.getStartDate().after(banner.getEndDate()))) {
            			bannerService.reconstructAndsave(banner);
            			res = new ModelAndView("redirect:/announcement/restaurant/list.do");
            		}else {
            			res = new ModelAndView("announcement/createBanner");
                    	res.addObject("banner", banner);
                    	res.addObject("message", "error.date.after");
            		}
            	}else {
            		res = new ModelAndView("announcement/createBanner");
                	res.addObject("banner", banner);
                	res.addObject("message", "message.file.empty");
            	}
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("announcement/createBanner");
        	res.addObject("banner", banner);
        }
        return res;
    }

}
