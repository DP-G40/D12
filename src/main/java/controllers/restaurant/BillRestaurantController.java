package controllers.restaurant;

import controllers.AbstractController;
import domain.Bill;
import forms.AcceptBillForm;
import forms.CancelBillForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.BillService;

import javax.validation.Valid;

@Controller
@RequestMapping("/bill/restaurant")
public class BillRestaurantController extends AbstractController {

    @Autowired
    private BillService billService;

    @RequestMapping(value = "/accept",method = RequestMethod.GET)
    public ModelAndView accept(@RequestParam Integer billId){
        ModelAndView res = new ModelAndView("bill/accept");
        try {
            Bill bill = billService.findOne(billId);
            billService.isMine(bill);
            Assert.isTrue(bill.getStatus().equals("PENDING"));
            AcceptBillForm acceptBillForm = new AcceptBillForm();
            acceptBillForm.setBillId(billId);
            res.addObject("acceptBillForm",acceptBillForm);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/accept",method = RequestMethod.POST,params = "save")
    public ModelAndView saveAccept(@Valid  AcceptBillForm acceptBillForm , BindingResult result){
        ModelAndView res = new ModelAndView("bill/accept");
        res.addObject("acceptBillForm",acceptBillForm);
        if (!result.hasErrors()) {
            try {
                Bill bill = billService.reconstructAccept(acceptBillForm);
                billService.save(bill);
                res = new ModelAndView("redirect:/bill/restaurant/list.do");
            } catch (Throwable oops) {
                if (oops.getMessage().equals("creditcard.date.error")){
                    res.addObject("message","creditcard.date.error");
                }
            }
        }
        return res;
    }

    @RequestMapping(value = "/cancel",method = RequestMethod.GET)
    public ModelAndView cancel(@RequestParam Integer billId){
        ModelAndView res = new ModelAndView("bill/cancel");
        try {
            Bill bill = billService.findOne(billId);
            billService.isMine(bill);
            Assert.isTrue(bill.getStatus().equals("PENDING"));
            CancelBillForm cancelBillForm = new CancelBillForm();
            cancelBillForm.setBillId(billId);
            res.addObject("cancelBillForm",cancelBillForm);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/cancel",method = RequestMethod.POST,params = "save")
    public ModelAndView saveCancel(@Valid  CancelBillForm cancelBillForm , BindingResult result){
        ModelAndView res = new ModelAndView("bill/cancel");
        res.addObject("cancelBillForm",cancelBillForm);
        if (!result.hasErrors()) {
            try {
                Bill bill = billService.reconstructCancel(cancelBillForm);
                billService.save(bill);
                res = new ModelAndView("redirect:/bill/restaurant/list.do");
            } catch (Throwable oops) {
                System.out.println(oops.getMessage());
            }
        }
        return res;
    }

    @RequestMapping(value = "/list")
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("bill/list");
        try {
            res.addObject("bills",billService.findBillByRestaurant());
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


}
