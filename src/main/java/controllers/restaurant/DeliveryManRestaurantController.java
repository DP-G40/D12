package controllers.restaurant;

import controllers.AbstractController;
import domain.DeliveryMan;
import domain.Restaurant;

import java.util.List;

import forms.RegisterCustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.DeliveryManService;
import services.OrderService;

import javax.validation.Valid;

@Controller
@RequestMapping("/deliveryman/restaurant")
public class DeliveryManRestaurantController extends AbstractController  {

    @Autowired
    private DeliveryManService deliveryManService;

    @Autowired
    private ActorService actorService;

    @RequestMapping(value = "/myDeliveryMan")
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("deliveryMan/all");
        Restaurant r = (Restaurant) actorService.findByPrincipal();
        
        List<DeliveryMan> deliverysmans = deliveryManService.findByRestaurantId(r.getId());
        res.addObject("deliverysmans", deliverysmans);
        res.addObject("requestUri", "deliveryman/restaurant/myDeliveryMan.do");
        
        return res;
    }

    @RequestMapping(value = "/seeMyDeliveryMan", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer deliverymanId) {
        ModelAndView res = new ModelAndView("deliveryMan/view");

        try {
            DeliveryMan deliveryman = deliveryManService.findOne(deliverymanId);
            Restaurant r = (Restaurant) actorService.findByPrincipal();
            Assert.notNull(deliveryman);
            Assert.isTrue(r.getId() == deliveryman.getRestaurant().getId());
            res.addObject("deliveryman", deliveryman);
            res.addObject("orders", deliveryman.getOrders());
            res.addObject("requestUri", "deliveryman/restaurant/seeMyDeliveryMan.do");

        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        
        return res;
    }

    @RequestMapping(value = "/fired", method = RequestMethod.GET)
    public ModelAndView fired(@RequestParam Integer deliveryId){
        ModelAndView res = new ModelAndView("redirect:/deliveryman/restaurant/myDeliveryMan.do");
        try {
            deliveryManService.fired(deliveryId);
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public ModelAndView register(){
        ModelAndView res = new ModelAndView("delivery/register");
        try {
            RegisterCustomerForm registerCustomerForm = new RegisterCustomerForm();
            res.addObject("registerCustomerForm", registerCustomerForm);
            res.addObject("action", "deliveryman/restaurant/register.do");
        } catch(Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/register",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid RegisterCustomerForm registerCustomerForm, BindingResult result){
        ModelAndView res = new ModelAndView("delivery/register");
        res.addObject("registerCustomerForm", registerCustomerForm);
        res.addObject("action", "deliveryman/restaurant/register.do");
        if (!result.hasErrors()){
            try {
                DeliveryMan deliveryMan = deliveryManService.reconstruct(registerCustomerForm);
                deliveryManService.save(deliveryMan);
                 res = new ModelAndView("redirect:/#");
            } catch (Throwable oops){
                if (oops.getMessage().equals("register.error.password")) {
                    res.addObject("message", "register.error.password");
                } else if (oops.getMessage().equals("register.error.username")) {
                    res.addObject("message", "register.error.username");
                }
            }
        }
        return res;

    }
    
}
