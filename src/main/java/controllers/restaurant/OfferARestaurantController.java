/*
 * AdministratorController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.restaurant;

import controllers.AbstractController;
import domain.Offer;
import forms.OfferForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.OfferService;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/offer/restaurant")
public class OfferARestaurantController extends AbstractController {

    // Constructors -----------------------------------------------------------

    public OfferARestaurantController() {
        super();
    }

    //SERVICIOS

    @Autowired
    private OfferService offerService;

    @Autowired
    private ActorService actorService;

    @RequestMapping(value = "/myOffers")
    public ModelAndView myOffers() {
        ModelAndView res;
        res = new ModelAndView("offer/list");

        Integer actorID = actorService.findByPrincipal().getId();
        res.addObject("offers", offerService.findByRestaurant(actorID));
        res.addObject("requestURI", "offer/restaurant/myOffers.do");
        res.addObject("actorID", actorID);

        return res;
    }


    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView ban(@RequestParam Integer offerId) {
        ModelAndView res;
        res = new ModelAndView("redirect:/offer/restaurant/myOffers.do");

        try {
            Offer offer = offerService.findOne(offerId);
            offerService.isMine(offer);
            offerService.delete(offer);
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView res;
        res = new ModelAndView("offer/create");
        try {
            OfferForm offerForm = new OfferForm();
            res.addObject("offerForm", offerForm);
            res.addObject("actionURL", "offer/restaurant/create.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid OfferForm offerForm, BindingResult result) {
        ModelAndView res = new ModelAndView("offer/create");
        res.addObject("offerForm", offerForm);
        if (!result.hasErrors()) {
            try {
                Offer offer = offerService.reconstruct(offerForm);
                offerService.save(offer);
                res = new ModelAndView("redirect:/offer/restaurant/myOffers.do");
            } catch (Throwable oops) {
                if (oops.getMessage().equals("offer.error.date")) {
                    res.addObject("message", "offer.error.date");
                }
            }
        }

        return res;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam Integer offerId) {
        ModelAndView res;
        res = new ModelAndView("offer/edit");
        try {
            Offer offer = offerService.findOne(offerId);
            offerService.isMine(offer);
            OfferForm offerForm = offerService.construct(offer);
            res.addObject("offerForm", offerForm);
            res.addObject("actionURL", "offer/restaurant/edit.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView saveEdit(@Valid OfferForm offerForm, BindingResult result) {
        ModelAndView res = new ModelAndView("offer/edit");
        res.addObject("offerForm", offerForm);
        if (!result.hasErrors()) {
            try {
                Offer offer = offerService.reconstruct(offerForm);
                offerService.save(offer);
                res = new ModelAndView("redirect:/offer/restaurant/myOffers.do");
            } catch (Throwable oops) {
                if (oops.getMessage().equals("offer.error.date")) {
                    res.addObject("message", "offer.error.date");
                }
            }
        }

        return res;
    }

}
