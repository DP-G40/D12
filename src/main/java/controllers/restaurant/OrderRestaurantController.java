package controllers.restaurant;

import controllers.AbstractController;
import domain.Order;
import domain.Restaurant;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.OrderService;

import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/order/restaurant/")
public class OrderRestaurantController extends AbstractController  {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ActorService actorService;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("order/restaurant/list");
        try {
            Restaurant r = (Restaurant) actorService.findByPrincipal();
            res.addObject("pendings", orderService.getOrdersByRestaurantAndStatus(r.getId(),"PENDING"));
            res.addObject("accepteds", orderService.getOrdersByRestaurantAndStatus(r.getId(),"COOKING"));
            res.addObject("deliverings", orderService.getOrdersByRestaurantAndStatus(r.getId(),"DELIVERING"));
            res.addObject("completeds", orderService.getOrdersByRestaurantAndStatus(r.getId(),"COMPLETED"));
            res.addObject("rejecteds", orderService.getOrdersByRestaurantAndStatus(r.getId(),"REJECTED"));
            res.addObject("readys", orderService.getOrdersByRestaurantAndStatus(r.getId(),"READY"));
            res.addObject("requestURI","order/restaurant/list.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


    @RequestMapping(value = "/accept", method = RequestMethod.GET)
    public ModelAndView acceptOrder(@RequestParam int orderId){
        ModelAndView res;
        try {
            Order o = orderService.findOne(orderId);
            o.setStatus("COOKING");

            Date fecha=sumarRestarHorasFecha(new Date(),1);
            o.setDeliveryHour(fecha);
            orderService.save(o);

            if(o.getReviewer().getEmail() != null || o.getReviewer().getEmail() !="" || !o.getReviewer().getEmail().contains("@") ){
                orderService.correoAceptado(o);
            }

            res = list();
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/ready", method = RequestMethod.GET)
    public ModelAndView readyOrder(@RequestParam int orderId){
        ModelAndView res;
        try {
            Order o = orderService.findOne(orderId);
            o.setStatus("READY");
            orderService.save(o);
            res = list();
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/decline", method = RequestMethod.GET)
    public ModelAndView declineOrder(@RequestParam int orderId){
        ModelAndView res;
        try {
            Order o = orderService.findOne(orderId);
            o.setStatus("REJECTED");
            orderService.save(o);
            if(o.getReviewer().getEmail() != null || o.getReviewer().getEmail() !="" || !o.getReviewer().getEmail().contains("@") ){
                orderService.correoRechazado(o);
            }
            res = list();
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer orderId){
        ModelAndView res = new ModelAndView("order/view");
        try {
            Order order = orderService.findOne(orderId);
            orderService.isMineRestaurant(order);
            res.addObject("order",order);
            res.addObject("back","order/restaurant/list.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }




    public Date sumarRestarHorasFecha(Date fecha, int horas){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.HOUR, horas);  // numero de horas a a�adir, o restar en caso de horas<0
        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas a�adidas
    }

}
