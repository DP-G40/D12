package controllers.restaurant;

import controllers.AbstractController;
import domain.Offer;
import domain.PostalAddress;
import domain.Restaurant;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.OrderService;
import services.PostalAddressService;
import services.RestaurantService;

@Controller
@RequestMapping("/postal/restaurant/")
public class PostalAddressRestaurantController extends AbstractController  {

    @Autowired
    private PostalAddressService postalAddressService;

    @Autowired
    private ActorService actorService;
    
    @Autowired
    private RestaurantService restaurantService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("postal/list");
        Restaurant r = (Restaurant) actorService.findByPrincipal();
        try {
        	res.addObject("postals", r.getPostalAddresses());
        	res.addObject("actionURL", "postal/restaurant/list.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam Integer postalId) {
        ModelAndView res;
        res = new ModelAndView("redirect:/postal/restaurant/list.do");
        
        try{
        	PostalAddress postal = postalAddressService.findOne(postalId);
        	Integer actorID = actorService.findByPrincipal().getId();
        	if((postal.getRestaurant().getId()==actorID)) {
        		postalAddressService.delete(postal);
        	}
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }
	
	@RequestMapping(value = "/create")
    public ModelAndView create() {
        ModelAndView res;
        
        res = new ModelAndView("postal/edit");
        res.addObject("postal", postalAddressService.create());
        res.addObject("actionURL", "postal/restaurant/save.do");

        return res;
    }
	
	@RequestMapping(value = "/save",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid @ModelAttribute("postal") PostalAddress postal, BindingResult result){
        ModelAndView res = new ModelAndView("postal/edit");
        if(!result.hasErrors()){
            try{
            	Integer actorID = actorService.findByPrincipal().getId();
            	Restaurant restaurant = restaurantService.findOne(actorID);
            	if((postal.getRestaurant().getId()==actorID)) {
            		PostalAddress comparation = postalAddressService.findByRestaurantAndAddress(restaurant, postal.getPostal());
            		if(comparation == null || comparation.getId()==postal.getId()) {
            			postalAddressService.save(postal);
                        res = new ModelAndView("redirect:/restaurant/view.do?restaurantId="+restaurant.getId());
            		}else {
            			res = new ModelAndView("postal/edit");
                    	res.addObject("postal", postal);
                    	res.addObject("actionURL", "postal/restaurant/save.do");
                    	res.addObject("message","postal.already.exists");
            		}
            	}
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("postal/edit");
        	res.addObject("postal", postal);
        	res.addObject("actionURL", "postal/restaurant/save.do");
        }
        return res;
    }
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam Integer postalId) {
        ModelAndView res;
        try {

            PostalAddress p = postalAddressService.findOne(postalId);
            Restaurant r = (Restaurant) actorService.findByPrincipal();
            Assert.notNull(p);
            Assert.isTrue(p.getRestaurant() == r);
            res = new ModelAndView("postal/edit");
            res.addObject("postal", p );
            res.addObject("actionURL", "postal/restaurant/save.do");
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }


        return res;
    }

}
