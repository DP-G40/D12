package controllers.restaurant;

import controllers.AbstractController;
import domain.*;

import java.util.Collection;

import forms.AddAllergenForm;
import forms.DishForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/dish/restaurant")
public class RestaurantDishController extends AbstractController {

    @Autowired
    private DishService dishService;


    @Autowired
    private RestaurantService restaurantService;


    @Autowired
    private ActorService actorService;

    @Autowired
    private CategoryService categoryService;


    @Autowired
    private AllegenService allergenService;




















    @RequestMapping("/list")
    public ModelAndView listAll(){
        ModelAndView res;
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Collection<Dish> dishes = dishService.findActivesByRestaurant(restaurant.getId());
        res = new ModelAndView("dish/all");
        res.addObject("dishes",dishes);
        res.addObject("requestURI", "/dish/restaurant/list.do");
        return res;
    }

    @RequestMapping("/deleted")
    public ModelAndView deleted(){
        ModelAndView res;
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Collection<Dish> dishes = dishService.findDeletedByRestaurant(restaurant.getId());
        res = new ModelAndView("dish/deleted");
        res.addObject("dishes",dishes);
        res.addObject("requestURI", "/dish/restaurant/list.do");
        return res;
    }


    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam int dishId) {
        ModelAndView res;
        try {
            Dish dish = dishService.findOne(dishId);
            res = new ModelAndView("dish/view");
            res.addObject("dish",dish);
            res.addObject("requestURI", "/dish/restaurant/view.do");

        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create(){
        ModelAndView res;
        DishForm form = new DishForm();
        Collection<Category> categories = categoryService.findAll();
        categories.remove(categoryService.findCategoryFather());
        res = new ModelAndView("dish/create");
        res.addObject("dishForm", form);
        res.addObject("categories", categories);

        return res;
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
    public ModelAndView saveCreate(@Valid DishForm dishForm, final BindingResult bindingResult){
        ModelAndView res;
        Collection<Category> categories = categoryService.findAll();
        categories.remove(categoryService.findCategoryFather());
        if (bindingResult.hasErrors()) {
            res = new ModelAndView("dish/create");
            res.addObject("dishForm", dishForm);
            res.addObject("categories", categories);
        } else
            try {
                Dish dish = dishService.reconstruct(dishForm);
                Dish nuevo = dishService.save(dish);
                res = new ModelAndView("redirect:/dish/restaurant/view.do?dishId="+nuevo.getId());
            } catch (final Throwable oops) {
                res = new ModelAndView("dish/create");
                res.addObject("dishForm", dishForm);
                res.addObject("categories", categories);
                res.addObject("message", "message.commit.error");
            }



        return res;
    }

    @RequestMapping(value = "/addAllergens", method = RequestMethod.GET)
    public ModelAndView addAllergens(@RequestParam int dishId){
        ModelAndView res;


        try {
            Dish d = dishService.findOne(dishId);
            
            res = new ModelAndView("dish/addallergen");
            AddAllergenForm addAllergenForm = new AddAllergenForm();
            addAllergenForm.setDishId(dishId);
            Collection<Allergen> allergens = allergenService.findAll();

            res.addObject("allergens", allergens);
            res.addObject("addAllergenForm",addAllergenForm);
            res.addObject("dishId",dishId);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }


        return res;
    }

    @RequestMapping(value = "/addAllergens", method = RequestMethod.POST, params = "save")
    public ModelAndView saveAddAllergen(@Valid AddAllergenForm addAllergenForm, final BindingResult bindingResult){
        ModelAndView res;

        Collection<Allergen> allergens = allergenService.findAll();

        if (bindingResult.hasErrors()) {
            res = new ModelAndView("dish/addallergen");
            res.addObject("allergens", allergens);
            res.addObject("addAllergenForm",addAllergenForm);


        } else
            try {
                Dish a = dishService.addAllergenToDish(addAllergenForm);
                res = new ModelAndView("redirect:/dish/restaurant/view.do?dishId="+a.getId());

            } catch (final Throwable oops) {
                res = new ModelAndView("dish/addallergen");
                res.addObject("allergens", allergens);
                res.addObject("addAllergenForm",addAllergenForm);

                res.addObject("message", "message.commit.error");
            }



        return res;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int dishId){
        ModelAndView res;
        try{
            Dish x = dishService.findOne(dishId);
            Restaurant yo = (Restaurant) actorService.findByPrincipal();

            Assert.isTrue(x.getRestaurant().getId() == yo.getId());

            DishForm form = dishService.construct(x);
            Collection<Category> categories = categoryService.findAll();
            categories.remove(categoryService.findCategoryFather());

            res = new ModelAndView("dish/edit");
            res.addObject("dishForm", form);
            res.addObject("categories", categories);


        }catch (Exception e){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView saveEdit(@Valid DishForm dishForm, final BindingResult bindingResult){
        ModelAndView res;
        Collection<Category> categories = categoryService.findAll();
        categories.remove(categoryService.findCategoryFather());
        if (bindingResult.hasErrors()) {
            res = new ModelAndView("dish/edit");
            res.addObject("dishForm", dishForm);
            res.addObject("categories", categories);
        } else
            try {
                Dish dish = dishService.reconstruct(dishForm);
                Dish nuevo = dishService.save(dish);
                res = new ModelAndView("redirect:/dish/restaurant/view.do?dishId="+nuevo.getId());
            } catch (final Throwable oops) {
                res = new ModelAndView("dish/create");
                res.addObject("dishForm", dishForm);
                res.addObject("categories", categories);
                res.addObject("message", "message.commit.error");
            }
        return res;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
    public ModelAndView saveDelete(@Valid DishForm dishForm, final BindingResult bindingResult){
        ModelAndView res;
        Collection<Category> categories = categoryService.findAll();
        categories.remove(categoryService.findCategoryFather());
        if (bindingResult.hasErrors()) {
            res = new ModelAndView("dish/edit");
            res.addObject("dishForm", dishForm);
            res.addObject("categories", categories);
        } else
            try {
                Dish dish = dishService.reconstruct(dishForm);
                dishService.delete(dish);
                res = new ModelAndView("redirect:/dish/restaurant/list.do");
            } catch (final Throwable oops) {
                res = new ModelAndView("dish/create");
                res.addObject("dishForm", dishForm);
                res.addObject("categories", categories);
                res.addObject("message", "message.commit.error");
            }
        return res;
    }



}
