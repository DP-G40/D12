package controllers.restaurant;

import controllers.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ReviewService;

@ControllerAdvice
@RequestMapping("/review/restaurant")
public class ReviewRestaurantController extends AbstractController {

    @Autowired
    private ReviewService reviewService;

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("review/list");
        try {
            res.addObject("reviews", reviewService.findByRestaurant());
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
}
