package controllers.restaurant;

import controllers.AbstractController;
import domain.Offer;
import domain.PostalAddress;
import domain.Restaurant;
import domain.Schedule;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.OrderService;
import services.PostalAddressService;
import services.RestaurantService;
import services.ScheduleService;

@Controller
@RequestMapping("/schedule/restaurant/")
public class ScheduleRestaurantController extends AbstractController  {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ActorService actorService;
    
    @Autowired
    private RestaurantService restaurantService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("schedule/list");
        Restaurant r = (Restaurant) actorService.findByPrincipal();
        try {
        	res.addObject("schedules", r.getSchedules());
        	res.addObject("actionURL", "schedule/restaurant/list.do");
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam Integer scheduleId) {
        ModelAndView res;
        res = new ModelAndView("redirect:/schedule/restaurant/list.do");
        
        try{
        	Schedule schedule = scheduleService.findOne(scheduleId);
        	Integer actorID = actorService.findByPrincipal().getId();
        	if((schedule.getRestaurant().getId()==actorID)) {
        		scheduleService.delete(schedule);
        	}
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }
	
	@RequestMapping(value = "/create")
    public ModelAndView create() {
        ModelAndView res;
        
        res = new ModelAndView("schedule/edit");
        res.addObject("schedule", scheduleService.create());
        res.addObject("actionURL", "schedule/restaurant/save.do");

        return res;
    }
	
	@RequestMapping(value = "/save",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid Schedule schedule, BindingResult result){
        ModelAndView res = new ModelAndView("schedule/edit");
        if(!result.hasErrors()){
            try{
            	Integer actorID = actorService.findByPrincipal().getId();
            	Restaurant restaurant = restaurantService.findOne(actorID);
            	if((schedule.getRestaurant().getId()==actorID)) {
            		Schedule comparation = scheduleService.findByRestaurantAndDay(restaurant.getId(), schedule.getDay());
            		if(comparation == null || comparation.getId()==schedule.getId()) {
            			scheduleService.save(schedule);
	            		res = new ModelAndView("redirect:/restaurant/view.do?restaurantId="+restaurant.getId());
            		}else {
            			res = new ModelAndView("schedule/edit");
                    	res.addObject("schedule", schedule);
                    	res.addObject("actionURL", "schedule/restaurant/save.do");
                    	res.addObject("message","schedule.already.exists");
            		}
            	}
            } catch (Throwable oops){
            	res = new ModelAndView("redirect:/#");
            }
        }else{
        	res = new ModelAndView("schedule/edit");
        	res.addObject("schedule", schedule);
        	res.addObject("actionURL", "schedule/restaurant/save.do");
        }
        return res;
    }
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam Integer scheduleId) {
        ModelAndView res;
        
        res = new ModelAndView("schedule/edit");
        res.addObject("schedule", scheduleService.findOne(scheduleId));
        res.addObject("actionURL", "schedule/restaurant/save.do");

        return res;
    }

}
