package controllers.restaurant;

import controllers.AbstractController;
import controllers.RestaurantController;
import domain.Restaurant;
import domain.Tag;
import forms.TagRestaurantForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.RestaurantService;
import services.TagService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/tag/restaurant")
public class TagRestaurantController extends AbstractController {


    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private TagService tagService;

    @Autowired
    private ActorService actorService;

    @RequestMapping(value = "/addtag", method = RequestMethod.GET)
    public ModelAndView addTag(){
        ModelAndView res;


        Restaurant r =(Restaurant) actorService.findByPrincipal();
        Collection<Tag> tags = tagService.findAll();
        Collection<Tag> tagsqueyatengo = r.getTags();
        tags.removeAll(tagsqueyatengo);
        TagRestaurantForm tagForm = new TagRestaurantForm();

        res = new ModelAndView("restaurant/addtag");
        res.addObject("tagRestaurantForm",tagForm);
        res.addObject("idres",r.getId());
        res.addObject("tags",tags);

        return res;
    }

    @RequestMapping(value = "/removetag", method = RequestMethod.GET)
    public ModelAndView removeTag(){
        ModelAndView res;


        Restaurant r =(Restaurant) actorService.findByPrincipal();
        Collection<Tag> tags = r.getTags();
        TagRestaurantForm tagForm = new TagRestaurantForm();

        res = new ModelAndView("restaurant/removetag");
        res.addObject("tagRestaurantForm",tagForm);
        res.addObject("idres",r.getId());
        res.addObject("tags",tags);

        return res;
    }

    @RequestMapping(value = "/addtag", method = RequestMethod.POST, params = "save")
    public ModelAndView saveAddTag(@Valid TagRestaurantForm tagRestaurantForm, final BindingResult bindingResult){
        ModelAndView res;
        Restaurant r =(Restaurant) actorService.findByPrincipal();
        Collection<Tag> tags = tagService.findAll();
        Collection<Tag> tagsqueyatengo = r.getTags();
        tags.removeAll(tagsqueyatengo);

        if(bindingResult.hasErrors()){
            res = new ModelAndView("restaurant/addtag");
            res.addObject("tagRestaurantForm",tagRestaurantForm);
            res.addObject("tags",tags);
        }else {
            try {
                Tag n = tagService.reconstructRestaurant(tagRestaurantForm);
                r.getTags().add(n);
                restaurantService.save(r);
                res = new ModelAndView("redirect:/restaurant/view.do?restaurantId="+r.getId());

            }catch (Throwable oops){
                res = new ModelAndView("restaurant/addtag");
                res.addObject("tagRestaurantForm",tagRestaurantForm);
                res.addObject("tags",tags);
                res.addObject("message", "message.commit.error");
            }
        }


        return res;

    }

    @RequestMapping(value = "/removetag", method = RequestMethod.POST, params = "save")
    public ModelAndView saveremoveAddTag(@Valid TagRestaurantForm tagRestaurantForm, final BindingResult bindingResult){
        ModelAndView res;
        Restaurant r =(Restaurant) actorService.findByPrincipal();
        Collection<Tag> tags = r.getTags();

        if(bindingResult.hasErrors()){
            res = new ModelAndView("restaurant/removetag");
            res.addObject("tagRestaurantForm",tagRestaurantForm);
            res.addObject("tags",tags);
        }else {
            try {
                Tag n = tagService.reconstructRestaurant(tagRestaurantForm);
                r.getTags().remove(n);
                restaurantService.save(r);
                res = new ModelAndView("redirect:/restaurant/view.do?restaurantId="+r.getId());

            }catch (Throwable oops){
                res = new ModelAndView("restaurant/removetag");
                res.addObject("tagRestaurantForm",tagRestaurantForm);
                res.addObject("tags",tags);
                res.addObject("message", "message.commit.error");
            }
        }


        return res;

    }

}
