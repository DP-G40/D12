package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.Allergen;
@Component
@Transactional
public class AllegenToStringConverter implements Converter<Allergen, String> {@Override
public String convert(Allergen allergen) {
String result;
if (allergen == null)
result = null;
else
result = String.valueOf(allergen.getId());
return result;
}
 }
