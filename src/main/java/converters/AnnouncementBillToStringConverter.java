package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.AnnouncementBill;
@Component
@Transactional
public class AnnouncementBillToStringConverter implements Converter<AnnouncementBill, String> {@Override
public String convert(AnnouncementBill announcementbill) {
String result;
if (announcementbill == null)
result = null;
else
result = String.valueOf(announcementbill.getId());
return result;
}
 }
