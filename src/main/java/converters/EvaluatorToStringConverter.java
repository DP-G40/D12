package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.Evaluator;
@Component
@Transactional
public class EvaluatorToStringConverter implements Converter<Evaluator, String> {@Override
public String convert(Evaluator evaluator) {
String result;
if (evaluator == null)
result = null;
else
result = String.valueOf(evaluator.getId());
return result;
}
 }
