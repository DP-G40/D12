package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.OrderBill;
@Component
@Transactional
public class OrderBillToStringConverter implements Converter<OrderBill, String> {@Override
public String convert(OrderBill orderbill) {
String result;
if (orderbill == null)
result = null;
else
result = String.valueOf(orderbill.getId());
return result;
}
 }
