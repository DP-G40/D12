package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.PostalAddress;
@Component
@Transactional
public class PostalAddressToStringConverter implements Converter<PostalAddress, String> {@Override
public String convert(PostalAddress postaladdress) {
String result;
if (postaladdress == null)
result = null;
else
result = String.valueOf(postaladdress.getId());
return result;
}
 }
