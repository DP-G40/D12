package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AllegenRepository;
import domain.Allergen;
@Component
@Transactional
public class StringToAllegenConverter implements Converter<String, Allergen> {
@Autowired
AllegenRepository   allegenRepository;
@Override
public Allergen convert(String text) {
Allergen result;
int id;
try {
id = Integer.valueOf(text);
result = allegenRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
