package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AnnouncementBillRepository;
import domain.AnnouncementBill;
@Component
@Transactional
public class StringToAnnouncementBillConverter implements Converter<String, AnnouncementBill> {
@Autowired
AnnouncementBillRepository   announcementbillRepository;
@Override
public AnnouncementBill convert(String text) {
AnnouncementBill result;
int id;
try {
id = Integer.valueOf(text);
result = announcementbillRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
