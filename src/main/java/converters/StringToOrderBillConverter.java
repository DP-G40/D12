package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.OrderBillRepository;
import domain.OrderBill;
@Component
@Transactional
public class StringToOrderBillConverter implements Converter<String, OrderBill> {
@Autowired
OrderBillRepository   orderbillRepository;
@Override
public OrderBill convert(String text) {
OrderBill result;
int id;
try {
id = Integer.valueOf(text);
result = orderbillRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
