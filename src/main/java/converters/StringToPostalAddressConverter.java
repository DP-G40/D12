package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.PostalAddressRepository;
import domain.PostalAddress;
@Component
@Transactional
public class StringToPostalAddressConverter implements Converter<String, PostalAddress> {
@Autowired
PostalAddressRepository   postaladdressRepository;
@Override
public PostalAddress convert(String text) {
PostalAddress result;
int id;
try {
id = Integer.valueOf(text);
result = postaladdressRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
