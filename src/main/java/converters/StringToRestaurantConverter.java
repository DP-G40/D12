package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.RestaurantRepository;
import domain.Restaurant;
@Component
@Transactional
public class StringToRestaurantConverter implements Converter<String, Restaurant> {
@Autowired
RestaurantRepository   restaurantRepository;
@Override
public Restaurant convert(String text) {
Restaurant result;
int id;
try {
id = Integer.valueOf(text);
result = restaurantRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
