package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.TemplateRepository;
import domain.Template;
@Component
@Transactional
public class StringToTemplateConverter implements Converter<String, Template> {
@Autowired
TemplateRepository   templateRepository;
@Override
public Template convert(String text) {
Template result;
int id;
try {
id = Integer.valueOf(text);
result = templateRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
