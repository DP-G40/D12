package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.Template;
@Component
@Transactional
public class TemplateToStringConverter implements Converter<Template, String> {@Override
public String convert(Template template) {
String result;
if (template == null)
result = null;
else
result = String.valueOf(template.getId());
return result;
}
 }
