package domain;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import security.UserAccount;

import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Access(AccessType.PROPERTY)
@XmlRootElement (name = "Restaurant")
@XmlAccessorType(XmlAccessType.NONE)
public abstract class Actor extends Commentable {

    // Atributos ----
	@XmlElement
    private String name;
	@XmlElement
    private String surname;
	@XmlElement
    private String email;
	@XmlElement
    private String address;
	@XmlElement
    private String phone;
	
	@Lob
    @Column(name="PHOTO", columnDefinition="mediumblob")
    private Blob photo;
	
	@Transient
    private String imageCode64;
 
    @Transient
    public void setImageCode64(String imageCode64) {
		this.imageCode64 = imageCode64;
	}
    
    @Transient
    @JsonIgnore
   	public String getImageCode64() {
   		try {
   			byte[] imgByte = photo.getBytes(1, (int) photo.length());
   			return Base64.encodeBase64String(imgByte);
   		} catch (SQLException e) {
   			return null;
   		}
   	}

	// Constructor ----
    public Actor() {
        super();
    }

    @NotNull
    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    @NotBlank
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @NotNull
    @NotBlank
    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull
    @NotBlank
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @NotNull
    @NotBlank
    @Pattern(regexp = "^\\+\\d{2}\\s\\d{9}$", message="Need be +XX NNNNNNNNN")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    @JsonIgnore
    public Blob getPhoto() {
		return photo;
	}

	public void setPhoto(Blob photo) {
		this.photo = photo;
	}


    private UserAccount userAccount;

    @OneToOne(optional = false , cascade = CascadeType.ALL)
    @JsonIgnore
    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }


}
