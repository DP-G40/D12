package domain;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Allergen extends DomainEntity {

    // Atributos ----
    private String name;

    // Constructor ----

    public Allergen() {
        super();
    }

    @NotNull
    @NotBlank
    @Column(unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    //relations

	private List<Dish> dishes = new ArrayList<Dish>();
	
	@ManyToMany(mappedBy = "allergens")
	public List<Dish> getDishes() {
		return dishes;
	}

	public void setDishes(List<Dish> dishes) {
		this.dishes = dishes;
	}

}
