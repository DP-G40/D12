package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Announcement extends DomainEntity {

    // Atributos ----
    private String url;
    private Date startDate;
    private Date endDate;

    // Constructor ----

    public Announcement() {
        super();
    }

    @NotBlank
    @NotNull
    @URL
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    //Relaciones

    private Restaurant restaurant;

    private Collection<AnnouncementBill> announcementBills;

    @ManyToOne(optional = false)
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @OneToMany(mappedBy = "announcement")
    public Collection<AnnouncementBill> getAnnouncementBills() {
        return announcementBills;
    }

    public void setAnnouncementBills(Collection<AnnouncementBill> announcementBills) {
        this.announcementBills = announcementBills;
    }
}
