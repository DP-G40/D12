package domain;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class AnnouncementBill extends DomainEntity {

    // Atributos ----
    private Double priceDay;
    private Integer days;

    // Constructor ----

    public AnnouncementBill() {
        super();
    }

    @NotNull
    @DecimalMin("0.1")
    public Double getPriceDay() {
        return priceDay;
    }

    public void setPriceDay(Double priceDay) {
        this.priceDay = priceDay;
    }

    @NotNull
    @Min(1)
    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    //Relaciones

    private Announcement announcement;
    private Bill bill;

    @ManyToOne
    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    @ManyToOne
    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
}
