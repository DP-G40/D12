package domain;

import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Base64;

@Entity
@Access(AccessType.PROPERTY)
public class Banner extends Announcement {

    // Atributos ----

    // Constructor ----

    public Banner() {
        super();
    }
    
    @Lob
    @NotNull
    @Column(name="IMAGE", columnDefinition="mediumblob")
    private Blob image;
    
    @Transient
    private String imageCode64;
 
    @Transient
    public void setImageCode64(String imageCode64) {
		this.imageCode64 = imageCode64;
	}
    
    @Transient
   	public String getImageCode64() {
   		try {
   			byte[] imgByte = image.getBytes(1, (int) image.length());
   			return Base64.encodeBase64String(imgByte);
   		} catch (SQLException e) {
   			return null;
   		}
   	}

	public Blob getImage() {
        return image;
    }
 
    public void setImage(Blob image) {
        this.image = image;
    }
    
   
    
}
