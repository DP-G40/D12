package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Bill extends DomainEntity {

    // Atributos ----
    private Double monthlyFee;
    private Double announcementFee;
    private String status;
    private Creditcard creditcard;
    private Date creationDate;
    private String reason;
    private String ticker;

    // Constructor ----

    public Bill() {
        super();
    }

    @NotNull
    public Double getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(Double monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    @NotNull
    public Double getAnnouncementFee() {
        return announcementFee;
    }

    public void setAnnouncementFee(Double announcementFee) {
        this.announcementFee = announcementFee;
    }

    @NotBlank
    @Pattern(regexp = "PENDING|REJECTED|PAID")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Valid
    public Creditcard getCreditcard() {
        return creditcard;
    }

    public void setCreditcard(Creditcard creditcard) {
        this.creditcard = creditcard;
    }

    @NotNull
    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

    //Relaciones

    private Restaurant restaurant;
    private Collection<AnnouncementBill> announcementBills = new ArrayList<>();

    @ManyToOne(optional = false)
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @OneToMany(mappedBy = "bill")
    public Collection<AnnouncementBill> getAnnouncementBills() {
        return announcementBills;
    }

    public void setAnnouncementBills(Collection<AnnouncementBill> announcementBills) {
        this.announcementBills = announcementBills;
    }

}
