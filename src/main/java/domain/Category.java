package domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Category extends DomainEntity {

    // Atributos ----
    private String name;

    // Constructor ----

    public Category() {
        super();
    }

    @NotNull
    @NotBlank
    @Column(unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Relaciones

    private Category father;

    @ManyToOne
    public Category getFather() {
        return father;
    }

    public void setFather(Category father) {
        this.father = father;
    }
}
