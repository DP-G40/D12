package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Commentable extends DomainEntity {

    // Atributos ----

    // Constructor ----

    public Commentable() {
        super();
    }

    //Relaciones

    private Collection<Comment> comments = new ArrayList<Comment>();

    @OneToMany(mappedBy = "commentable")
    @JsonIgnore
    public Collection<Comment> getComments() {
        return comments;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }
}
