package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Configuration extends DomainEntity {

    // Atributos ----
    private Double systemPrice;
    private Double bannerPrice;
    private Double videoPrice;

    // Constructor ----

    public Configuration() {
        super();
    }

    @NotNull
    @DecimalMin("0.1")
    public Double getSystemPrice() {
        return systemPrice;
    }

    public void setSystemPrice(Double systemPrice) {
        this.systemPrice = systemPrice;
    }

    @NotNull
    @DecimalMin("0.1")
    public Double getBannerPrice() {
        return bannerPrice;
    }

    public void setBannerPrice(Double bannerPrice) {
        this.bannerPrice = bannerPrice;
    }

    @NotNull
    @DecimalMin("0.1")
    public Double getVideoPrice() {
        return videoPrice;
    }

    public void setVideoPrice(Double videoPrice) {
        this.videoPrice = videoPrice;
    }
}
