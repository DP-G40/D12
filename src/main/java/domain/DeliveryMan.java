package domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class DeliveryMan extends Actor {


    public Boolean active = true;

    // Atributos ----

    @NotNull
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


    // Constructor ----

    public DeliveryMan() {
        super();
    }


    //Relaciones

    private Restaurant restaurant;
    private Collection<Order> orders = new ArrayList<Order>();

    @ManyToOne(optional = false)
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @OneToMany(mappedBy = "deliveryMan")
    public Collection<Order> getOrders() {
        return orders;
    }

    public void setOrders(Collection<Order> orders) {
        this.orders = orders;
    }
}
