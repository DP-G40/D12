package domain;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
@XmlRootElement (name = "Dish")
@XmlAccessorType(XmlAccessType.NONE)
public class Dish extends Commentable {

    // Atributos ----
	@XmlElement
    private String name;
	@XmlElement
    private String description;
	@XmlElement
    private Double price;
	@XmlElement
    private Boolean del = false;

    // Constructor ----

    public Dish() {
        super();
    }

    @NotNull
    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    @NotBlank
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    @Min(0)
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @NotNull
    public Boolean getDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }

    //Relaciones

    private Restaurant restaurant;
    private Collection<Allergen> allergens = new ArrayList<Allergen>();
    private Category category;

    @ManyToOne(optional = false)
    @JsonIgnore
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @ManyToMany
    @JsonIgnore
    public Collection<Allergen> getAllergens() {
        return allergens;
    }

    public void setAllergens(Collection<Allergen> allergens) {
        this.allergens = allergens;
    }

    @ManyToOne(optional = false)
    @JsonIgnore
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
