package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Evaluator extends Reviewer {

    // Atributos ----

    // Constructor ----

    public Evaluator() {
        super();
    }

    //Relaciones

    private Collection<Review> reviews = new ArrayList<Review>();

    @OneToMany(mappedBy = "evaluator")
    public Collection<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Collection<Review> reviews) {
        this.reviews = reviews;
    }
}
