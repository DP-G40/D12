package domain;

import com.google.common.collect.HashMultiset;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "order2")
public class Order extends DomainEntity {

    private String ticker;
    private String comment;
    private String status;
    private Date deliveryHour;
    private String address;
    private Creditcard creditcard;
    private Date creationDate;
    private String postalAddress;


    @NotNull
    @NotBlank
    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @NotNull
    @NotBlank
    @Pattern(regexp = "COMPLETED|DELIVERING|ACCEPTED|PENDING|DRAFT|REJECTED|COOKING|READY")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIME)
    @DateTimeFormat(pattern = "HH:mm")
    public Date getDeliveryHour() {
        return deliveryHour;
    }

    public void setDeliveryHour(Date deliveryHour) {
        this.deliveryHour = deliveryHour;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Valid
    public Creditcard getCreditcard() {
        return creditcard;
    }

    public void setCreditcard(Creditcard creditcard) {
        this.creditcard = creditcard;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    @Pattern(regexp = "^\\d{5}$")
    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }


    //Relaciones

    private Restaurant restaurant;
    private Reviewer reviewer;
    private OrderBill orderBill;
    private Offer offer;
    private Collection<Dish> dishes = HashMultiset.create();
    private DeliveryMan deliveryMan;
    private Review review;

    @ManyToOne(optional = false)
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @ManyToOne(optional = false)
    public Reviewer getReviewer() {
        return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }

    @OneToOne(cascade = CascadeType.ALL)
    public OrderBill getOrderBill() {
        return orderBill;
    }

    public void setOrderBill(OrderBill orderBill) {
        this.orderBill = orderBill;
    }

    @ManyToOne
    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    @ManyToMany
    public Collection<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(Collection<Dish> dishes) {
        this.dishes = dishes;
    }

    @ManyToOne
    public DeliveryMan getDeliveryMan() {
        return deliveryMan;
    }

    public void setDeliveryMan(DeliveryMan deliveryMan) {
        this.deliveryMan = deliveryMan;
    }

    @OneToOne
    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
}
