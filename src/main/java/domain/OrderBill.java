package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class OrderBill extends DomainEntity {

    // Atributos ----
    private Double price = 0.0;
    private String ticker;

    // Constructor ----

    public OrderBill() {
        super();
    }

    @NotNull
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    
    public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

    //Relaciones

    private Order order;

    @OneToOne(optional = false , mappedBy = "orderBill")
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

}
