package domain;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
@XmlRootElement (name = "Restaurant")
@XmlAccessorType(XmlAccessType.NONE)
public class Restaurant extends Actor {

    // Atributos ----
	@XmlElement
    private String CIF;
	@XmlElement
    private Double deliveryPrice;
	@XmlElement
    private String description;
	@XmlElement
    private Boolean banned = false;

    // Constructor ----

    public Restaurant() {
        super();
    }

    @NotNull
    @NotBlank
    @Pattern(regexp = "^[A-Za-z]\\d{7}\\w{1}$", message="Need be: NDDDDDDDW")
    public String getCIF() {
        return CIF;
    }

    public void setCIF(String CIF) {
        this.CIF = CIF;
    }

    @NotNull
    @Min(0)
    public Double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(Double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    @NotNull
    @NotBlank
    @Column(length = 3500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    //Relaciones

    private Collection<Dish> dishes = new ArrayList<>();
    private Collection<Offer> offers = new ArrayList<>();
    private Collection<Tag> tags = new ArrayList<>();
    private Collection<Order> orders = new ArrayList<>();
    private Collection<DeliveryMan> deliveryMen = new ArrayList<>();
    private Collection<PostalAddress> postalAddresses = new ArrayList<>();
    private Collection<Announcement> announcements = new ArrayList<>();
    private Collection<Bill> bills = new ArrayList<>();
    private Collection<Schedule> schedules = new ArrayList<>();

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(Collection<Dish> dishes) {
        this.dishes = dishes;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<Offer> getOffers() {
        return offers;
    }

    public void setOffers(Collection<Offer> offers) {
        this.offers = offers;
    }
    @ManyToMany
    @JsonIgnore
    public Collection<Tag> getTags() {
        return tags;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<Order> getOrders() {
        return orders;
    }

    public void setOrders(Collection<Order> orders) {
        this.orders = orders;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<DeliveryMan> getDeliveryMen() {
        return deliveryMen;
    }

    public void setDeliveryMen(Collection<DeliveryMan> deliveryMen) {
        this.deliveryMen = deliveryMen;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<PostalAddress> getPostalAddresses() {
        return postalAddresses;
    }

    public void setPostalAddresses(Collection<PostalAddress> postalAddresses) {
        this.postalAddresses = postalAddresses;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(Collection<Announcement> announcements) {
        this.announcements = announcements;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<Bill> getBills() {
        return bills;
    }

    public void setBills(Collection<Bill> bills) {
        this.bills = bills;
    }

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    public Collection<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(Collection<Schedule> schedules) {
        this.schedules = schedules;
    }
}
