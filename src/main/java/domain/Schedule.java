package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Schedule extends DomainEntity {

    // Atributos ----
    private String day;
    private Date opening;
    private Date close;

    // Constructor ----

    public Schedule() {
        super();
    }

    @NotNull
    @NotBlank
    @Pattern(regexp = "MONDAY|THURSDAY|WEDNESDAY|TUESDAY|FRIDAY|SATURDAY|SUNDAY")
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @NotNull
    @Temporal(TemporalType.TIME)
    @DateTimeFormat(pattern = "HH:mm")
    public Date getOpening() {
        return opening;
    }

    public void setOpening(Date opening) {
        this.opening = opening;
    }

    @NotNull
    @Temporal(TemporalType.TIME)
    @DateTimeFormat(pattern = "HH:mm")
    public Date getClose() {
        return close;
    }

    public void setClose(Date close) {
        this.close = close;
    }

    //Relaciones

    private Restaurant restaurant;

    @ManyToOne(optional = false)
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
