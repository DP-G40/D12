package forms;

import domain.Creditcard;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class AcceptBillForm {

    private Integer billId;

    private Creditcard creditCard;

    @NotNull
    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    @Valid
    public Creditcard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(Creditcard creditCard) {
        this.creditCard = creditCard;
    }
}
