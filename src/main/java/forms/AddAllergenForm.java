package forms;

import domain.Allergen;

import javax.validation.constraints.NotNull;

public class AddAllergenForm {

    private Allergen allergen;
    private int dishId;

    @NotNull
    public Allergen getAllergen() {
        return allergen;
    }

    public void setAllergen(Allergen allergen) {
        this.allergen = allergen;
    }

    @NotNull
    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }
}
