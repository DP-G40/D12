package forms;

import domain.Creditcard;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ConfirmOrderForm {

    private Integer orderId;

    private Creditcard creditCard;

    private String address;

    private String postalAddress;

    private String offer;

    @NotNull
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @NotNull
    @Valid
    public Creditcard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(Creditcard creditCard) {
        this.creditCard = creditCard;
    }

    @NotBlank
    @NotNull
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @NotBlank
    @NotNull
    @Pattern(regexp = "^\\d{5}$")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }


    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }
}
