package forms;

import domain.Tag;

import javax.validation.constraints.NotNull;

public class TagRestaurantForm {

    private Tag tag;

    @NotNull
    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
