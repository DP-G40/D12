package forms;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class TemplateForm {

    private String title;
    private String body;
    private Boolean active = false;
    private String type;

    @NotNull
    @NotBlank
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotNull
    @NotBlank
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    @NotNull
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @NotNull
    @NotBlank
    @Pattern(regexp = "COMPLETED|DELIVERING|REJECTED|COOKING")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
