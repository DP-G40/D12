package repositories;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Administrator;
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {
	
	//Dashboard arreglada
		
		//1
		@Query("select min(r.orders.size),avg(r.orders.size),max(r.orders.size) from Restaurant r")
		List<Object> Q1();
		
		//2
		@Query("select r from Restaurant r join r.comments c group by c.commentable.id order by sum(c.mark) desc")
		List<Object> Q2(Pageable pageable);
		
		//3
		@Query("select d from Dish d join d.comments c group by c.commentable.id order by sum(c.mark) desc")
		List<Object> Q3(Pageable pageable);
		
		//4 
		@Query("select r from Restaurant r join r.comments c group by c.commentable.id order by sum(c.mark) desc")
		List<Object> Q4(Pageable pageable);
		
		//5 
		@Query("select min(r.comments.size),avg(r.comments.size),max(r.comments.size) from Restaurant r")
		List<Object> Q5();
		
		//6
		@Query("select min(c.orders.size),avg(c.orders.size),max(c.orders.size) from Customer c")
		List<Object> Q6();
		
		//7.1
		@Query("select c from Customer c where c.orders.size>=ALL(select max(cc.orders.size) from Customer cc)")
		List<Object> Q7();
		
		//7.2
		@Query("select c from Customer c where c.orders.size<=ALL(select min(cc.orders.size) from Customer cc)")
		List<Object> Q7_2();
		
		//8.1
		@Query("select e from Evaluator e where e.reviews.size>=ALL(select max(ee.reviews.size) from Evaluator ee)")
		List<Object> Q8();
		
		//8.2
		@Query("select e from Evaluator e where e.reviews.size<=ALL(select min(ee.reviews.size) from Evaluator ee)")
		List<Object> Q8_2();
		
		//9
		@Query("select d from Order o join o.dishes d group by d.id having count(o)>=ALL(select count(o) from Order oo join oo.dishes dd group by dd.id)")
		List<Object> Q9(Pageable pageable);
		
		//10
		@Query("select c from Customer c join c.orders o where o.creationDate > ?1 and o.creationDate < ?2 order by count(o.id)")
		List<Object> Q10(Date d1, Date d2, Pageable pageable);
		
		//11
		@Query("select r from Restaurant r join r.orders o where o.creationDate > ?1 and o.creationDate < ?2 order by count(o.id)")
		List<Object> Q11(Date d1, Date d2, Pageable pageable);
		
		@Query("select r from Restaurant r join r.orders o where o.creationDate > ?1 and o.creationDate < ?2 order by sum(o.orderBill.price)")
		List<Object> Q12(Date d1, Date d2, Pageable pageable);

}
