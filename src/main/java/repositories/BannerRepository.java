package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Announcement;
import domain.Banner;
import domain.Video;
@Repository
public interface BannerRepository extends JpaRepository<Banner, Integer> {

	@Query("select a from Banner a where a.restaurant.id = ?1 and a.endDate > ?2 and a.startDate < ?3")
	List<Banner> getAnnouncementsByRestaurants(Integer restaurant, Date from, Date until);
	
	@Query("select a from Banner a where a.restaurant.id = ?")
	List<Banner> getAnnouncementsByRestaurant(Integer restaurant);
	
	@Query("select a from Banner a where a.endDate > ?1 and a.startDate < ?1")
	List<Banner> getAnnouncements(Date from);

}
