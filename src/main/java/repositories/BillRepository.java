package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Banner;
import domain.Bill;
@Repository
public interface BillRepository extends JpaRepository<Bill, Integer> {

	@Query("select a from Bill a where a.creationDate > ?1 and a.creationDate < ?2")
	List<Bill> getBillByDates(Date from, Date until);

	@Query("select b from Bill b where b.restaurant.id = ?1 ")
	Collection<Bill> findBillByRestaurant(Integer restaurantId);

	@Query("select b from Bill b where b.restaurant.id = ?1 and b.creationDate > ?2")
	Collection<Bill> getBillsByRestaurantAndYear(int id, Date yearFirstDay);

}
