package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Category;
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query("select c from Category c where c.father = null")
    Category findCategoryFather();

    @Query("select c from Category c where c.father.id=?1")
    List<Category> getChildrens(int categoryId);

    @Query("select c from Category c where c.father.id=?1 and c.name like %?2%")
    List<Category> getChildrens(int categoryId, String name);

    @Query("select t from Category t where t.name=?1")
    Category findByName(String name);


}
