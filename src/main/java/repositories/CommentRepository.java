package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Comment;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	@Query("select c from Comment c where c.commentable.id = ?1")
	List<Comment> getCommentsByIdCommentable(Integer id, Pageable pageable);
	
	@Query("select c from Comment c where c.commentable.id = ?1")
	List<Comment> getCommentsByIdCommentable(Integer id);

}
