package repositories;

import domain.Commentable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentableRepository extends JpaRepository<Commentable, Integer> {


    @Query("select c from Commentable c where c.id =?1")
    Commentable findById(Integer commentableId);
}
