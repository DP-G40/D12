package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.DeliveryMan;
@Repository
public interface DeliveryManRepository extends JpaRepository<DeliveryMan, Integer> {

	@Query("select d from DeliveryMan d where d.restaurant.id = ?1 and active=true")
	List<DeliveryMan> findByRestaurantId(Integer id);

}
