package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Dish;
@Repository
public interface DishRepository extends JpaRepository<Dish, Integer> {

    @Query("select a from Dish a where a.restaurant.id = ?1")
    Collection<Dish> findAllByRestaurant(int restaurantId);

    @Query("select a from Dish a where a.restaurant.id = ?1 and a.del=false")
    Collection<Dish> findActivesByRestaurant(int restaurantId);

    @Query("select a from Dish a where a.restaurant.id = ?1 and a.del=true")
    Collection<Dish> findDeletedByRestaurant(int restaurantId);

    @Query("select d from Dish d where d.restaurant.id=?1 and d.del=false")
    Collection<Dish> findDishByRestaurant(Integer restaurantID);

    @Query("select d from Dish d where d.category.id=?1")
    Collection<Dish> findByCategory(Integer categoryId);

}
