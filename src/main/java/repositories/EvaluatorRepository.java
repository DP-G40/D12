package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Evaluator;
@Repository
public interface EvaluatorRepository extends JpaRepository<Evaluator, Integer> {



}
