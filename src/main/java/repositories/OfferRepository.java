package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Offer;
@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer> {

    @Query("select o from Offer o where o.restaurant.id = ?1 and o.code = ?2")
    Offer findByCodeAndRestaurant(Integer restaurantId, String code);
    
    @Query("select o from Offer o where o.restaurant.id = ?1")
    List<Offer> findByRestaurant(Integer restaurantId);
	
	@Query("select  o  from Offer o where o.promotion = ?1")
	List<Offer> getPromotionsByCode(String promotion);
	
	@Query("select  distinct(o.promotion) from Offer o")
	List<String> getPromotionsCode();

}
