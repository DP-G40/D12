package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Bill;
import domain.OrderBill;
@Repository
public interface OrderBillRepository extends JpaRepository<OrderBill, Integer> {

	@Query("select b from OrderBill b where b.order.creationDate > ?1")
	List<OrderBill> getBillsByYear(Date startYear);	

}
