package repositories;

import domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;

@Repository
public interface OrderRepository extends JpaRepository<Order,Integer> {

    @Query("select o from Order o where o.reviewer.id = ?1 and o.restaurant.id = ?2 and o.status = 'DRAFT'")
    Order findOrderByReviewerAndRestaurantDraft(Integer customerId, Integer restaurantId);

    @Query("select o from Order o where o.reviewer.id = ?1 and o.status = ?2 and o.dishes.size > 0")
    Collection<Order> getOrdersByReviewerAndStatus(Integer reviewerId, String status);

    @Query("select o from Order o where o.reviewer.id = ?1 and o.restaurant.id = ?2 and o.status = 'COMPLETED'")
    Collection<Order> findOrderByReviewerAndRestaurant(Integer customerId, Integer restaurantId);

    @Query("select o from Order o join o.dishes d where o.reviewer.id = ?1 and o.status = 'COMPLETED' and d.id = ?2")
    Collection<Order> findOrderByReviewerAndDish(Integer customerId, Integer dishId);
    
    @Query("select o from Order o where o.restaurant.id = ?1 and o.status = ?2")
    Collection<Order> getOrdersByRestaurantAndStatus(Integer restaurantId, String status);
    
    @Query("select o from Order o where o.deliveryMan.id = ?1 and o.status = ?2")
    Collection<Order> getOrdersByDeliveryManAndStatus(Integer deliveryManId, String status);

    @Query("select o from Order o where o.reviewer.id = ?1 and o.status = 'COMPLETED' and o.review is null")
    Collection<Order> findByEvaluatorWithNoReview(Integer evaluatorId);
}
