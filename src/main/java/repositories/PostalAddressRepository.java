package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.PostalAddress;
@Repository
public interface PostalAddressRepository extends JpaRepository<PostalAddress, Integer> {

    @Query("select p from PostalAddress p where p.restaurant.id = ?1 and p.postal = ?2")
    PostalAddress findByRestaurantAndAddress(Integer restaurantId, String address);


}
