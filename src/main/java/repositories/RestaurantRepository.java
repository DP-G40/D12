package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import domain.Restaurant;
@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

	@Query("select r from Restaurant r where r.banned=true")
	List<Restaurant> getRestaurantsBanned();
	
	@Query("select r from Restaurant r where r.banned=false")
	List<Restaurant> getRestaurantsNoBanned();


	@Query("select distinct(r) from Restaurant r left join r.dishes d left join d.category c left join r.postalAddresses pa left join r.tags t where (r.name LIKE %?1% or d.name LIKE %?1% or c.name LIKE %?1% or pa.postal LIKE %?1% or t.name LIKE %?1%) and r.banned=false")
	List<Restaurant> searchRestaurant(String keyword);


}
