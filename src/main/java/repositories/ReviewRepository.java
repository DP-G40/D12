package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Review;
@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {


    @Query("select r from Review r where r.evaluator.id = ?1")
    Collection<Review> findByEvaluator(Integer evaluatorId);

    @Query("select r from Review r join r.order o where o.restaurant.id = ?1")
    Collection<Review> findByRestaurant(Integer restaurantId);

    @Query("select r from Review r where r.order.creationDate > ?1")
	Collection<Review> getReviewsByYear(Date startYear);



}
