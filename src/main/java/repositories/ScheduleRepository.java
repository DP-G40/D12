package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.PostalAddress;
import domain.Schedule;
@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {

	@Query("select s from Schedule s where s.restaurant.id = ?1 and s.day = ?2")
    Schedule findByRestaurantAndDay(Integer restaurantId, String day);
	
	@Query("select s from Schedule s where s.restaurant.id = ?1")
    Collection<Schedule> findByRestaurant(Integer restaurantId);

}
