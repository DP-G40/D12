package repositories;

import domain.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends JpaRepository<Template, Integer> {

    @Query("select t from Template t where active=true and type='COOKING'")
    Template getTemplateAccepted();

    @Query("select t from Template t where active=true and type='REJECTED'")
    Template getTemplateRejected();

    @Query("select t from Template t where active=true and type='DELIVERING'")
    Template getTemplateDelivering();

    @Query("select t from Template t where active=true and type='COMPLETED'")
    Template getTemplateCompleted();

    @Query("select t from Template t where active=true and type=?1")
    Template getTemplateActiveByType(String type);

}
