package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import repositories.AdministratorRepository;
import domain.Administrator;

@Service
@Transactional
public class AdministratorService {
// Managed repository -----------------------------------------------------
@Autowired
private AdministratorRepository administratorRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public AdministratorService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Administrator create() {
Administrator result;
result = new Administrator();
return result;
}

public Collection<Administrator> findAll() {
Collection<Administrator> result;
Assert.notNull(administratorRepository);
result = administratorRepository.findAll();
Assert.notNull(result);
return result;
}

public Administrator findOne(int administratorId) {
Administrator result;
result = administratorRepository.findOne(administratorId);
return result;
}

public Administrator save(Administrator administrator) {
Assert.notNull(administrator);
Administrator result;
result = administratorRepository.save(administrator);
return result;
}

public void delete(Administrator administrator) {
Assert.notNull(administrator);
Assert.isTrue(administrator.getId() != 0);
administratorRepository.delete(administrator);
}
public List<Object> Q1() {
	return administratorRepository.Q1();
}
public List<Object> Q2(Pageable pageable) {
	return administratorRepository.Q2(pageable);
}
public List<Object> Q3(Pageable pageable) {
	return administratorRepository.Q3(pageable);
}
public List<Object> Q4(Pageable pageable) {
	return administratorRepository.Q4(pageable);
}
public List<Object> Q5() {
	return administratorRepository.Q5();
}
public List<Object> Q6() {
	return administratorRepository.Q6();
}
public Object Q7() {
	return administratorRepository.Q7();
}
public Object Q7_2() {
	return administratorRepository.Q7_2();
}
public Object Q8() {
	return administratorRepository.Q8();
}
public Object Q8_2() {
	return administratorRepository.Q8_2();
}
public List<Object> Q9(Pageable pageable) {
	return administratorRepository.Q9(pageable);
}
public List<Object> Q10(Date d1, Date d2, Pageable pageable) {
	return administratorRepository.Q10(d1, d2, pageable);
}
public List<Object> Q11(Date d1, Date d2, Pageable pageable) {
	return administratorRepository.Q11(d1, d2, pageable);
}
public List<Object> Q12(Date d1, Date d2, Pageable pageable) {
	return administratorRepository.Q12(d1, d2, pageable);
}

// Other business methods -------------------------------------------------



}
