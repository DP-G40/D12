package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;

import repositories.AllegenRepository;
import domain.Allergen;

@Service
@Transactional
public class AllegenService {
// Managed repository -----------------------------------------------------
@Autowired
private AllegenRepository allegenRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public AllegenService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Allergen create() {
Allergen result;
result = new Allergen();
return result;
}

public Collection<Allergen> findAll() {
Collection<Allergen> result;
Assert.notNull(allegenRepository);
result = allegenRepository.findAll();
Assert.notNull(result);
return result;
}

public Allergen findOne(int allegenId) {
Allergen result;
result = allegenRepository.findOne(allegenId);
return result;
}

public Allergen save(Allergen allergen) {
Assert.notNull(allergen);
Allergen result;
result = allegenRepository.save(allergen);
return result;
}

public void delete(Allergen allergen) {
Assert.notNull(allergen);
Assert.isTrue(allergen.getId() != 0);
allegenRepository.delete(allergen);
}

// Other business methods -------------------------------------------------
}
