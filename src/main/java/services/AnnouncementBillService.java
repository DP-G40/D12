package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.AnnouncementBillRepository;
import domain.AnnouncementBill;

@Service
@Transactional
public class AnnouncementBillService {
// Managed repository -----------------------------------------------------
@Autowired
private AnnouncementBillRepository announcementbillRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public AnnouncementBillService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public AnnouncementBill create() {
AnnouncementBill result;
result = new AnnouncementBill();
return result;
}

public Collection<AnnouncementBill> findAll() {
Collection<AnnouncementBill> result;
Assert.notNull(announcementbillRepository);
result = announcementbillRepository.findAll();
Assert.notNull(result);
return result;
}

public AnnouncementBill findOne(int announcementbillId) {
AnnouncementBill result;
result = announcementbillRepository.findOne(announcementbillId);
return result;
}

public AnnouncementBill save(AnnouncementBill announcementbill) {
Assert.notNull(announcementbill);
AnnouncementBill result;
result = announcementbillRepository.save(announcementbill);
return result;
}

public void delete(AnnouncementBill announcementbill) {
Assert.notNull(announcementbill);
Assert.isTrue(announcementbill.getId() != 0);
announcementbillRepository.delete(announcementbill);
}

// Other business methods -------------------------------------------------
}
