package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;

import repositories.AnnouncementRepository;
import domain.Announcement;

@Service
@Transactional
public class AnnouncementService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private AnnouncementRepository announcementRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public AnnouncementService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------


    public Collection<Announcement> findAll() {
        Collection<Announcement> result;
        Assert.notNull(announcementRepository);
        result = announcementRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Announcement findOne(int announcementId) {
        Announcement result;
        result = announcementRepository.findOne(announcementId);
        return result;
    }

    public Announcement save(Announcement announcement) {
        Assert.notNull(announcement);
        Announcement result;
        result = announcementRepository.save(announcement);
        return result;
    }

    public void delete(Announcement announcement) {
        Assert.notNull(announcement);
        Assert.isTrue(announcement.getId() != 0);
        announcementRepository.delete(announcement);
    }

// Other business methods -------------------------------------------------
}
