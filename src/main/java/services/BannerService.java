package services;

import domain.Banner;
import forms.BannerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.BannerRepository;

import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BannerService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private BannerRepository bannerRepository;
// Suporting repository --------------------------------------------------

    @Autowired
    private ActorService actorService;

    @Autowired
    private RestaurantService restaurantService;

    // Constructors -----------------------------------------------------------
    public BannerService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Banner create() {
        Banner result;
        result = new Banner();
        result.setRestaurant(restaurantService.findOne(actorService.findByPrincipal().getId()));
        return result;
    }

    public Collection<Banner> findAll() {
        Collection<Banner> result;
        Assert.notNull(bannerRepository);
        result = bannerRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Banner findOne(int bannerId) {
        Banner result;
        result = bannerRepository.findOne(bannerId);
        return result;
    }

    public Banner save(Banner banner) {
        Assert.notNull(banner);
        Banner result;
        result = bannerRepository.save(banner);
        return result;
    }

    public void delete(Banner banner) {
        Assert.notNull(banner);
        Assert.isTrue(banner.getId() != 0);
        bannerRepository.delete(banner);
    }

    public List<Banner> getAnnouncementsByRestaurants(Integer restaurant, Date from, Date until) {
        return bannerRepository.getAnnouncementsByRestaurants(restaurant, from, until);
    }

    public List<Banner> getAnnouncementsByRestaurant(Integer restaurant) {
        return bannerRepository.getAnnouncementsByRestaurant(restaurant);
    }

    public void reconstructAndsave(BannerForm bannerForm) throws IOException, SerialException, SQLException {
        Banner banner = this.create();

        byte[] imgByte = bannerForm.getFile().getBytes();
        banner.setImage(new javax.sql.rowset.serial.SerialBlob(imgByte));
        banner.setUrl("http://www.acme-foodinhome.com/");
        banner.setStartDate(bannerForm.getStartDate());
        banner.setEndDate(bannerForm.getEndDate());

        this.save(banner);
    }

	public List<Banner> getAnnouncements(Date from) {
		return bannerRepository.getAnnouncements(from);
	}

// Other business methods -------------------------------------------------
    
    

}
