package services;

import domain.*;
import forms.AcceptBillForm;
import forms.CancelBillForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.BillRepository;

import java.util.*;

@Service
@Transactional
public class BillService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private BillRepository billRepository;
    // Suporting repository --------------------------------------------------
    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private AnnouncementService announcementService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private BannerService bannerService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private AnnouncementBillService announcementBillService;

    @Autowired
    private ActorService actorService;

    // Constructors -----------------------------------------------------------
    public BillService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Bill create() {
        Bill result;
        result = new Bill();
        return result;
    }

    public Collection<Bill> findAll() {
        Collection<Bill> result;
        Assert.notNull(billRepository);
        result = billRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Bill findOne(int billId) {
        Bill result;
        result = billRepository.findOne(billId);
        Assert.notNull(result);
        return result;
    }

    public Bill save(Bill bill) {
        Assert.notNull(bill);
        Bill result;
        result = billRepository.save(bill);
        return result;
    }

    public void delete(Bill bill) {
        Assert.notNull(bill);
        Assert.isTrue(bill.getId() != 0);
        billRepository.delete(bill);
    }


    public void generateBill(Date date, Date date2) {

        Collection<Restaurant> restaurants = restaurantService.findAll();

        for (Restaurant r : restaurants) {
            Bill bill = this.create();
            Configuration configuration = configurationService.findAll().iterator().next();
            bill.setMonthlyFee(configuration.getSystemPrice());
            bill.setAnnouncementFee(0.0);
            bill.setRestaurant(r);
            bill.setStatus("PENDING");
            bill.setCreationDate(new Date());
            bill.setTicker(this.generateTicker(r));
            List<AnnouncementBill> announcementsBills = new ArrayList<AnnouncementBill>();

            List<Video> videosAnnouncements = videoService.getAnnouncementsByRestaurants(r.getId(), date, date2);
            for (Video v : videosAnnouncements) {
                AnnouncementBill aBill = announcementBillService.create();
                aBill.setAnnouncement(v);
                aBill.setBill(bill);
                aBill.setPriceDay(configuration.getVideoPrice());
                Date fechaPrimera = v.getStartDate();
                if (fechaPrimera.before(date)) {
                    fechaPrimera = date;
                }
                Date fechaSegunda = v.getEndDate();
                if (fechaSegunda.after(date2)) {
                    fechaSegunda = date2;
                }
                int dias = (int) ((fechaSegunda.getTime() - fechaPrimera.getTime()) / 86400000);
                aBill.setDays(dias);
                announcementsBills.add(aBill);

                Double enounncementFee = bill.getAnnouncementFee();
                if (enounncementFee == null) {
                    enounncementFee = 0.0;
                }
                enounncementFee += (aBill.getPriceDay() * aBill.getDays());
                bill.setAnnouncementFee(enounncementFee);
            }

            List<Banner> bannerAnnouncements = bannerService.getAnnouncementsByRestaurants(r.getId(), date, date2);
            for (Banner b : bannerAnnouncements) {
                AnnouncementBill aBill = announcementBillService.create();
                aBill.setAnnouncement(b);
                aBill.setBill(bill);
                aBill.setPriceDay(configuration.getVideoPrice());
                Date fechaPrimera = b.getStartDate();
                if (fechaPrimera.before(date)) {
                    fechaPrimera = date;
                }
                Date fechaSegunda = b.getEndDate();
                if (fechaSegunda.after(date2)) {
                    fechaSegunda = date2;
                }
                int dias = (int) ((fechaSegunda.getTime() - fechaPrimera.getTime()) / 86400000);
                aBill.setDays(dias);
                announcementsBills.add(aBill);

                Double enounncementFee = bill.getAnnouncementFee();
                if (enounncementFee == null) {
                    enounncementFee = 0.0;
                }
                enounncementFee += (aBill.getPriceDay() * aBill.getDays());
                bill.setAnnouncementFee(enounncementFee);
            }

            this.save(bill);
        }
    }

    private String generateTicker(Restaurant r) {
		String res="";
		try {
			res = res + r.getCIF();
			
			res = res + "-";
			
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			String yearInString = String.valueOf(year);
			res = res + yearInString;
			
			res = res + "-";
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, 2014);
			cal.set(Calendar.DAY_OF_YEAR, 1);    
			Date startYear = cal.getTime();
			res = res + (billRepository.getBillsByRestaurantAndYear(r.getId(), startYear).size()+1);
		} catch(Exception e) {
			res="CIF-YYYY-NNNN";
		}
		
		return res;
	}

	public List<Bill> getBillByDates(Date from, Date until) {
        return billRepository.getBillByDates(from, until);
    }

    public Bill reconstructAccept(AcceptBillForm acceptBillForm) {
        Bill res = findOne(acceptBillForm.getBillId());
        isMine(res);

        Calendar time = Calendar.getInstance();
        Integer mes = time.get(Calendar.MONTH) + 1;
        Integer anio = time.get(Calendar.YEAR);
        Assert.isTrue((acceptBillForm.getCreditCard().getExpiryYear().equals(anio) && (acceptBillForm.getCreditCard().getExpiryMonth().compareTo(mes) >= 1)) || (acceptBillForm.getCreditCard().getExpiryYear() > anio), "creditcard.date.error");

        res.setCreditcard(acceptBillForm.getCreditCard());

        res.setStatus("PAID");
        return res;
    }

    public void isMine(Bill bill) {
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Assert.isTrue(bill.getRestaurant().getId() == restaurant.getId());
    }

    public void isMine(Integer billId) {
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Bill bill = findOne(billId);
        Assert.isTrue(bill.getRestaurant().getId() == restaurant.getId());
    }

    public Bill reconstructCancel(CancelBillForm cancelBillForm){
        Bill res = findOne(cancelBillForm.getBillId());
        isMine(res);
        res.setReason(cancelBillForm.getReason());
        res.setStatus("REJECTED");
        return res;
    }

    public Collection<Bill> findBillByRestaurant(){
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Collection<Bill> res = billRepository.findBillByRestaurant(restaurant.getId());
        Assert.notNull(res);
        return res;
    }


}
