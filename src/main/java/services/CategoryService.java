package services;

import domain.Category;
import forms.CategoryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CategoryRepository;

import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CategoryService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private CategoryRepository categoryRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public CategoryService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Category create() {
        Category result;
        result = new Category();
        return result;
    }

    public Category create(Integer categoryId){
        Category result;
        result = new Category();
        Category father = this.findOne(categoryId);
        result.setFather(father);
        return result;
    }

    public Collection<Category> findAll() {
        Collection<Category> result;
        Assert.notNull(categoryRepository);
        result = categoryRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Category findOne(int categoryId) {
        Category result;
        Assert.notNull(categoryId);
        result = categoryRepository.findOne(categoryId);
        Assert.notNull(result);
        return result;
    }

    public Category findByName(String name){
        return this.categoryRepository.findByName(name);
    }

    public Category save(Category category) {
        Assert.notNull(category);
        Category result;
        //FIXME Aqui peta una cosa un poco... porque s� xD
//        Assert.isNull(this.findByName(category.getName()),"category.name.error");
        result = categoryRepository.save(category);
        return result;
    }

    public void delete(Category category) {
        Assert.notNull(category);
        Assert.isTrue(category.getId() != 0);
        categoryRepository.delete(category);
    }

// Other business methods -------------------------------------------------

    public Category findCategoryFather(){
        Category result;
        result = categoryRepository.findCategoryFather();
        Assert.notNull(result);
        return result;
    }

    public List<Category> getChildrens(int categoryId) {
        return categoryRepository.getChildrens(categoryId);
    }

    public List<Category> getChildrens(int categoryId, String name) {
        return categoryRepository.getChildrens(categoryId, name);
    }

    public CategoryForm construct(Category category){
        CategoryForm res = new CategoryForm();
        res.setName(category.getName());
        res.setFather(category.getFather());
        res.setCategoryId(category.getId());
        return res;
    }

    public Category reconstruct(CategoryForm categoryForm){
        Category res;
        if (categoryForm.getCategoryId() == 0){
            res = this.create();
        } else {
            res = this.findOne(categoryForm.getCategoryId());
        }
        res.setName(categoryForm.getName());
        res.setFather(categoryForm.getFather());
        return res;
    }

    public Category flush(Category category){
        return categoryRepository.saveAndFlush(category);
    }


}
