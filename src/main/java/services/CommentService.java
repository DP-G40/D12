package services;

import domain.Comment;
import domain.Reviewer;
import forms.CommentForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CommentRepository;

import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CommentService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private CommentableService commentableService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public CommentService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Comment create() {
        Comment result;
        result = new Comment();
        result.setReviewer((Reviewer) actorService.findByPrincipal());
        return result;
    }

    public Collection<Comment> findAll() {
        Collection<Comment> result;
        Assert.notNull(commentRepository);
        result = commentRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Comment findOne(int commentId) {
        Comment result;
        result = commentRepository.findOne(commentId);
        return result;
    }

    public Comment save(Comment comment) {
        Assert.notNull(comment);
        Comment result;
        result = commentRepository.save(comment);
        return result;
    }

    public void delete(Comment comment) {
        Assert.notNull(comment);
        Assert.isTrue(comment.getId() != 0);
        commentRepository.delete(comment);
    }

    public Double getMedia(int id) {
        List<Comment> comments = commentRepository.getCommentsByIdCommentable(id, new PageRequest(0, 3));
        Double total = 0.0;
        for (Comment c : comments) {
            total += c.getMark();
        }
        return ((total * 1.0) / (comments.size() * 1.0));
    }

// Other business methods -------------------------------------------------

    public void reconstruct(CommentForm commentForm) {
        Comment res = this.create();
        res.setCommentable(commentableService.findOne(commentForm.getCommentableId()));
        res.setTitle(commentForm.getTitle());
        res.setDescription(commentForm.getDescription());
        res.setMark(commentForm.getMark());
        save(res);
    }

    public Collection<Comment> findByRestaurant(Integer restaurantId) {
        return commentRepository.getCommentsByIdCommentable(restaurantId);
    }


    public Collection<Comment> findCommentforRestaurant(Integer restaurantId) {
        return commentRepository.getCommentsByIdCommentable(restaurantId);
    }




}


