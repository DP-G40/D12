package services;

import domain.Commentable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CommentableRepository;

import java.util.Collection;

@Service
@Transactional
public class CommentableService {

    @Autowired
    private CommentableRepository commentableRepository;

    public Commentable findOne(Integer commentableId){
        Assert.notNull(commentableId);
        Commentable res = commentableRepository.findById(commentableId);
        Assert.notNull(res);
        return res;
    }

    public Collection<Commentable> findAll(){
        Collection<Commentable> res = commentableRepository.findAll();
        Assert.notNull(res);
        return res;
    }
}
