package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.ConfigurationRepository;
import domain.Configuration;

@Service
@Transactional
public class ConfigurationService {
// Managed repository -----------------------------------------------------
@Autowired
private ConfigurationRepository configurationRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public ConfigurationService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Configuration create() {
Configuration result;
result = new Configuration();
return result;
}

public Collection<Configuration> findAll() {
Collection<Configuration> result;
Assert.notNull(configurationRepository);
result = configurationRepository.findAll();
Assert.notNull(result);
return result;
}

public Configuration findOne(int configurationId) {
Configuration result;
result = configurationRepository.findOne(configurationId);
return result;
}

public Configuration save(Configuration configuration) {
Assert.notNull(configuration);
Configuration result;
result = configurationRepository.save(configuration);
return result;
}

public void delete(Configuration configuration) {
Assert.notNull(configuration);
Assert.isTrue(configuration.getId() != 0);
configurationRepository.delete(configuration);
}

// Other business methods -------------------------------------------------
}
