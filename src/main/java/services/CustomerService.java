package services;

import domain.Customer;
import domain.Restaurant;
import forms.EditCustomerForm;
import forms.EditRestaurantForm;
import forms.RegisterCustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CustomerRepository;
import security.Authority;
import security.UserAccount;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import javax.sql.rowset.serial.SerialException;

@Service
@Transactional
public class CustomerService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ActorService actorService;

// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public CustomerService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Customer create() {
        Customer result;
        result = new Customer();
        UserAccount userAccount = new UserAccount();
        Authority authority = new Authority();
        authority.setAuthority(Authority.CUSTOMER);
        userAccount.getAuthorities().add(authority);
        result.setUserAccount(userAccount);
        return result;
    }

    public Collection<Customer> findAll() {
        Collection<Customer> result;
        Assert.notNull(customerRepository);
        result = customerRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Customer findOne(int customerId) {
        Customer result;
        result = customerRepository.findOne(customerId);
        return result;
    }

    public Customer save(Customer customer) {
        Assert.notNull(customer);
        Customer result;
        result = customerRepository.save(customer);
        return result;
    }

    public void delete(Customer customer) {
        Assert.notNull(customer);
        Assert.isTrue(customer.getId() != 0);
        customerRepository.delete(customer);
    }

// Other business methods -------------------------------------------------

    public Customer reconstruct(RegisterCustomerForm registerCustomerForm) throws IOException, SerialException, SQLException{
        Customer res = create();
        Assert.isTrue(
                registerCustomerForm.getPassword().equals(
                        registerCustomerForm.getRepeatPassword()),
                "register.error.password");
        Assert.isNull(actorService.getByUserName(registerCustomerForm
                .getUserName()), "register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        res.getUserAccount().setPassword(md5.encodePassword(
                registerCustomerForm.getPassword(), null));
        res.getUserAccount().setUsername(registerCustomerForm.getUserName());
        res.setName(registerCustomerForm.getName());
        res.setSurname(registerCustomerForm.getSurname());
        res.setPhone(registerCustomerForm.getPhone());
        res.setEmail(registerCustomerForm.getEmail());
        res.setAddress(registerCustomerForm.getAddress());
        byte[] imgByte = registerCustomerForm.getFile().getBytes();
    	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        return res;

    }
    
    public EditCustomerForm construct(Customer customer) {
    	EditCustomerForm editCustomerForm = new EditCustomerForm();
    	editCustomerForm.setAddress(customer.getAddress());
		editCustomerForm.setEmail(customer.getEmail());
		editCustomerForm.setName(customer.getName());
		editCustomerForm.setPhone(customer.getPhone());
		editCustomerForm.setSurname(customer.getSurname());
		
		return editCustomerForm;
	}

	public Customer reconstructEdit(EditCustomerForm editCustomerForm) throws SerialException, SQLException, IOException {
		Customer res = this.findOne(actorService.findByPrincipal().getId());
        
        res.setName(editCustomerForm.getName());
        res.setSurname(editCustomerForm.getSurname());
        res.setPhone(editCustomerForm.getPhone());
        res.setEmail(editCustomerForm.getEmail());
        res.setAddress(editCustomerForm.getAddress());
        
        if(editCustomerForm.getFile() != null) {
        	byte[] imgByte = editCustomerForm.getFile().getBytes();
        	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        }
        return res;
	}
}
