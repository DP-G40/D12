package services;

import domain.Customer;
import domain.DeliveryMan;
import domain.Restaurant;
import forms.EditCustomerForm;
import forms.RegisterCustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;
import repositories.DeliveryManRepository;
import security.Authority;
import security.UserAccount;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.sql.rowset.serial.SerialException;

@Service
@Transactional
public class DeliveryManService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private DeliveryManRepository deliverymanRepository;

    @Autowired
    private ActorService actorService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public DeliveryManService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public DeliveryMan create() {
        DeliveryMan result;
        result = new DeliveryMan();
        UserAccount userAccount = new UserAccount();
        Authority authority = new Authority();
        authority.setAuthority(Authority.DELIVERYMAN);
        userAccount.getAuthorities().add(authority);
        result.setUserAccount(userAccount);
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        result.setRestaurant(restaurant);
        return result;
    }

    public Collection<DeliveryMan> findAll() {
        Collection<DeliveryMan> result;
        Assert.notNull(deliverymanRepository);
        result = deliverymanRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public DeliveryMan findOne(int deliverymanId) {
        DeliveryMan result;
        result = deliverymanRepository.findOne(deliverymanId);
        Assert.notNull(deliverymanId);
        return result;
    }

    public DeliveryMan save(DeliveryMan deliveryman) {
        Assert.notNull(deliveryman);
        DeliveryMan result;
        result = deliverymanRepository.save(deliveryman);
        return result;
    }

    public void delete(DeliveryMan deliveryman) {
        Assert.notNull(deliveryman);
        Assert.isTrue(deliveryman.getId() != 0);
        deliverymanRepository.delete(deliveryman);
    }


    public List<DeliveryMan> findByRestaurantId(Integer id) {
        return deliverymanRepository.findByRestaurantId(id);
    }

// Other business methods -------------------------------------------------

    public DeliveryMan reconstruct(RegisterCustomerForm registerCustomerForm) throws IOException, SerialException, SQLException {
        DeliveryMan res = create();
        Assert.isTrue(
                registerCustomerForm.getPassword().equals(
                        registerCustomerForm.getRepeatPassword()),
                "register.error.password");
        Assert.isNull(actorService.getByUserName(registerCustomerForm
                .getUserName()), "register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        res.getUserAccount().setPassword(md5.encodePassword(
                registerCustomerForm.getPassword(), null));
        res.getUserAccount().setUsername(registerCustomerForm.getUserName());
        res.setName(registerCustomerForm.getName());
        res.setSurname(registerCustomerForm.getSurname());
        res.setPhone(registerCustomerForm.getPhone());
        res.setEmail(registerCustomerForm.getEmail());
        res.setAddress(registerCustomerForm.getAddress());
        byte[] imgByte = registerCustomerForm.getFile().getBytes();
    	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        return res;

    }

    public void fired(Integer deliveryId){
        DeliveryMan deliveryMan = findOne(deliveryId);
        isMine(deliveryMan);
        deliveryMan.getUserAccount().setEnabled(false);
        deliveryMan.setActive(false);
        save(deliveryMan);
    }
    public EditCustomerForm construct(DeliveryMan deliveryMan) {
    	EditCustomerForm editCustomerForm = new EditCustomerForm();
    	editCustomerForm.setAddress(deliveryMan.getAddress());
		editCustomerForm.setEmail(deliveryMan.getEmail());
		editCustomerForm.setName(deliveryMan.getName());
		editCustomerForm.setPhone(deliveryMan.getPhone());
		editCustomerForm.setSurname(deliveryMan.getSurname());
		
		return editCustomerForm;
	}

    public void isMine(DeliveryMan deliveryMan){
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Assert.isTrue(deliveryMan.getRestaurant().getId() == restaurant.getId());
    }


	public DeliveryMan reconstructEdit(EditCustomerForm editCustomerForm) throws SerialException, SQLException, IOException {
		DeliveryMan res = this.findOne(actorService.findByPrincipal().getId());
        
        res.setName(editCustomerForm.getName());
        res.setSurname(editCustomerForm.getSurname());
        res.setPhone(editCustomerForm.getPhone());
        res.setEmail(editCustomerForm.getEmail());
        res.setAddress(editCustomerForm.getAddress());
        
        if(editCustomerForm.getFile() != null) {
        	byte[] imgByte = editCustomerForm.getFile().getBytes();
        	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        }
        return res;
	}

}
