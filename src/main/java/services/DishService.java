package services;

import domain.Allergen;
import domain.Dish;
import domain.Restaurant;
import forms.AddAllergenForm;
import forms.DishForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.DishRepository;

import java.util.Collection;

@Service
@Transactional
public class DishService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ActorService actorService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public DishService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Dish create() {
        Dish result;
        Restaurant r = (Restaurant) actorService.findByPrincipal();
        result = new Dish();
        result.setRestaurant(r);
        return result;
    }

    public Collection<Dish> findAll() {
        Collection<Dish> result;
        Assert.notNull(dishRepository);
        result = dishRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Dish findOne(int dishId) {
        Dish result;
        result = dishRepository.findOne(dishId);
        Assert.notNull(result);
        return result;
    }

    public Dish save(Dish dish) {
        Assert.notNull(dish);
        Dish result;
        result = dishRepository.save(dish);
        return result;
    }

    public void delete(Dish dish) {
        Assert.notNull(dish);
        Assert.isTrue(dish.getId() != 0);
        dish.setDel(true);
        save(dish);

    }

// Other business methods -------------------------------------------------


    public Collection<Dish> findActivesByRestaurant(int restaurantId){
        Assert.notNull(restaurantId);
        Collection<Dish> res = dishRepository.findActivesByRestaurant(restaurantId);
        return res;
    }

    public Collection<Dish> findDeletedByRestaurant(int restaurantId){
        Assert.notNull(restaurantId);
        Collection<Dish> res = dishRepository.findDeletedByRestaurant(restaurantId);
        return res;
    }

    public Dish reconstruct(DishForm dishForm){
        Dish res;
        if(dishForm.getDishId()==0){
            res  = this.create();
        }else {
            res = findOne(dishForm.getDishId());
        }

        res.setName(dishForm.getName());
        res.setDescription(dishForm.getDescription());
        res.setPrice(dishForm.getPrice());
        res.setCategory(dishForm.getCategory());
        return res;

    }

    public Dish addAllergenToDish(AddAllergenForm addAllergenForm){
        Allergen a = addAllergenForm.getAllergen();
        Dish d = findOne(addAllergenForm.getDishId());

        d.getAllergens().add(a);
        Dish x = save(d);
        return x;
    }

    public DishForm construct(Dish d){
        DishForm res = new DishForm();
        res.setDishId(d.getId());
        res.setName(d.getName());
        res.setDescription(d.getDescription());
        res.setPrice(d.getPrice());
        res.setCategory(d.getCategory());
        return res;
    }


    public Collection<Dish> findbyRestaurant(Integer restaurantId) {
        Assert.notNull(restaurantId);
        return dishRepository.findDishByRestaurant(restaurantId);
    }

    public Collection<Dish> findByCategory(Integer categoryId){
        return dishRepository.findByCategory(categoryId);
    }

}
