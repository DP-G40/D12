package services;

import domain.DeliveryMan;
import domain.Evaluator;
import forms.EditCustomerForm;
import forms.RegisterCustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EvaluatorRepository;
import security.Authority;
import security.UserAccount;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import javax.sql.rowset.serial.SerialException;

@Service
@Transactional
public class EvaluatorService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private EvaluatorRepository evaluatorRepository;

    @Autowired
    private ActorService actorService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public EvaluatorService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Evaluator create() {
        Evaluator result;
        result = new Evaluator();
        UserAccount userAccount = new UserAccount();
        Authority authority = new Authority();
        authority.setAuthority(Authority.EVALUATOR);
        userAccount.getAuthorities().add(authority);
        result.setUserAccount(userAccount);
        return result;
    }

    public Collection<Evaluator> findAll() {
        Collection<Evaluator> result;
        Assert.notNull(evaluatorRepository);
        result = evaluatorRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Evaluator findOne(int evaluatorId) {
        Evaluator result;
        result = evaluatorRepository.findOne(evaluatorId);
        return result;
    }

    public Evaluator save(Evaluator evaluator) {
        Assert.notNull(evaluator);
        Evaluator result;
        result = evaluatorRepository.save(evaluator);
        return result;
    }

    public void delete(Evaluator evaluator) {
        Assert.notNull(evaluator);
        Assert.isTrue(evaluator.getId() != 0);
        evaluatorRepository.delete(evaluator);
    }

// Other business methods -------------------------------------------------

    public Evaluator reconstruct(RegisterCustomerForm registerCustomerForm) throws IOException, SerialException, SQLException {
        Evaluator res = create();
        Assert.isTrue(
                registerCustomerForm.getPassword().equals(
                        registerCustomerForm.getRepeatPassword()),
                "register.error.password");
        Assert.isNull(actorService.getByUserName(registerCustomerForm
                .getUserName()), "register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        res.getUserAccount().setPassword(md5.encodePassword(
                registerCustomerForm.getPassword(), null));
        res.getUserAccount().setUsername(registerCustomerForm.getUserName());
        res.setName(registerCustomerForm.getName());
        res.setSurname(registerCustomerForm.getSurname());
        res.setPhone(registerCustomerForm.getPhone());
        res.setEmail(registerCustomerForm.getEmail());
        res.setAddress(registerCustomerForm.getAddress());
        if(registerCustomerForm.getFile() != null) {
        	byte[] imgByte = registerCustomerForm.getFile().getBytes();
        	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        }
        return res;

    }
    
    public EditCustomerForm construct(Evaluator evaluator) {
    	EditCustomerForm editCustomerForm = new EditCustomerForm();
    	editCustomerForm.setAddress(evaluator.getAddress());
		editCustomerForm.setEmail(evaluator.getEmail());
		editCustomerForm.setName(evaluator.getName());
		editCustomerForm.setPhone(evaluator.getPhone());
		editCustomerForm.setSurname(evaluator.getSurname());
		
		return editCustomerForm;
	}
    
    public Evaluator reconstructEdit(EditCustomerForm editCustomerForm) throws SerialException, SQLException, IOException {
    	Evaluator res = this.findOne(actorService.findByPrincipal().getId());
        
        res.setName(editCustomerForm.getName());
        res.setSurname(editCustomerForm.getSurname());
        res.setPhone(editCustomerForm.getPhone());
        res.setEmail(editCustomerForm.getEmail());
        res.setAddress(editCustomerForm.getAddress());
        
        if(editCustomerForm.getFile() != null) {
        	byte[] imgByte = editCustomerForm.getFile().getBytes();
        	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        }
        return res;
	}
    
    
}
