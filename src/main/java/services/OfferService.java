package services;

import domain.Offer;
import domain.Restaurant;
import forms.OfferForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.OfferRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service
@Transactional
public class OfferService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private OfferRepository offerRepository;
    // Suporting repository --------------------------------------------------
    @Autowired
    private RestaurantService restaurantService;
    
    @Autowired
    private ActorService actorService;

    // Constructors -----------------------------------------------------------
    public OfferService() {
        super();
    }

    @PersistenceContext
    EntityManager entityManager;

    // Simple CRUD methods ----------------------------------------------------
    public Offer create() {
        Offer result;
        result = new Offer();
        result.setCode(this.getActualYear().concat(this.getNextCodeNumber()).concat(this.getRandomLetters()));
        result.setCreationDate(new Date());
        result.setRestaurant((Restaurant) actorService.findByPrincipal());
        result.setPromotion(null);
        return result;
    }


    public Collection<Offer> findAll() {
        Collection<Offer> result;
        Assert.notNull(offerRepository);
        result = offerRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Offer findOne(int offerId) {
        Offer result;
        result = offerRepository.findOne(offerId);
        return result;
    }

    public Offer save(Offer offer) {
        Assert.notNull(offer);
        Offer result;
        result = offerRepository.save(offer);
        return result;
    }

    public void delete(Offer offer) {
        Assert.notNull(offer);
        Assert.isTrue(offer.getId() != 0);
        offerRepository.delete(offer);
    }


    public void savePromotion(OfferForm offer) {
        Date d = new Date();
        Collection<Restaurant> restaurants = restaurantService.findAll();
        for (Restaurant r : restaurants) {
            Offer o = this.reconstructAdmin(offer);
            o.setRestaurant(r);
            o.setPromotion("P-".concat(String.valueOf(d.getTime())));
            this.save(o);
        }
    }

    public void saveEditPromotion(OfferForm offerForm) {
        Collection<Offer> offers = offerRepository.getPromotionsByCode(offerForm.getPromotion());
        for (Offer o : offers) {
            o.setDescription(offerForm.getDescription());
            o.setDiscount(offerForm.getDiscount());
            o.setEndDate(offerForm.getEndDate());
            o.setStartDate(offerForm.getStartDate());
            o.setTitle(offerForm.getTitle());
            this.save(o);
        }
    }

    public Offer reconstructAdmin(OfferForm of) {
        Offer offer = this.createForAdmin();
        offer.setDescription(of.getDescription());
        offer.setDiscount(of.getDiscount());
        offer.setEndDate(of.getEndDate());
        offer.setStartDate(of.getStartDate());
        offer.setTitle(of.getTitle());
        return offer;
    }

    public OfferForm reconstructFormAdmin(Offer of) {
        OfferForm offer = new OfferForm();
        offer.setDescription(of.getDescription());
        offer.setDiscount(of.getDiscount());
        offer.setEndDate(of.getEndDate());
        offer.setStartDate(of.getStartDate());
        offer.setTitle(of.getTitle());
        offer.setPromotion(of.getPromotion());
        return offer;
    }

    @SuppressWarnings("unchecked")
    public List<Offer> getPromotions() {
        //return offerRepository.getPromotions();
        return entityManager
                .createNativeQuery("select * from Offer o where o.id IN (select max(oo.id) from Offer oo where oo.startDate > now() and oo.promotion is not null group by oo.promotion)", Offer.class)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Offer> getPromotionsActive() {
        return entityManager
                .createNativeQuery("select * from Offer o where o.id IN (select max(oo.id) from Offer oo where oo.startDate < now() and oo.promotion is not null group by oo.promotion)", Offer.class)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
    public Offer getPromotionById(String promotion) {
        //return offerRepository.getPromotions();
        List<Offer> res = entityManager
                .createNativeQuery("select * from Offer o where o.id IN (select max(oo.id) from Offer oo where oo.startDate > now() and oo.promotion is not null and oo.promotion='" + promotion + "' group by oo.promotion)", Offer.class)
                .getResultList();
        if (res.size() > 0) {
            return res.get(0);
        } else {
            return null;
        }
    }

    public Offer createForAdmin() {
        Offer result;
        result = new Offer();
        result.setCode(this.getActualYear().concat(this.getNextCodeNumber()).concat("ADM"));
        result.setCreationDate(new Date());
        return result;
    }

    private String getRandomLetters() {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        Random random = new Random();
        String pass = "";
        for (int i = 0; i < 3; i++) {
            pass += alphabet.charAt(random.nextInt(alphabet.length()));
        }
        return pass.substring(0, 3);
    }

    private String getActualYear() {
        return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
    }

    public String getNextCodeNumber() {
        Integer total = this.findAll().size() + 1;
        String ret = "";
        if (total <= 9) {
            ret = ret.concat("0").concat("0").concat(String.valueOf(total));
        } else if (total > 9 && total <= 99) {
            ret = ret.concat("0").concat(String.valueOf(total));
        } else {
            ret = ret.concat(String.valueOf(total));
        }
        return ret.substring(0, 3);
    }

    public List<Offer> getPromotionsByCode(String promotion) {
        return offerRepository.getPromotionsByCode(promotion);
    }
    
    

    public void delete(Iterable<? extends Offer> arg0) {
		offerRepository.delete(arg0);
	}


	public void deletePromotion(String promotion) {
        List<Offer> offers = this.getPromotionsByCode(promotion);
        this.delete(offers);

    }

// Other business methods -------------------------------------------------

    public Offer findByCodeAndRestaurant(Restaurant restaurant, String code) {
        return offerRepository.findByCodeAndRestaurant(restaurant.getId(), code);
    }


	public List<Offer> findByRestaurant(Integer findOne) {
		return offerRepository. findByRestaurant(findOne);
	}


	public Offer reconstruct(OfferForm offerForm){
        Offer res = offerForm.getOfferId() == 0 ? create() : findOne(offerForm.getOfferId());
        res.setDescription(offerForm.getDescription());
        res.setDiscount(offerForm.getDiscount());
        res.setEndDate(offerForm.getEndDate());
        res.setStartDate(offerForm.getStartDate());
        res.setTitle(offerForm.getTitle());
        Assert.isTrue(offerForm.getStartDate().before(offerForm.getEndDate()) || offerForm.getStartDate().equals(offerForm.getEndDate()),"offer.error.date");
        return res;
    }

    public OfferForm construct(Offer offer){
        OfferForm res = new OfferForm();
        res.setOfferId(offer.getId());
        res.setDescription(offer.getDescription());
        res.setDiscount(offer.getDiscount());
        res.setEndDate(offer.getEndDate());
        res.setStartDate(offer.getStartDate());
        res.setTitle(offer.getTitle());
        return res;
    }

    public void isMine(Offer offer){
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Assert.isTrue(restaurant.getId() == offer.getRestaurant().getId());
    }
}
