package services;

import domain.OrderBill;
import domain.Restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.OrderBillRepository;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class OrderBillService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private OrderBillRepository orderbillRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public OrderBillService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public OrderBill create() {
        OrderBill result;
        result = new OrderBill();
        return result;
    }

    public Collection<OrderBill> findAll() {
        Collection<OrderBill> result;
        Assert.notNull(orderbillRepository);
        result = orderbillRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public OrderBill findOne(int orderbillId) {
        OrderBill result;
        result = orderbillRepository.findOne(orderbillId);
        return result;
    }

    public OrderBill save(OrderBill orderbill) {
        Assert.notNull(orderbill);
        OrderBill result;
        orderbill.setTicker(this.generateTicker());
        result = orderbillRepository.save(orderbill);
        return result;
    }

    public void delete(OrderBill orderbill) {
        Assert.notNull(orderbill);
        Assert.isTrue(orderbill.getId() != 0);
        orderbillRepository.delete(orderbill);
    }

// Other business methods -------------------------------------------------
    
    private String generateTicker() {
		String res="";
		
		try {
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			String yearInString = String.valueOf(year);
			res = res + yearInString;
			
			res = res + "-";
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, 2014);
			cal.set(Calendar.DAY_OF_YEAR, 1);    
			Date startYear = cal.getTime();
			res = res + (orderbillRepository.getBillsByYear(startYear).size()+1);
		} catch(Exception e) {
			res="YYYY-NNNN";
		}
		return res;
	}
}
