package services;

import domain.*;
import forms.ConfirmOrderForm;
import org.apache.commons.lang.StringUtils;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.OrderRepository;

import java.util.*;

@Service
@Transactional
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private DishService dishService;

    @Autowired
    private OrderBillService orderBillService;

    @Autowired
    private PostalAddressService postalAddressService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private CommentableService commentableService;

    @Autowired
    private TemplateService templateService;


    public Order create(Restaurant restaurant) {
        Order res = new Order();
        res.setReviewer((Reviewer) actorService.findByPrincipal());
        res.setRestaurant(restaurant);
        res.setOrderBill(orderBillService.create());
        res.setStatus("DRAFT");
        res.setTicker(generateTicker());
        res.setCreationDate(new Date());
        return res;
    }

    public Order findOne(Integer orderId) {
        Assert.notNull(orderId);
        Order res = orderRepository.findOne(orderId);
        Assert.notNull(res);
        return res;
    }

    public Collection<Order> findAll() {
        Collection<Order> res = orderRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public Order save(Order order) {
        Assert.notNull(order);
        order = orderRepository.save(order);
        return order;
    }

    public void delete(Order order) {
        Assert.notNull(order);
        orderRepository.delete(order);
    }

    public Order getOrderByReviewerAndRestaurant(Restaurant restaurant) {
        Order res = findOrderByReviewerAndRestaurantDraft(restaurant.getId());
        if (res == null) {
            res = create(restaurant);
        }
        return res;
    }

    public Order findOrderByReviewerAndRestaurantDraft(Integer restaurantId) {
        return orderRepository.findOrderByReviewerAndRestaurantDraft(actorService.findByPrincipal().getId(), restaurantId);
    }

    public void addDishToOrder(Integer dishId) {
        Dish dish = dishService.findOne(dishId);
        Order order = getOrderByReviewerAndRestaurant(dish.getRestaurant());
        order.getDishes().add(dish);
        calcularPrecioOrder(order);
        save(order);
    }

    public void removeDishToOrder(Integer dishId,Integer orderId) {
        Dish dish = dishService.findOne(dishId);
        Order order = findOne(orderId);
        order.getDishes().remove(dish);
        save(order);
    }

    public Collection<Order> getOrdersByReviewerAndStatus(String status) {
        Collection<Order> res = orderRepository.getOrdersByReviewerAndStatus(actorService.findByPrincipal().getId(), status);
        Assert.notNull(res);
        return res;
    }

    public void confirm(Integer orderId) {
        Order order = findOne(orderId);
        isMine(order);
        Assert.isTrue(order.getStatus().equals("DRAFT"));
        Assert.notEmpty(order.getDishes());
    }

    public void isMine(Order order) {
        Reviewer reviewer = (Reviewer) actorService.findByPrincipal();
        Assert.isTrue(reviewer.getId() == order.getReviewer().getId());
    }

    public void isMineRestaurant(Order order) {
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Assert.isTrue(restaurant.getId() == order.getRestaurant().getId());
    }

    public void isMineDeliveryMan(Order order) {
        DeliveryMan deliveryMan = (DeliveryMan) actorService.findByPrincipal();
        Assert.isTrue(deliveryMan.getRestaurant().getId() == order.getRestaurant().getId());
    }

    public Order reconstruct(ConfirmOrderForm confirmOrderForm) {
        Order res = this.findOne(confirmOrderForm.getOrderId());
        Assert.notNull(postalAddressService.findByRestaurantAndAddress(res.getRestaurant(),confirmOrderForm.getPostalAddress()),"order.postal.error");
        if (StringUtils.isNotBlank(confirmOrderForm.getOffer())) {
            Offer offer = offerService.findByCodeAndRestaurant(res.getRestaurant(), confirmOrderForm.getOffer());
            Assert.notNull(offer, "order.offer.error");
            res.setOffer(offer);
        }
        isMine(res);
        res.setCreditcard(confirmOrderForm.getCreditCard());
        Calendar time = Calendar.getInstance();
        Integer mes = time.get(Calendar.MONTH) + 1;
        Integer anio = time.get(Calendar.YEAR);
        Assert.isTrue((confirmOrderForm.getCreditCard().getExpiryYear().equals(anio) && (confirmOrderForm.getCreditCard().getExpiryMonth().compareTo(mes) >= 1)) || (confirmOrderForm.getCreditCard().getExpiryYear() > anio), "creditcard.date.error");
        res.setStatus("PENDING");
        res.setAddress(confirmOrderForm.getAddress());
        res.setPostalAddress(confirmOrderForm.getPostalAddress());
        return res;
    }

    public void calcularPrecioOrder(Order order){
        Double precio = 0.0;
        if (!order.getDishes().isEmpty()) {
            for (Dish dish : order.getDishes()) {
                precio += dish.getPrice();
            }

            PostalAddress postalAddress = postalAddressService.findByRestaurantAndAddress(order.getRestaurant(), order.getPostalAddress());

            if (postalAddress != null) {
                precio += postalAddress.getPrice();
            }
            precio += order.getRestaurant().getDeliveryPrice();

            if (order.getOffer() != null) {
                precio *= 1 - order.getOffer().getDiscount() / 100.0;
            }
        }
        order.getOrderBill().setPrice(precio);
    }

    public void cancel(Integer orderId) {
        Order order = findOne(orderId);
        isMine(order);
        Assert.isTrue(order.getStatus().equals("DRAFT") || order.getStatus().equals("PENDING"));
        order.setStatus("REJECTED");
        save(order);
    }

    public Collection<Order> findOrderByCommentable(Integer commentableId){
        Commentable commentable = commentableService.findOne(commentableId);
        Collection<Order> res = new ArrayList<>();
        if (commentable instanceof Restaurant){
            res = findOrderByReviewerAndRestaurant(commentableId);
        } else if (commentable instanceof Dish){
            res = findOrderByReviewerAndDish(commentableId);
        }
        return res;
    }


    public Collection<Order> findOrderByReviewerAndRestaurant(Integer restaurantId) {
        return orderRepository.findOrderByReviewerAndRestaurant(actorService.findByPrincipal().getId(), restaurantId);
    }

    public Collection<Order> findOrderByReviewerAndDish(Integer dishId) {
        return orderRepository.findOrderByReviewerAndDish(actorService.findByPrincipal().getId(), dishId);
    }


	public Collection<Order> getOrdersByReviewerAndStatus(Integer reviewerId, String status) {
		return orderRepository.getOrdersByReviewerAndStatus(reviewerId, status);
	}

	public Collection<Order> getOrdersByRestaurantAndStatus(Integer restaurantId, String status) {
		return orderRepository.getOrdersByRestaurantAndStatus(restaurantId, status);
	}

	public Collection<Order> getOrdersByDeliveryManAndStatus(Integer deliveryManId, String status) {
		return orderRepository.getOrdersByDeliveryManAndStatus(deliveryManId, status);
	}

	public Collection<Order> findByEvaluatorWithNoReview(){
        Evaluator evaluator = (Evaluator) actorService.findByPrincipal();
        return orderRepository.findByEvaluatorWithNoReview(evaluator.getId());
    }




    public void correoAceptado(Order o){
        String from = "noreply@foodinhome.es";
        String to = o.getReviewer().getEmail();
        // String subject="Su pedido "+o.getTicker()+" ha sido aceptado";
        Template plantilla = templateService.getTemplateAccepted();
        String subject = plantilla.getTitle();
        String saludo = plantilla.getBody();
        String body=saludo+" "+o.getReviewer().getName()+"!\nSu pedido "+o.getTicker()+" del restaurante "+o.getRestaurant().getName()+" ha sido aceptado y esta siendo cocinado.\nUn saludo.\nFoodInHome ";
        String name = o.getReviewer().getName();
        enviarCorreo(from,name,to,subject, body);
    }



    public void correoDelivering(Order o){
        String from = "noreply@foodinhome.es";
        String to = o.getReviewer().getEmail();
        // String subject="Su pedido "+o.getTicker()+" ha sido aceptado";
        Template plantilla = templateService.getTemplateDelivering();
        String subject = plantilla.getTitle();
        String saludo = plantilla.getBody();
        String body=saludo+" "+o.getReviewer().getName()+"!\nSu pedido "+o.getTicker()+" del restaurante "+o.getRestaurant().getName()+" esta en reparto.\nUn saludo.\nFoodInHome ";
        String name = o.getReviewer().getName();
        enviarCorreo(from,name,to,subject, body);
    }

    public void correoRechazado(Order o){
        String from = "noreply@foodinhome.es";
        String to = o.getReviewer().getEmail();
        // String subject="Su pedido "+o.getTicker()+" ha sido aceptado";
        Template plantilla = templateService.getTemplateRejected();
        String subject = plantilla.getTitle();
        String saludo = plantilla.getBody();
        String body=saludo+" "+o.getReviewer().getName()+"\nSu pedido "+o.getTicker()+" del restaurante "+o.getRestaurant().getName()+" ha sido rechazado por el restaurante.\nUn saludo.\nFoodInHome ";
        String name = o.getReviewer().getName();
        enviarCorreo(from,name,to,subject, body);
    }

    public void correoCompletado(Order o){
        String from = "noreply@foodinhome.es";
        String to = o.getReviewer().getEmail();
        // String subject="Su pedido "+o.getTicker()+" ha sido aceptado";
        Template plantilla = templateService.getTemplateCompleted();
        String subject = plantilla.getTitle();
        String saludo = plantilla.getBody();
        String body=saludo+" "+o.getReviewer().getName()+"\nSu pedido "+o.getTicker()+" del restaurante "+o.getRestaurant().getName()+" ha sido completado. Deje una opinion en nuestra web.\nUn saludo.\nFoodInHome ";
        String name = o.getReviewer().getName();
        enviarCorreo(from,name,to,subject, body);
    }





    public void enviarCorreo(String from, String name, String to, String subject, String body){
        Email email = EmailBuilder.startingBlank()
                .from("Acme FoodInhome",from)
                .to(name, to)
                .withSubject(subject)
                .withPlainText(body)
                .buildEmail();

        MailerBuilder
                .withSMTPServer("smtp.gmail.com", 587, "acme.food.in.home", "acme12345")
                .buildMailer()
                .sendMail(email);
    }


    private String generateTicker(){
        Calendar calendar = Calendar.getInstance();
        String anio = String.valueOf(calendar.get(Calendar.YEAR));
        Random ran = new Random();
        String n1 = String.valueOf(ran.nextInt(10));
        String n2 = String.valueOf(ran.nextInt(10));
        String n3 = String.valueOf(ran.nextInt(10));
        String n4 = String.valueOf(ran.nextInt(10));

        String res = anio+"O"+n1+n2+n3+n4;
        return res;

    }

    

}
