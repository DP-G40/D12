package services;

import domain.PostalAddress;
import domain.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.PostalAddressRepository;

import java.util.Collection;

@Service
@Transactional
public class PostalAddressService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private PostalAddressRepository postaladdressRepository;
// Suporting repository --------------------------------------------------

    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private ActorService actorService;
    
    // Constructors -----------------------------------------------------------
    public PostalAddressService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public PostalAddress create() {
        PostalAddress result;
        result = new PostalAddress();
        result.setRestaurant(restaurantService.findOne(actorService.findByPrincipal().getId()));
        return result;
    }

    public Collection<PostalAddress> findAll() {
        Collection<PostalAddress> result;
        Assert.notNull(postaladdressRepository);
        result = postaladdressRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public PostalAddress findOne(int postaladdressId) {
        PostalAddress result;
        result = postaladdressRepository.findOne(postaladdressId);
        return result;
    }

    public PostalAddress save(PostalAddress postaladdress) {
        Assert.notNull(postaladdress);
        PostalAddress result;
        result = postaladdressRepository.save(postaladdress);
        return result;
    }

    public void delete(PostalAddress postaladdress) {
        Assert.notNull(postaladdress);
        Assert.isTrue(postaladdress.getId() != 0);
        postaladdressRepository.delete(postaladdress);
    }

    public PostalAddress findByRestaurantAndAddress(Restaurant restaurant, String address){
        return postaladdressRepository.findByRestaurantAndAddress(restaurant.getId(),address);
    }

// Other business methods -------------------------------------------------
}
