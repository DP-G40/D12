package services;

import domain.Restaurant;
import forms.EditRestaurantForm;
import forms.RegisterRestaurantForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RestaurantRepository;
import security.Authority;
import security.UserAccount;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.rowset.serial.SerialException;

@Service
@Transactional
public class RestaurantService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ActorService actorService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public RestaurantService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Restaurant create() {
        Restaurant result;
        result = new Restaurant();
        UserAccount userAccount = new UserAccount();
        Authority a = new Authority();
        a.setAuthority(Authority.RESTAURANT);
        userAccount.getAuthorities().add(a);
        result.setUserAccount(userAccount);
        return result;
    }

    public Collection<Restaurant> findAll() {
        Collection<Restaurant> result;
        Assert.notNull(restaurantRepository);
        result = restaurantRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Restaurant findOne(int restaurantId) {
        Restaurant result;
        result = restaurantRepository.findOne(restaurantId);
        return result;
    }

    public Restaurant save(Restaurant restaurant) {
        Assert.notNull(restaurant);
        Restaurant result;
        result = restaurantRepository.save(restaurant);
        return result;
    }

    public void delete(Restaurant restaurant) {
        Assert.notNull(restaurant);
        Assert.isTrue(restaurant.getId() != 0);
        restaurantRepository.delete(restaurant);
    }

    public List<Restaurant> getRestaurantsBanned() {
        return restaurantRepository.getRestaurantsBanned();
    }

    public List<Restaurant> getRestaurantsNoBanned() {
        return restaurantRepository.getRestaurantsNoBanned();
    }

// Other business methods -------------------------------------------------

    public Object[] getRestaurantsAndMedia() {
        Collection<Restaurant> restaurantsNotBanned = this.getRestaurantsNoBanned();
        List<Double> medias = new ArrayList<Double>();
        Object[] res = new Object[2];

        for (Restaurant r : restaurantsNotBanned) {
            medias.add(commentService.getMedia(r.getId()));
        }

        res[0] = restaurantsNotBanned;
        res[1] = medias;

        return res;
    }

    public Restaurant reconstruct(RegisterRestaurantForm registerRestaurantForm) throws IOException, SerialException, SQLException {
        Restaurant res = this.create();
        Assert.isTrue(
                registerRestaurantForm.getPassword().equals(
                        registerRestaurantForm.getRepeatPassword()),
                "register.error.password");
        Assert.isNull(actorService.getByUserName(registerRestaurantForm
                .getUserName()), "register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        res.getUserAccount().setPassword(md5.encodePassword(
                registerRestaurantForm.getPassword(), null));
        res.getUserAccount().setUsername(registerRestaurantForm.getUserName());
        res.setName(registerRestaurantForm.getName());
        res.setSurname(registerRestaurantForm.getSurname());
        res.setPhone(registerRestaurantForm.getPhone());
        res.setEmail(registerRestaurantForm.getEmail());
        res.setAddress(registerRestaurantForm.getAddress());
        res.setCIF(registerRestaurantForm.getCif());
        res.setDescription(registerRestaurantForm.getDescription());
        res.setDeliveryPrice(registerRestaurantForm.getDeliveryPrice());
        
        if(registerRestaurantForm.getFile() != null) {
        	byte[] imgByte = registerRestaurantForm.getFile().getBytes();
        	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        }
        
        return res;

    }


    public List<Restaurant> searchRestaurant(String key) {
        return restaurantRepository.searchRestaurant(key);
    }

	public EditRestaurantForm construct(Restaurant restaurant) {
		EditRestaurantForm editRestaurantForm = new EditRestaurantForm();
		editRestaurantForm.setAddress(restaurant.getAddress());
		editRestaurantForm.setCif(restaurant.getCIF());
		editRestaurantForm.setDeliveryPrice(restaurant.getDeliveryPrice());
		editRestaurantForm.setDescription(restaurant.getDescription());
		editRestaurantForm.setEmail(restaurant.getEmail());
		editRestaurantForm.setName(restaurant.getName());
		editRestaurantForm.setPhone(restaurant.getPhone());
		editRestaurantForm.setSurname(restaurant.getSurname());
		
		return editRestaurantForm;
	}

	public Restaurant reconstructEdit(EditRestaurantForm editRestaurantForm) throws SerialException, SQLException, IOException {
		Restaurant res = this.findOne(actorService.findByPrincipal().getId());
        
        res.setName(editRestaurantForm.getName());
        res.setSurname(editRestaurantForm.getSurname());
        res.setPhone(editRestaurantForm.getPhone());
        res.setEmail(editRestaurantForm.getEmail());
        res.setAddress(editRestaurantForm.getAddress());
        res.setCIF(editRestaurantForm.getCif());
        res.setDescription(editRestaurantForm.getDescription());
        res.setDeliveryPrice(editRestaurantForm.getDeliveryPrice());
        
        if(editRestaurantForm.getFile() != null) {
        	byte[] imgByte = editRestaurantForm.getFile().getBytes();
        	res.setPhoto( new javax.sql.rowset.serial.SerialBlob(imgByte));
        }
        return res;
	}

	public void banRestaurant(Integer restaurantId){
        Restaurant restaurant = findOne(restaurantId);
        restaurant.setBanned(true);
        restaurant.getUserAccount().setEnabled(false);
        save(restaurant);
    }

    public void unbanRestaurant(Integer restaurantId){
        Restaurant restaurant = findOne(restaurantId);
        restaurant.setBanned(false);
        save(restaurant);
    }

    public Collection<Restaurant> findAllActives(){
        return restaurantRepository.getRestaurantsNoBanned();
    }

}
