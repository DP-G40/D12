package services;

import domain.Evaluator;
import domain.Restaurant;
import domain.Review;
import forms.ReviewForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ReviewRepository;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

@Service
@Transactional
public class ReviewService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private OrderService orderService;

// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public ReviewService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Review create() {
        Review result;
        result = new Review();
        result.setEvaluator((Evaluator) actorService.findByPrincipal());
        result.setCode(generateTicker());
        return result;
    }

    public Collection<Review> findAll() {
        Collection<Review> result;
        Assert.notNull(reviewRepository);
        result = reviewRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Review findOne(int reviewId) {
        Review result;
        result = reviewRepository.findOne(reviewId);
        return result;
    }

    public Review save(Review review) {
        Assert.notNull(review);
        Review result;
        result = reviewRepository.save(review);
        return result;
    }

    public void delete(Review review) {
        Assert.notNull(review);
        Assert.isTrue(review.getId() != 0);
        reviewRepository.delete(review);
    }

// Other business methods -------------------------------------------------

    public Review reconstruct(ReviewForm reviewForm){
        Review res = create();
        res.setOrder(reviewForm.getOrder());
        res.setEvaluation(reviewForm.getEvaluation());
        res.setMark(reviewForm.getMark());
        res.setTime(reviewForm.getTime());
        return res;
    }

    private String generateTicker() {
		String res="";
		try {
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			String yearInString = String.valueOf(year);
			res = res + yearInString;
			
			res = res + "-";
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, 2014);
			cal.set(Calendar.DAY_OF_YEAR, 1);    
			Date startYear = cal.getTime();
			res = res + (reviewRepository.getReviewsByYear(startYear).size()+1);
		} catch(Exception e) {
			res="YYYY-NNNN";
		}
		
		return res;
	}

    public Collection<Review> findByEvaluator(){
        Evaluator evaluator = (Evaluator) actorService.findByPrincipal();
        Collection<Review> res = reviewRepository.findByEvaluator(evaluator.getId());
        return res;
    }

    public Collection<Review> findByRestaurant(){
        Restaurant restaurant = (Restaurant) actorService.findByPrincipal();
        Collection<Review> res = reviewRepository.findByRestaurant(restaurant.getId());
        return res;
    }

	public Object findByEvaluatorId(Integer evaluatorId) {
		return reviewRepository.findByEvaluator(evaluatorId);
	}
}
