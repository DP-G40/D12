package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.ScheduleRepository;
import domain.Schedule;

@Service
@Transactional
public class ScheduleService {
// Managed repository -----------------------------------------------------
@Autowired
private ScheduleRepository scheduleRepository;
// Suporting repository --------------------------------------------------

@Autowired
private RestaurantService restaurantService;
@Autowired
private ActorService actorService;

// Constructors -----------------------------------------------------------
public ScheduleService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Schedule create() {
Schedule result;
result = new Schedule();
result.setRestaurant(restaurantService.findOne(actorService.findByPrincipal().getId()));
return result;
}

public Collection<Schedule> findAll() {
Collection<Schedule> result;
Assert.notNull(scheduleRepository);
result = scheduleRepository.findAll();
Assert.notNull(result);
return result;
}

public Schedule findOne(int scheduleId) {
Schedule result;
result = scheduleRepository.findOne(scheduleId);
return result;
}

public Schedule save(Schedule schedule) {
Assert.notNull(schedule);
Schedule result;
result = scheduleRepository.save(schedule);
return result;
}

public void delete(Schedule schedule) {
Assert.notNull(schedule);
Assert.isTrue(schedule.getId() != 0);
scheduleRepository.delete(schedule);
}
public Schedule findByRestaurantAndDay(Integer restaurantId, String day) {
	return scheduleRepository.findByRestaurantAndDay(restaurantId, day);
}
public Collection<Schedule> findByRestaurant(Integer restaurantId) {
	return scheduleRepository.findByRestaurant(restaurantId);
}

// Other business methods -------------------------------------------------


	
}
