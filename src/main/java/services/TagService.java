package services;

import domain.Tag;
import forms.TagRestaurantForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.TagRepository;

import java.util.Collection;

@Service
@Transactional
public class TagService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private TagRepository tagRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public TagService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Tag create() {
        Tag result;
        result = new Tag();
        return result;
    }

    public Collection<Tag> findAll() {
        Collection<Tag> result;
        result = tagRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Tag findOne(int tagId) {
        Tag result;
        Assert.notNull(tagId);
        result = tagRepository.findOne(tagId);
        Assert.notNull(result);
        return result;
    }

    public Tag save(Tag tag) {
        Assert.notNull(tag);
        Tag result;
        Assert.notNull(tag);
        Assert.isNull(this.findByName(tag.getName()),"tag.name.error");
        result = tagRepository.save(tag);
        return result;
    }

    public void delete(Tag tag) {
        Assert.notNull(tag);
        Assert.isTrue(tag.getId() != 0);
        tagRepository.delete(tag);
    }

// Other business methods -------------------------------------------------


    public Tag findByName(String name){
        return this.tagRepository.findByName(name);
    }

    public Tag reconstructRestaurant(TagRestaurantForm tagRestaurantForm){
       return tagRestaurantForm.getTag();

    }
}
