package services;

import domain.Template;
import forms.TemplateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.TemplateRepository;

import java.util.Collection;

@Service
@Transactional
public class TemplateService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private TemplateRepository templateRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public TemplateService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Template create() {
        Template result;
        result = new Template();
        return result;
    }

    public Collection<Template> findAll() {
        Collection<Template> result;
        Assert.notNull(templateRepository);
        result = templateRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Template findOne(int templateId) {
        Template result;
        result = templateRepository.findOne(templateId);
        return result;
    }

    public Template save(Template template) {
        Assert.notNull(template);
        Template result;
        result = templateRepository.save(template);
        return result;
    }

    public void delete(Template template) {
        Assert.notNull(template);
        Assert.isTrue(template.getId() != 0);
        templateRepository.delete(template);
    }

// Other business methods -------------------------------------------------


    public Template getTemplateAccepted(){
        return templateRepository.getTemplateAccepted();
    }

    public Template getTemplateRejected(){
        return templateRepository.getTemplateRejected();
    }

    public Template getTemplateDelivering(){
        return templateRepository.getTemplateDelivering();
    }

    public Template getTemplateCompleted(){
        return templateRepository.getTemplateCompleted();
    }

    public Template getTemplateActiveByType(String type){
        return templateRepository.getTemplateActiveByType(type);
    }

    public Template reconstruct(TemplateForm t){
        Template res;
        res = this.create();
        res.setTitle(t.getTitle());
        res.setBody(t.getBody());
        res.setActive(t.getActive());
        res.setType(t.getType());
        return res;
    }
}
