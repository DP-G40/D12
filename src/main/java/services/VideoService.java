package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import repositories.VideoRepository;
import domain.Video;

@Service
@Transactional
public class VideoService {
// Managed repository -----------------------------------------------------
@Autowired
private VideoRepository videoRepository;
// Suporting repository --------------------------------------------------

@Autowired
private ActorService actorService;

@Autowired
private RestaurantService restaurantService;

// Constructors -----------------------------------------------------------
public VideoService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Video create() {
Video result;
result = new Video();
result.setRestaurant(restaurantService.findOne(actorService.findByPrincipal().getId()));
return result;
}

public Collection<Video> findAll() {
Collection<Video> result;
Assert.notNull(videoRepository);
result = videoRepository.findAll();
Assert.notNull(result);
return result;
}

public Video findOne(int videoId) {
Video result;
result = videoRepository.findOne(videoId);
return result;
}

public Video save(Video video) {
Assert.notNull(video);
Video result;
result = videoRepository.save(video);
return result;
}

public void delete(Video video) {
Assert.notNull(video);
Assert.isTrue(video.getId() != 0);
videoRepository.delete(video);
}

public List<Video> getAnnouncementsByRestaurants(Integer restaurant, Date from, Date until) {
	return videoRepository.getAnnouncementsByRestaurants(restaurant, from, until);
}
public List<Video> getAnnouncementsByRestaurant(Integer restaurant) {
	return videoRepository.getAnnouncementsByRestaurant(restaurant);
}
public Collection<Video> getByDates(Date date) {
	return videoRepository.getAnnouncements(date);
}

// Other business methods -------------------------------------------------

	

}
