<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<br><br>
<h2><spring:message	code="controlPanel.editConfig" /></h2><br>
<form:form action="administrator/saveConfig.do" modelAttribute="configuration" method="post">
	<form:hidden path="id"/>
	<form:hidden path="version"/>

    <div class="row">
        <div class="col-xs-12 form-group">
            <acme:textbox path="systemPrice" code="config.systemPrice" type="number" min="0.1" step="0.1"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="bannerPrice" code="config.bannerPrice" type="number" min="0.1" step="0.1"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="videoPrice" code="config.videoPrice" type="number" min="0.1" step="0.1"/>
        </div>

        <br><br>
        <div class="col-xs-12 form-group">
            <acme:submit name="save" code="button.save"/>
            <acme:cancel code="button.cancel" url=""/>
        </div>
    </div>
</form:form>

<br>
<h2><spring:message	code="controlPanel.activepromotions" /></h2>
<display:table name="activepromotions" id="offers" requestURI="administrator/controlPanel.do">

    <display:column titleKey="offer.title" property="title"/>
    <display:column titleKey="offer.description" property="description"/>
    <display:column titleKey="offer.startDate" property="startDate"/>
    <display:column titleKey="offer.endDate" property="endDate"/>
    <display:column titleKey="offer.discount" property="discount"/>

</display:table>


<br>
<h2><spring:message	code="controlPanel.promotions" /></h2>
<display:table name="offers" id="offers" requestURI="administrator/controlPanel.do">

    <display:column titleKey="offer.title" property="title"/>
    <display:column titleKey="offer.description" property="description"/>
    <display:column titleKey="offer.startDate" property="startDate"/>
    <display:column titleKey="offer.endDate" property="endDate"/>
    <display:column titleKey="offer.discount" property="discount"/>
    
    <display:column titleKey="button.edit">
        <acme:cancel code="button.edit" url="promotion/administrator/edit.do?promotion=${offers.promotion}"></acme:cancel>
    </display:column>

	<display:column titleKey="button.delete">
        <acme:cancel code="button.delete" url="promotion/administrator/delete.do?promotion=${offers.promotion}"></acme:cancel>
    </display:column>

</display:table>
<acme:cancel code="administrator.promotion.create" url="promotion/administrator/create.do"></acme:cancel>

<br>
<h2><spring:message	code="controlPanel.bannedRestaurants" /></h2>
<display:table name="restaurantsBanned" id="restaurantsBanned" requestURI="administrator/controlPanel.do">

    <display:column titleKey="restaurantsBanned.CIF" property="CIF"/>
    
    <display:column>
        <acme:cancel code="restaurant.comments" url="comment/administrator/all.do?restaurantId=${restaurantsBanned.id}"></acme:cancel>
    </display:column>

	<display:column titleKey="button.unban">
        <acme:cancel code="button.unban" url="restaurant/administrator/unban.do?restaurantId=${restaurantsBanned.id}"></acme:cancel>
    </display:column>

</display:table>


<br>
<c:set var="count" value="0" scope="page" />
<h2><spring:message	code="controlPanel.restaurantsAndMedias" /></h2>
<display:table name="restaurantsNotBanned" id="restaurantAndMedia" requestURI="administrator/controlPanel.do">

	<c:if test = "${restaurantsAverages[count] <= 4}">
    	<display:column titleKey="restaurantsBanned.CIF" class="resaltar" property="CIF"/>
    </c:if>
    <c:if test = "${restaurantsAverages[count] >= 4 || restaurantsAverages[count] == 'NaN'}">
    	<display:column titleKey="restaurantsBanned.CIF" property="CIF"/>
    </c:if>
    
    <c:if test = "${restaurantsAverages[count] == 'NaN'}">
         <display:column titleKey="restaurants.media" value=" - " />
    </c:if>
    <c:if test = "${restaurantsAverages[count] != 'NaN'}">
         <display:column titleKey="restaurants.media" value="${restaurantsAverages[count]}" />
    </c:if>
   
   <display:column>
       <acme:cancel code="restaurant.comments" url="comment/administrator/all.do?restaurantId=${restaurantAndMedia.id}"></acme:cancel>
    </display:column>

	<display:column titleKey="button.ban">
        <acme:cancel code="button.ban" url="restaurant/administrator/ban.do?restaurantId=${restaurantAndMedia.id}"></acme:cancel>
    </display:column>

	<c:set var="count" value="${count + 1}" scope="page"/>
</display:table>

<br><br><h2><spring:message	code="controlPanel.generateBillLastMonth" /></h2>
<acme:cancel code="controlPanel.generateBill" url="bill/administrator/generate.do"></acme:cancel>