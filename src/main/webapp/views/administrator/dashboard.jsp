<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2><spring:message	code="dashboard.Q1" />:</h2> <c:out value="${Q1[0][0]}"/> - <c:out value="${Q1[0][1]}"/> - <c:out value="${Q1[0][2]}"/><br>

<h2><spring:message	code="dashboard.Q2" /></h2><br>
<display:table name="Q2" id="Q2" requestURI="administrator/dashboard.do">

    <display:column titleKey="restaurantsBanned.CIF" property="CIF"/>

</display:table>

<h2><spring:message	code="dashboard.Q3" /></h2><br>
<display:table name="Q3" id="Q3" requestURI="administrator/dashboard.do">

    <display:column titleKey="dish.name" property="name"/>

</display:table>

<h2><spring:message	code="dashboard.Q4" /></h2><br>
<display:table name="Q4" id="Q4" requestURI="administrator/dashboard.do">

    <display:column titleKey="restaurantsBanned.CIF" property="CIF"/>

</display:table>

<h2><spring:message	code="dashboard.Q5" />:</h2> <c:out value="${Q5[0][0]}"/> - <c:out value="${Q5[0][1]}"/> - <c:out value="${Q5[0][2]}"/><br>

<h2><spring:message	code="dashboard.Q6" />:</h2> <c:out value="${Q6[0][0]}"/> - <c:out value="${Q6[0][1]}"/> - <c:out value="${Q6[0][2]}"/><br>

<h2><spring:message	code="dashboard.Q7" />:</h2><br>
Max: <c:out value="${Q7[0].name}"/> - Min: <c:out value="${Q7_2[0].name}"/><br>

<h2><spring:message	code="dashboard.Q8" />:</h2><br>
Max: <c:out value="${Q8[0].name}"/> - Min: <c:out value="${Q8_2[0].name}"/><br>

<h2><spring:message	code="dashboard.Q9" />:</h2> <c:out value="${Q9[0].name}"/><br>

<h2><spring:message	code="dashboard.Q10" />:</h2> <c:out value="${Q10[0].name}"/><br>

<h2><spring:message	code="dashboard.Q11" />:</h2> <c:out value="${Q11[0].CIF}"/><br>

<h2><spring:message	code="dashboard.Q12" />:</h2> <c:out value="${Q12[0].name}"/><br>
