<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty errorMessage}">
	<div class="resaltar">
    	${errorMessage}
    </div>
</c:if>

<h2><spring:message	code="allergens.all" /></h2><br>
<display:table name="allergens" id="allergen" requestURI="allergen/administrator/all.do">

    <display:column titleKey="allergen.name" property="name"/>
	
	<!-- Formulario para editar cada plantilla -->
	
	<c:if test="${fn:length(allergen.dishes) == 0}">
	
		<display:column titleKey="button.edit">
		<c:set var="allergenEdit" value="${allergen}" scope="request" />
		
			<form:form action="allergen/administrator/save.do" modelAttribute="allergenEdit" method="post">
				<form:hidden path="id"/>
				<form:hidden path="version"/>
			
			    <div class="row">
			        <div class="col-xs-12 form-group">
			            <acme:textbox path="name" code="allergen.name"/>
			        </div>
			        
			        <br>
			        <div class="col-xs-12 form-group">
			            <acme:submit name="save" code="button.save"/>
			        </div>
			    </div>
			</form:form>
			
		</display:column>
		
		<display:column titleKey="button.delete">
			<acme:cancel code="button.delete" url="allergen/administrator/delete.do?allegenId=${allergen.id}"></acme:cancel>
	    </display:column>
	    
	</c:if>
	
	<c:if test="${fn:length(allergen.dishes) != 0}">
		<display:column></display:column>
		<display:column></display:column>
	</c:if>
	
</display:table>

<h2><spring:message	code="allergens.create" /></h2><br>
<form:form action="allergen/administrator/saveNew.do" modelAttribute="allergenNew" method="post">
			<form:hidden path="id"/>
			<form:hidden path="version"/>
			<form:hidden path="dishes"/>
		
		    <div class="row">
		        <div class="col-xs-12 form-group">
		            <acme:textbox path="name" code="allergen.name"/>
		        </div>
		        
		        <br>
		        <div class="col-xs-12 form-group">
		            <acme:submit name="save" code="button.save"/>
		        </div>
		    </div>
</form:form>