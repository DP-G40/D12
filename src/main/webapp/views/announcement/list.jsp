<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="domain.Banner" %>

<h2><spring:message	code="videos.all" /></h2><br>
<display:table name="videos" id="video" requestURI="${requestURI}">

    <display:column titleKey="annoucement.url" property="url"/>
    <display:column titleKey="annoucement.startDate" property="startDate" format="{0,date,dd/MM/yyyy}"/>
    <display:column titleKey="annoucement.endDate" property="endDate" format="{0,date,dd/MM/yyyy}"/>
	
</display:table>

<acme:cancel url="announcement/restaurant/createVideo.do" code="button.create"></acme:cancel>

<h2><spring:message	code="banners.all" /></h2><br>
<display:table name="banners" id="banner" requestURI="${requestURI}">

	<!-- Atacamos a una propiedad derivada para que pueda sacar el code64 y poder mostrar la img-->
    <% Banner b1 = (Banner) pageContext.getAttribute("banner", PageContext.PAGE_SCOPE);
       String imgString = "";
       try{
       		imgString = b1.getImageCode64();
       }catch(Exception e){
    	   
       }
    %>
    
	<display:column>
        <img src="data:image/jpeg;base64,<%=imgString%>" title="banner.jpeg">
	</display:column>
    <display:column titleKey="annoucement.startDate" property="startDate" format="{0,date,dd/MM/yyyy}"/>
    <display:column titleKey="annoucement.endDate" property="endDate" format="{0,date,dd/MM/yyyy}"/>
	
</display:table>

<acme:cancel url="announcement/restaurant/createBanner.do" code="button.create"></acme:cancel>
