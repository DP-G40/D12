<%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="bill/restaurant/accept.do" modelAttribute="acceptBillForm" method="post">

	<form:hidden path="billId"/>

	<acme:textbox code="row.holderName" path="creditCard.holderName"/>
	<acme:textbox code="row.brand" path="creditCard.brand"/>
	<acme:textbox code="row.number" path="creditCard.number"/>
	<acme:textbox code="row.month" path="creditCard.expiryMonth" type="number" min="1" max="12" step="1"/>
	<acme:textbox code="row.year" path="creditCard.expiryYear" type="number" min="2018" max="9999" step="1"/>
	<acme:textbox code="row.cvv" path="creditCard.cvv" type="number" min="100" max="999" step="1"/>
	<br>

	<tr>
		<td colspan="3">
			<acme:submit name="save" code="row.save"/>
			<acme:cancel code="row.cancel" url="bill/restaurant/list.do" />

		</td>
	</tr>
	
</form:form>
