<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<display:table name="bills" id="bill" requestURI="/bill/restaurant/list.do"
	pagesize="20" class="displaytag">
	
	<display:column property="status" titleKey="bill.status"/>

	<display:column property="monthlyFee" titleKey="bill.monthlyFee"/>

	<display:column property="announcementFee" titleKey="bill.announcementFee"/>

	<display:column property="creationDate" titleKey="bill.creationDate" format="{0,date,dd/MM/yyyy HH:mm}"/>

	<display:column property="reason" titleKey="bill.reason"/>

	<display:column>
		<c:if test="${bill.status.equals('PENDING')}">
			<acme:cancel code="bill.accept" url="/bill/restaurant/accept.do?billId=${bill.id}"/>
			<acme:cancel code="bill.cancel" url="/bill/restaurant/cancel.do?billId=${bill.id}"/>
		</c:if>
	</display:column>




	
</display:table>


