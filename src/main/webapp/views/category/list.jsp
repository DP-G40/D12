<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%
	Date date = new Date();
	pageContext.setAttribute("now", date);
	Integer mes = 60 * 60 * 24 * 30 * 1000;
%>




<display:table name="categories" id="row" requestURI="${requestURI}"
	pagesize="10" class="displaytag">
	
	

	<display:column titleKey="category.name" >
	${row.name}
	</display:column>

	<display:column  titleKey="category.father" >
				${row.father.name}
	</display:column>

	<security:authorize access="hasRole('ADMIN')">
	<display:column>
		<c:if test = "${not empty row.father}">
			<acme:cancel code="category.edit" url="/category/administrator/edit.do?categoryId=${row.id}"/>
		</c:if>
	</display:column>
	</security:authorize>


	
</display:table>

<br>



<security:authorize access="hasRole('ADMIN')">
<acme:cancel code="category.create" url="/category/administrator/create.do"/>
</security:authorize>


