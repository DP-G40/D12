<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty errorMessage}">
	<div class="resaltar">
    	${errorMessage}
    </div>
</c:if>

<h2><spring:message	code="comments.all" /></h2><br>
<display:table name="comments" id="comment" requestURI="comment/administrator/all.do">

    <display:column titleKey="comments.title" property="title"/>
	<display:column titleKey="comments.description" property="description"/>
	<display:column titleKey="comments.mark" property="mark"/>
	
	<security:authorize access="hasAnyRole('ADMIN')">	
	<display:column titleKey="button.delete">
	     <a href="comment/administrator/delete.do?commentId=${comment.id}"><spring:message code="button.delete"/></a>
	 </display:column>
	 </security:authorize>   
	
</display:table>
