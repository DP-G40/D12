<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<form:form action="${action}" modelAttribute="commentForm">

    <form:hidden path="commentableId"/>

    <acme:textbox code="comment.title" path="title" readonly="${read}"/>
    <acme:textbox code="comment.mark" path="mark" type="number" min="0" max="5" step="1" readonly="${read}"/>
    <br/>
    <acme:textarea code="comment.description" path="description" readonly="${read}"/><br/>







    <tr>
        <td colspan="3">
            <c:if test="${!read}">
                <acme:submit name="save" code="row.save"/>
            </c:if>
            <acme:cancel code="row.cancel" url="/#" />

        </td>
    </tr>

</form:form>
