<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<display:table name="actors" id="actor" requestURI="${requestUri}">

    <display:column titleKey="actor.name" property="name"/>
    <display:column titleKey="actor.surname" property="surname"/>
    <display:column titleKey="actor.email" property="email"/>
	<display:column titleKey="actor.phone" property="phone"/>
	
	<display:column titleKey="button.see">
		<acme:cancel code="button.see" url="evaluator/administrator/view.do?evaluatorId=${actor.id}"></acme:cancel>
	</display:column>
	
</display:table>
