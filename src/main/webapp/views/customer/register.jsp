<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%><%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<br>
<br>
<form:form action="${action}" modelAttribute="registerCustomerForm" method="post" enctype="multipart/form-data">



			<acme:textbox code="customer.name" path="name" />


			<acme:textbox code="customer.surname" path="surname" />


			<acme:textbox code="customer.email" path="email"/>


			<acme:textbox code="customer.phone" path="phone"/>


			<acme:textbox code="customer.address" path="address" />


			<acme:textbox code="customer.userName" path="userName" />

		

			<acme:password code="customer.password" path="password" />

		

			<acme:password code="customer.repeatPassword" path="repeatPassword" />

		<br>

			<acme:textbox code="customer.photo" path="file" type="file" accept="image/*"/>


		<br>
		<br>

			<acme:checkbox code="customer.termsAndConditions" path="check" />
	<br><br>
			<acme:submit name="save" code="row.save" />
			<acme:cancel code="row.cancel" url="" />


</form:form>
