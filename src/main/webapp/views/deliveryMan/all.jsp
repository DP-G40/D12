<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var = "currentOrderLock" scope = "session" value = "false"/>
<display:table name="deliverysmans" id="deliverysman" requestURI="${requestUri}">

    <display:column titleKey="deliverysman.name" property="name"/>
	<display:column titleKey="deliverysman.phone" property="phone"/>
	
	<c:forEach items="${deliverysman.orders}" var="order">
        <c:if test = "${order.status == 'DELIVERING'}">
        	<c:set var = "currentOrderLock" scope = "session" value = "true"/>
        	<display:column titleKey="current.order" value="${order.ticker}"></display:column>
        </c:if>
    </c:forEach>
    
     <c:if test = "${!currentOrderLock}">
     	 <display:column titleKey="current.order" value=" - "></display:column>
     </c:if>
		
	<display:column titleKey="button.see">
		<acme:cancel code="button.see" url="deliveryman/restaurant/seeMyDeliveryMan.do?deliverymanId=${deliverysman.id}"></acme:cancel>
	</display:column>

	<display:column titleKey="button.unsubscribe">
		<acme:cancel code="button.unsubscribe" url="deliveryman/restaurant/fired.do?deliveryId=${deliverysman.id}"></acme:cancel>
	</display:column>



	<c:set var = "currentOrderLock" scope = "session" value = "false"/>
	
</display:table>

<security:authorize access="hasRole('RESTAURANT')">
	<acme:cancel code="button.create" url="deliveryman/restaurant/register.do"></acme:cancel>
</security:authorize>