<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty errorMessage}">
	<div class="resaltar">
    	${errorMessage}
    </div>
</c:if>

<h2><spring:message	code="order.accepted" /></h2><br>
<display:table name="allAcceptedOrders" id="allAcceptedOrder" requestURI="order/deliveryMan/allOrders.do">

    <display:column titleKey="order.ticker" property="ticker"/>
	<display:column titleKey="order.status" property="status"/>
	<display:column titleKey="order.address" property="address"/>
	<display:column titleKey="order.deliveryHour" property="deliveryHour"/>
		
	<display:column titleKey="button.assignMe">
		<acme:cancel code="button.assignMe" url="order/deliveryMan/assign.do?orderId=${allAcceptedOrder.id}"/>
	</display:column>

	<display:column>
		<acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${allAcceptedOrder.id}"/>
	</display:column>
	    
	
</display:table>

<h2><spring:message	code="order.delivering" /></h2><br>
<display:table name="myDealsOrder" id="myDealOrder" requestURI="order/deliveryMan/allOrders.do">

    <display:column titleKey="order.ticker" property="ticker"/>
	<display:column titleKey="order.status" property="status"/>
	<display:column titleKey="order.address" property="address"/>
	<display:column titleKey="order.deliveryHour" property="deliveryHour"/>
		
	<display:column titleKey="button.complete">
		<acme:cancel url="order/deliveryMan/complete.do?orderId=${myDealOrder.id}" code="button.complete"></acme:cancel>
	</display:column>

	<display:column>
		<acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${myDealOrder.id}"/>
	</display:column>
	    
	
</display:table>

<h2><spring:message	code="order.allDeals" /></h2><br>
<display:table name="myClompletedDealsOrder" id="myClompletedDealOrder" requestURI="order/deliveryMan/allOrders.do">

    <display:column titleKey="order.ticker" property="ticker"/>
	<display:column titleKey="order.status" property="status"/>
	<display:column titleKey="order.address" property="address"/>
	<display:column titleKey="order.deliveryHour" property="deliveryHour"/>

	<display:column>
		<acme:cancel code="standard.view" url="order/deliveryMan/view.do?orderId=${myClompletedDealOrder.id}"/>
	</display:column>

</display:table>
