<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<display:table name="deliveryman" id="deliveryman">

    <display:column titleKey="deliverysman.name" property="name"/>
	<display:column titleKey="deliverysman.surname" property="surname"/>
	<display:column titleKey="deliverysman.email" property="email"/>
	<display:column titleKey="deliverysman.address" property="address"/>
	<display:column titleKey="deliverysman.phone" property="phone"/>
	
</display:table>

<br>
<br>
<h1><spring:message code="deliveryman.orders"></spring:message></h1>

<display:table name="orders" id="orders" requestURI="${requestUri}">

    <display:column titleKey="order.ticker" property="ticker"/>
	<display:column titleKey="order.status" property="status"/>
	<display:column titleKey="order.address" property="address"/>
	<display:column titleKey="order.deliveryHour" property="deliveryHour"/>
	
</display:table>