<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
        uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${action}" modelAttribute="addAllergenForm">


<acme:select path="allergen" code="dish.selectalergen" items="${allergens}" itemLabel="name"/>
<form:errors cssClass="error" path="allergen"/>
<br>


<tr>
    <td colspan="3">
        <acme:submit name="save" code="row.save"/>
        <acme:cancel code="row.cancel" url="dish/restaurant/view.do?dishId=${dishId}" />
    </td>
</tr>

</form:form>