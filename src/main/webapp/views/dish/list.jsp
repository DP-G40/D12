<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="dishes" id="row"   pagesize="15"  requestURI="${requestURI}" class="displaytag">

    <display:column titleKey="standard.name" property="name"/>
    <display:column titleKey="standard.description" property="description"/>
    <display:column titleKey="dish.price" property="price"/>
    <display:column titleKey="standard.view"><acme:cancel code="standard.view" url="dish/restaurant/view.do?dishId=${row.id}" ></acme:cancel></display:column>

</display:table>

<acme:cancel code="dish.create" url="dish/restaurant/create.do"></acme:cancel>

<acme:cancel code="dish.deleted" url="dish/restaurant/deleted.do"></acme:cancel>
