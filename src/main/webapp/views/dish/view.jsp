<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<security:authentication property="principal" var="principal"/>

<div>
    <table>
        <tr>
            <th><spring:message code="standard.name"></spring:message></th>
            <th>${dish.name}</th>
        </tr>
        <tr>
            <td><spring:message code="standard.description"></spring:message></td>
            <td>${dish.description}</td>
        </tr>
        <tr>
            <td><spring:message code="dish.price"></spring:message></td>
            <td>${dish.price}  &euro;</td>
        </tr>
        <tr>
            <td><spring:message code="category.category"></spring:message></td>
            <td>${dish.category.name}</td>
        </tr>
        <tr>
            <td><spring:message code="dish.allergens"></spring:message></td>
            <td>
                <ul>
                    <jstl:forEach items="${dish.allergens}" var="x">
                        <li>${x.name}</li>
                    </jstl:forEach>
                </ul>


            </td>
        </tr>

    </table>




</div>

<security:authorize access="hasRole('RESTAURANT')">
    <jstl:if test="${principal.id==dish.restaurant.userAccount.id}">
<div>
        <acme:cancel code="dish.addAlergen" url="dish/restaurant/addAllergens.do?dishId=${dish.id}"></acme:cancel>
        <acme:cancel code="standard.edit" url="dish/restaurant/edit.do?dishId=${dish.id}"></acme:cancel>
        <acme:cancel code="standard.back" url="dish/restaurant/list.do"></acme:cancel>
</div>
    </jstl:if>


<jstl:if test="${principal.id!=dish.restaurant.userAccount.id}">
    <div>
    <acme:cancel code="standard.back" url="restaurant/view.do?restaurantId=${dish.restaurant.id}"></acme:cancel>
    </div>
</jstl:if>

</security:authorize>

<security:authorize access="hasAnyRole('CUSTOMER','EVALUATOR')">
<jstl:if test="${principal != 'anonymousUser'}">
    <div>
        <acme:cancel code="comment.create" url="comment/customer/comment.do?commentableId=${dish.id}" />
    </div>

</jstl:if>
</security:authorize>