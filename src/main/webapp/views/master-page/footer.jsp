<%--
 * footer.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="date" class="java.util.Date" />

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=209641289650911&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-comments" data-href="https://www.acme-foodinhome.com" data-width="100%" data-numposts="5"></div>

<hr />

<a href="about/terms.do"><spring:message code="masterpage.terms" /></a>
<a href="about/cookies.do"><spring:message code="masterpage.cookies" /></a>
<a href="about/contact.do"><spring:message code="masterpage.contact" /></a>
<a href="about/privacy.do"><spring:message code="masterpage.privacy" /></a>

<b>Copyright &copy; <fmt:formatDate value="${date}" pattern="yyyy" /> Acme-FoodInHome</b>