<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<meta charset="UTF-8">

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%--Aqui va el banner--%>

<div>
    <img style="max-width:35%;display:block; margin:auto; " src="images/acme-foodinhome.png" alt="Sample Co., Inc."/>
</div>

<%--<div>--%>
<%--<a href=""><img class="img-responsive center-block" src=${banner }--%>
<%--style="margin-bottom: -10px; margin-top: -25px;" /></a>--%>
<%--</div>--%>
<%----%>
<%----%>
<%----%>
<%----%>


<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<%----%>
<%----%>
<%----%>
<%----%>


<security:authentication property="principal" var="principal"/>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="">Acme Food-In-Home</a>
        </div>

        <ul class="nav navbar-nav navbar-left">


            <security:authorize access="!isAuthenticated()">
                <li><a href="restaurant/list.do"><spring:message code="master.page.restaurant.list"/></a></li>
            </security:authorize>

            <security:authorize access="hasRole('CUSTOMER')">
                <li><a href="restaurant/list.do"><spring:message code="master.page.restaurant.list"/></a></li>
                <li><a href="order/customer/list.do"><spring:message code="master.page.restaurant.order.list"/></a>
                <li><a href="offer/customer/listPromotion.do"><spring:message code="master.page.customer.offer.list"/></a>

            </security:authorize>

            <security:authorize access="hasRole('RESTAURANT')">
                <li><a href="order/restaurant/list.do"><spring:message code="master.page.restaurant.order.list"/></a>
                </li>
                <li><a href="deliveryman/restaurant/myDeliveryMan.do"><spring:message
                        code="master.page.restaurant.myDeliveryMan"/></a></li>

                <li><a href="dish/restaurant/list.do"><spring:message code="master.page.restaurant.dish.list"/></a></li>
                <li><a href="offer/restaurant/myOffers.do"><spring:message code="master.page.restaurant.myOffers"/></a>
                <li><a href="announcement/restaurant/list.do"><spring:message
                        code="master.page.restaurant.announcements"/></a></li>

                <li><a href="bill/restaurant/list.do"><spring:message   code="master.page.restaurant.bill"/></a></li>
                <li><a href="review/restaurant/list.do"><spring:message   code="master.page.restaurant.review"/></a></li>

                <li><a href="restaurant/view.do?restaurantId=${principal.id}"><spring:message code="master.page.restaurant.mypage" /></a></li>

                <li><a href="actor/restaurant/edit.do"><spring:message code="master.page.restaurant.profile.edit" /></a></li>

        </security:authorize>


            <security:authorize access="hasRole('EVALUATOR')">
            <li><a href="restaurant/list.do"><spring:message code="master.page.restaurant.list"/></a></li>
            <li><a href="order/customer/list.do"><spring:message code="master.page.restaurant.order.list"/></a>
            <li><a href="offer/customer/listPromotion.do"><spring:message code="master.page.customer.offer.list"/></a>
            <li><a href="review/evaluator/list.do"><spring:message code="master.page.reviewer.review.list"/></a></li>

            </security:authorize>

            <security:authorize access="hasRole('DELIVERYMAN')">

                <li><a href="order/deliveryMan/allOrders.do"><spring:message
                        code="master.page.deliveryman.allorders"/></a></li>

            </security:authorize>


            <!-- Do not forget the "fNiv" class for the first level links !! -->
            <security:authorize access="hasRole('ADMIN')">

                <li><a href="restaurant/list.do"><spring:message code="master.page.restaurant.list"/></a></li>


                        <li><a href="administrator/dashboard.do"><spring:message
                                code="master.page.administrator.dashboard"/></a></li>
                        <li><a href="administrator/controlPanel.do"><spring:message
                                code="master.page.administrator.controlPanel"/></a></li>
                        <li><a href="evaluator/administrator/list.do"><spring:message
                                code="master.page.administrator.evaluators"/></a></li>


                <li><a href="evaluator/administrator/register.do"><spring:message
                        code="master.page.administrator.evaluator.register"/></a></li>










                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">
                    <spring:message code="master.page.administrator.entities"/> <span class="caret"></span></a>

                    <ul class="dropdown-menu">
                        <li><a href="category/administrator/list.do"><spring:message
                                code="master.page.category.list"/></a></li>
                        <li><a href="allergen/administrator/all.do"><spring:message
                                code="master.page.administrator.allergens"/></a></li>
                        <li><a href="tag/administrator/list.do"><spring:message code="master.page.administrator.tag"/></a></li>

                        <li><a href="template/administrator/all.do"><spring:message
                                code="master.page.administrator.templates"/></a></li>
                    </ul>

                </li>
            </security:authorize>








        </ul>




        <ul class="nav navbar-nav navbar-right">
            <security:authorize access="isAuthenticated()">
                <li class="dropdown"><a href="" class="dropdown-toggle"
                                        data-toggle="dropdown"> <spring:message
                        code="master.page.profile"/> (<security:authentication
                        property="principal.username"/>) <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <security:authorize access="hasRole('CUSTOMER')" >
                            <li><a href="actor/customer/edit.do"><spring:message code="master.page.restaurant.profile.edit" /></a></li>
                        </security:authorize>

                        <security:authorize access="hasRole('EVALUATOR')">
                            <li><a href="actor/evaluator/edit.do"><spring:message code="master.page.restaurant.profile.edit" /></a></li>

                        </security:authorize>

                        <security:authorize access="hasRole('DELIVERYMAN')">
                            <li><a href="actor/deliveryman/edit.do"><spring:message code="master.page.restaurant.profile.edit" /></a></li>
                        </security:authorize>


                        <li><a href="j_spring_security_logout"><spring:message
                                code="master.page.logout"/> </a></li>
                    </ul>
                </li>
            </security:authorize>

            <security:authorize access="isAnonymous()">
                <li><a href="restaurant/register.do"><spring:message code="master.page.register.restaurant"></spring:message> </a></li>
                <li><a href="customer/register.do"><spring:message code="master.page.register.customer"></spring:message> </a></li>

                <li><a href="security/login.do"><spring:message
                        code="master.page.login"/></a></li>

            </security:authorize>



            <li><a href="?language=en"><img class="flag"
                                            src="https://lipis.github.io/flag-icon-css/flags/4x3/us.svg"
                                            style="width: 24px;" alt="United States of America Flag"></a></li>
            <li><a href="?language=es"><img class="flag"
                                            src="https://lipis.github.io/flag-icon-css/flags/4x3/es.svg"
                                            style="width: 24px;" alt="Spain Flag"></a></li>
        </ul>
    </div>
</nav>
<br>

<c:if test="${not empty youtubeURL}">
	<iframe width="420" height="315" src="${youtubeURL}" frameborder="0" allowfullscreen></iframe>
</c:if>

<c:if test="${not empty offersActives}">
<br>
<h2><spring:message	code="controlPanel.activepromotions" /></h2>
<display:table name="offersActives" id="offersActive" requestURI="index.do">

    <display:column titleKey="offer.title" property="title"/>
    <display:column titleKey="offer.description" property="description"/>
    <display:column titleKey="offer.startDate" property="startDate"/>
    <display:column titleKey="offer.endDate" property="endDate"/>
    <display:column titleKey="offer.discount" property="discount"/>

</display:table>
</c:if>

<script type="text/javascript">
    $(document).ready(function () {
        $("#jMenu").jMenu();
    });

    function askSubmission(msg, form) {
        if (confirm(msg)) {
            form.submit();
        } else {
            return false
        }
    }


    function relativeRedir(loc) {
        var b = document.getElementsByTagName('base');
        if (b && b[0] && b[0].href) {
            if (b[0].href.substr(b[0].href.length - 1) == '/' && loc.charAt(0) == '/')
                loc = loc.substr(1);
            loc = b[0].href + loc;
        }
        window.location.replace(loc);
    }
</script>

