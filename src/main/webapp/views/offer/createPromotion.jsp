<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%
    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String now = sdf.format(date);

    pageContext.setAttribute("now",now);
%>
<h2><spring:message	code="controlPanel.newPromotion" /></h2><br>
<br><br>
<form:form action="${actionURL}" modelAttribute="offerForm" method="post">
	<form:hidden path="promotion"/>
	
    <div class="row">
        <div class="col-xs-12 form-group">
            <acme:textbox path="title" code="offer.title"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="description" code="offer.description"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="startDate" code="offer.startDate" type="date" min="${now}"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="endDate" code="offer.endDate" type="date" min="${now}"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="discount" code="offer.discount" type="number" min="1" max="100" step="1"/>
        </div>

        <br><br>
        <div class="col-xs-12 form-group">
            <acme:submit name="save" code="button.save"/>
            <acme:cancel code="button.cancel" url="/administrator/controlPanel.do"/>
        </div>
    </div>
</form:form>


