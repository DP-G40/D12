<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%pageContext.setAttribute("now", new Date());%>

<display:table name="offers" id="offer" requestURI="${requestURI}">

    <display:column property="title" titleKey="offer.title"/>

    <display:column property="description" titleKey="offer.description"/>

    <display:column property="startDate" titleKey="offer.startDate" format="{0,date,dd/MM/yyyy}"/>

    <display:column property="endDate" titleKey="offer.endDate" format="{0,date,dd/MM/yyyy}"/>

    <display:column property="discount" titleKey="offer.discount"/>

    <display:column property="code" titleKey="offer.code"/>
    
    <security:authorize access="hasRole('RESTAURANT')">
    		<c:if test="${offer.startDate.after(now) && actorID == offer.restaurant.id}">
	    		<display:column><a href="offer/restaurant/edit.do?offerId=${offer.id}"><spring:message code="button.edit"/></a></display:column>
	    		<display:column><a href="offer/restaurant/delete.do?offerId=${offer.id}"><spring:message code="button.delete"/></a></display:column>
			</c:if>
			<c:if test="${offer.startDate.before(now) || actorID != offer.restaurant.id}">
				<display:column></display:column>
	    		<display:column></display:column>
			</c:if>
	</security:authorize>

</display:table>


<security:authorize access="hasRole('RESTAURANT')">
    <acme:cancel code="button.create" url="offer/restaurant/create.do"></acme:cancel>
</security:authorize>




