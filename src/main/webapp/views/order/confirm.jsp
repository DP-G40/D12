<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${action}" modelAttribute="confirmOrderForm">

    <form:hidden path="orderId"/>

    <acme:textbox code="order.address" path="address"/>
    <acme:textbox code="order.postalAddress" path="postalAddress" />
    <br/>

    <acme:textbox code="row.holderName" path="creditCard.holderName"/>
    <acme:textbox code="row.brand" path="creditCard.brand"/>
    <acme:textbox code="row.number" path="creditCard.number"/>
    <acme:textbox code="row.month" path="creditCard.expiryMonth" type="number" min="1" max="12" step="1"/>
    <acme:textbox code="row.year" path="creditCard.expiryYear" type="number" min="2018" max="9999" step="1"/>
    <acme:textbox code="row.cvv" path="creditCard.cvv" type="number" min="100" max="999" step="1"/>
    <br/>
    <acme:textbox code="order.offer" path="offer"/>
    <br/>






    <tr>
        <td colspan="3">
            <acme:submit name="save" code="row.save"/>
            <acme:cancel code="row.cancel" url="order/customer/list.do" />

        </td>
    </tr>

</form:form>
