<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>






<h2><spring:message code="order.pending"/></h2>
<display:table name="pendings" id="pending" requestURI="${requestURI}">

    <display:column property="ticker" titleKey="order.ticker"/>

    <display:column property="address" titleKey="order.address"/>

    <display:column property="status" titleKey="order.status"/>

    <display:column property="deliveryHour" titleKey="order.deliveryHour"/>

    <display:column titleKey="user.name">${pending.reviewer.name} ${pending.reviewer.surname}</display:column>

    <display:column property="orderBill.price" titleKey="order.total"/>

    <display:column>
        <acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${pending.id}"/>
    </display:column>

    <display:column>
        <acme:cancel code="order.accept" url="order/restaurant/accept.do?orderId=${pending.id}"/>
    </display:column>

    <display:column>
        <acme:cancel code="order.reject" url="order/restaurant/decline.do?orderId=${pending.id}"/>
    </display:column>

</display:table>


<h2><spring:message code="order.accepted"/></h2>
<display:table name="accepteds" id="accepted" requestURI="${requestURI}">

    <display:column property="ticker" titleKey="order.ticker"/>

    <display:column property="address" titleKey="order.address"/>

    <display:column property="status" titleKey="order.status"/>

    <display:column property="deliveryHour" titleKey="order.deliveryHour" format="{0,date,HH:mm}" />

    <display:column titleKey="user.name">${pending.reviewer.name} ${pending.reviewer.surname}</display:column>

    <display:column property="orderBill.price" titleKey="order.total"/>
    <display:column>
        <acme:cancel code="order.readyto" url="order/restaurant/ready.do?orderId=${accepted.id}"/>
    </display:column>

    <display:column>
        <acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${accepted.id}"/>
    </display:column>

</display:table>

<h2><spring:message code="order.ready"/></h2>
<display:table name="readys" id="ready" requestURI="${requestURI}">

    <display:column property="ticker" titleKey="order.ticker"/>

    <display:column property="address" titleKey="order.address"/>

    <display:column property="status" titleKey="order.status"/>

    <display:column property="deliveryHour" titleKey="order.deliveryHour"/>

    <display:column titleKey="user.name">${ready.reviewer.name} ${ready.reviewer.surname}</display:column>

    <display:column property="orderBill.price" titleKey="order.total"/>

    <display:column>
        <acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${ready.id}"/>
    </display:column>

</display:table>

<h2><spring:message code="order.delivering"/></h2>
<display:table name="deliverings" id="delivering" requestURI="${requestURI}">

    <display:column property="ticker" titleKey="order.ticker"/>

    <display:column property="address" titleKey="order.address"/>

    <display:column property="status" titleKey="order.status"/>

    <display:column property="deliveryHour" titleKey="order.deliveryHour"/>

    <display:column titleKey="user.name">${delivering.reviewer.name} ${delivering.reviewer.surname}</display:column>

    <display:column property="orderBill.price" titleKey="order.total"/>

    <display:column>
        <acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${delivering.id}"/>
    </display:column>

</display:table>

<h2><spring:message code="order.completed"/></h2>
<display:table name="completeds" id="completed" requestURI="${requestURI}">

    <display:column property="ticker" titleKey="order.ticker"/>

    <display:column property="address" titleKey="order.address"/>

    <display:column property="status" titleKey="order.status"/>

    <display:column titleKey="user.name">${completed.reviewer.name} ${completed.reviewer.surname}</display:column>

    <display:column property="orderBill.price" titleKey="order.total"/>

    <display:column>
        <acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${completed.id}"/>
    </display:column>

</display:table>

<h2><spring:message code="order.rejected"/></h2>
<display:table name="rejecteds" id="rejected" requestURI="${requestURI}">

    <display:column property="ticker" titleKey="order.ticker"/>

    <display:column property="address" titleKey="order.address"/>

    <display:column property="status" titleKey="order.status"/>

    <display:column titleKey="user.name">${rejected.reviewer.name} ${rejected.reviewer.surname}</display:column>

    <display:column property="orderBill.price" titleKey="order.total"/>

    <display:column>
        <acme:cancel code="standard.view" url="order/restaurant/view.do?orderId=${rejected.id}"/>
    </display:column>

</display:table>





