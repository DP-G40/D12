<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="table">
    <table class="order_ver">
        <tr>
            <td><b><spring:message code="order.ticker"></spring:message></b>: ${order.ticker}</td>
        </tr>
        <tr>
            <td><b><spring:message code="order.status"></spring:message></b>: ${order.status}</td>
        </tr>
        <tr>
            <td><b><spring:message code="order.datecreated"></spring:message></b>: ${order.creationDate}</td>
        </tr>
        <tr>
            <td><b><spring:message code="order.deliveryHour"></spring:message></b>: ${order.deliveryHour} </td>
        </tr>
        <tr>
            <td><b><spring:message code="order.address"></spring:message></b>: ${order.address}</td>
        </tr>
        <tr>
            <td><b><spring:message code="order.restaurant"></spring:message></b>: ${order.restaurant.name}</td>
        </tr>
        <tr>
            <td><b><spring:message code="order.orderbill.total"></spring:message></b>: ${order.orderBill.price}</td>
        </tr>
        <jstl:if test="${order.offer != null}">
            <tr>
                <td><b><spring:message code="order.offer"></spring:message></b>: ${order.offer.title}</td>
            </tr>
        </jstl:if>



        <tr>
            <td> <b><spring:message code="order.dishes"></spring:message></b></b>
                <ul>
                    <jstl:forEach items="${order.dishes}" var="x">
                        <li>${x.name} - ${x.price}<security:authorize access="hasRole('CUSTOMER')"><jstl:if test="${order.status.equals('DRAFT')}"> - <acme:cancel code="row.cancel" url="/order/customer/remove.do?dishId=${x.id}&orderId=${order.id}"/></jstl:if></security:authorize></li>
                    </jstl:forEach>
                </ul>
            </td>

        </tr>

    </table>
</div>
<acme:cancel code="standard.back" url="${back}"></acme:cancel>