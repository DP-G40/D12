<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<br><br>
<form:form action="${actionURL}" modelAttribute="postal" method="post">
	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="restaurant"/>
	
    <div class="row">
        <div class="col-xs-12 form-group">
            <acme:textbox path="postal" code="postal.postal" type="number"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="price" code="postal.price"/>
        </div>

        <br><br>
        <div class="col-xs-12 form-group">
            <acme:submit name="save" code="button.save"/>
            <acme:cancel code="button.cancel" url=""/>
        </div>
    </div>
</form:form>


