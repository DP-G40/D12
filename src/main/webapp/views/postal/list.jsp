<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>



<display:table name="postals" id="postal" requestURI="${requestURI}">

    <display:column property="price" titleKey="postal.price"/>

    <display:column property="postal" titleKey="postal.postal"/>

	<display:column><a href="postal/restaurant/edit.do?postalId=${postal.id}"><spring:message code="button.edit"/></a></display:column>
	<display:column><a href="postal/restaurant/delete.do?postalId=${postal.id}"><spring:message code="button.delete"/></a></display:column>

</display:table>


<security:authorize access="hasRole('RESTAURANT')">
    <a href="postal/restaurant/create.do"><spring:message code="button.create"/></a>
</security:authorize>




