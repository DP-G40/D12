<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%><%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="actor/restaurant/save.do" modelAttribute="editRestaurantForm"
	method="post" enctype="multipart/form-data">
	<div class="row">

		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.name" path="name" />
		</div>
		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.surname" path="surname" />
		</div>
		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.cif" path="cif" />
		</div>
		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.email" path="email" />
		</div>
		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.phone" path="phone" />
		</div>
		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.address" path="address" />
		</div>
		<div class="col-xs-12 form-group">
			<acme:textbox code="restaurant.deliveryPrice" path="deliveryPrice" type="number" min="0" step="0.1"/>
		</div>
		<br>
	<div class="col-xs-12 form-group">
		<acme:textbox code="customer.photo" path="file" type="file" accept="image/*"/>
	</div>
		<br>
		<br>
		<div class="col-xs-12 form-group">
			<acme:textarea code="restaurant.description" path="description"   />
		</div>

		<br>
		
		<div class="col-xs-12 form-group">

			<acme:submit name="save" code="row.save" />
			<acme:cancel code="row.cancel" url="" />
		</div>
	</div>
</form:form>
