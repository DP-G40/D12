<%@page import="com.lowagie.tools.plugins.AbstractTool.Console"%>
<%@ page import="java.util.Date" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ page import="domain.Banner" %>

<!-- Atacamos a una propiedad derivada para que pueda sacar el code64 y poder mostrar la img-->
    <% Banner b1 = (Banner) pageContext.getAttribute("bannerAnnouncement", PageContext.REQUEST_SCOPE);
       String imgString = "";
       try{
       		imgString = b1.getImageCode64();
       }catch(Exception e){
    	   
       }
    %>
    
<c:if test="${not empty bannerAnnouncement}">
	<br>   <br>
        <img src="data:image/jpeg;base64,<%=imgString%>" title="banner.jpeg">
    <br>   <br>
</c:if>

<form:form action="restaurant/search.do" modelAttribute="restaurant">

    <input type="search" name="keyword">&nbsp;
    <spring:message code="standard.search"/>

    <input type="submit" name="search"
           value="<spring:message code="standard.search" />"/>

</form:form>






<security:authentication property="principal" var="principal"/>


    <h2><spring:message code="restaurant.list"/></h2>
    <display:table name="restaurant" id="row" requestURI="${requestURI}">
    
        <display:column property="name" titleKey="restaurant.name" sortable="true"/>
         <display:column property="description" titleKey="restaurant.description" />
        
		<display:column titleKey="restaurant.view">            
        	<acme:cancel code="restaurant.view" url="/restaurant/view.do?restaurantId=${row.id}"/>
        </display:column>
        
	
	
    </display:table>