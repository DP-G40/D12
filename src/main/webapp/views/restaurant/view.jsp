<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="domain.Banner" %>


<!-- Atacamos a una propiedad derivada para que pueda sacar el code64 y poder mostrar la img-->
    <% Banner b1 = (Banner) pageContext.getAttribute("bannerAnnouncement", PageContext.REQUEST_SCOPE);
       String imgString = "";
       try{
       		imgString = b1.getImageCode64();
       }catch(Exception e){
    	   
       }
    %>
    
<c:if test="${not empty bannerAnnouncement}">
	<br>   <br>
        <img src="data:image/jpeg;base64,<%=imgString%>" title="banner.jpeg">
    <br>   <br>
</c:if>


<security:authentication property="principal" var="principal"/>



<h1>${restaurant.name}</h1>
<div class="table">
    <table class="restaurant_ver">
        <tr>
            <td><b><spring:message code="restaurant.description"></spring:message></b>: ${restaurant.description}</td>      
        </tr>
        <tr>
            <td><b><spring:message code="restaurant.direction"></spring:message></b>: ${restaurant.address}</td>
        </tr>
        <tr>
            <td><b><spring:message code="restaurant.phoneNumber"></spring:message></b>: ${restaurant.phone}</td>
        </tr>
        <tr>
            <td><b><spring:message code="restaurant.email"></spring:message></b>: ${restaurant.email}</td>
        </tr>
        <tr>
            <td> <b>Tags:</b>
                <ul>
                    <jstl:forEach items="${restaurant.tags}" var="tag">
                        <li>${tag.name}</li>
                    </jstl:forEach>
                </ul>
            </td>

        </tr>

    </table>
</div>
<jstl:if test="${principal != 'anonymousUser'}">
    <jstl:if test="${principal.id==restaurant.userAccount.id}">
        <div>
            <acme:cancel code="restaurant.tag.addtag" url="tag/restaurant/addtag.do"></acme:cancel>
            <acme:cancel code="restaurant.tag.removetag" url="tag/restaurant/removetag.do"></acme:cancel>
        </div>

    </jstl:if>
</jstl:if>







<h2><spring:message code="restaurant.schedules"></spring:message></h2>
<display:table name="schedules" id="schedule" requestURI="${requestURI}">

    <display:column property="opening" titleKey="schedule.opening"/>

    <display:column property="close" titleKey="schedule.close"/>

    <display:column property="day" titleKey="schedule.day"/>

</display:table>

<jstl:if test="${principal != 'anonymousUser'}">
    <jstl:if test="${principal.id==restaurant.userAccount.id}">
        <div>
            <acme:cancel code="restaurant.createschedule" url="schedule/restaurant/create.do"></acme:cancel>

        </div>

    </jstl:if>
</jstl:if>




<h2><spring:message code="restaurant.postalAddress"></spring:message></h2>

<display:table name="postals" id="postal" requestURI="${requestURI}">

    <display:column property="postal" titleKey="postal.postal"/>
    <display:column property="price" titleKey="postal.price"/>

    <jstl:if test="${principal != 'anonymousUser'}">
        <jstl:if test="${principal.id==restaurant.userAccount.id}">
            <display:column>
                <acme:cancel code="button.edit" url="postal/restaurant/edit.do?postalId=${postal.id}"></acme:cancel>

            </display:column>
            <display:column>
                <acme:cancel code="button.delete" url="postal/restaurant/delete.do?postalId=${postal.id}"></acme:cancel>
            </display:column>
        </jstl:if>
    </jstl:if>

</display:table>


<jstl:if test="${principal != 'anonymousUser'}">
    <jstl:if test="${principal.id==restaurant.userAccount.id}">
        <acme:cancel code="button.create" url="postal/restaurant/create.do"></acme:cancel>

    </jstl:if>
</jstl:if>













<h2><spring:message code="restaurant.dishes"></spring:message></h2>
<display:table name="dishes" id="row"   pagesize="60"  requestURI="${requestUri}" class="displaytag">

    <display:column titleKey="category.category" property="category.name" sortable="true"/>
    <display:column titleKey="standard.name" property="name"/>
    <display:column titleKey="dish.price">${row.price} &euro;</display:column>
    <display:column titleKey="restaurant.dish.moreinfo"><acme:cancel code="restaurant.dish.moreinfo" url="dish/view.do?dishId=${row.id}" ></acme:cancel></display:column>
    <security:authorize access="hasAnyRole('CUSTOMER','EVALUATOR')">
        <display:column titleKey="restaurant.dish.buy"><acme:cancel code="restaurant.dish.buy" url="order/customer/add.do?dishId=${row.id}" ></acme:cancel></display:column>
    </security:authorize>


</display:table>









<h2><spring:message code="restaurant.comments"></spring:message></h2>
<display:table name="comments" id="row" requestURI="${requestURI}">

    <display:column property="reviewer.name" titleKey="comments.username" />
    <display:column property="title" titleKey="comments.title" />
    <display:column property="description" titleKey="comments.description" />
    <display:column property="mark" titleKey="comments.mark" />

    <security:authorize access="hasRole('ADMIN')">
        <display:column titleKey="standard.delete">
            <acme:cancel code="standard.delete" url="comment/administrator/delete.do?commentId=${row.id}"/>
        </display:column>
    </security:authorize>
</display:table>

<security:authorize access="hasAnyRole('CUSTOMER','EVALUATOR')">
        <acme:cancel code="comment.create" url="comment/customer/comment.do?commentableId=${restaurant.id}" />
</security:authorize>