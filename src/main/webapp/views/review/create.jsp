<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="review/evaluator/create.do" modelAttribute="reviewForm">

    <acme:select path="order" code="review.order" items="${orders}" itemLabel="ticker"/>
    <br/>
    <acme:textbox code="review.time" path="time" type="number" step="0.1" min="0"/>
    <acme:textbox code="review.mark" path="mark" type="number" min="0" max="10" step="1"/>
    <acme:textarea code="review.evaluation" path="evaluation" />
    <br/>
    <br/>






    <tr>
        <td colspan="3">
            <acme:submit name="save" code="row.save"/>
            <acme:cancel code="row.cancel" url="/review/evaluator/list.do" />

        </td>
    </tr>

</form:form>
