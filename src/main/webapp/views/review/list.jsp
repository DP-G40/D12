<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>




<display:table name="reviews" id="review" requestURI="${requestURI}">

    <display:column property="code" titleKey="review.code"/>

    <display:column property="order.restaurant.name" titleKey="review.restaurant"/>

    <display:column property="evaluation" titleKey="review.evaluation"/>

    <display:column property="time" titleKey="review.time"/>

    <display:column property="mark" titleKey="review.mark"/>


</display:table>
<br/>
<br/>
<security:authorize access="hasRole('EVALUATOR')">
<acme:cancel code="review.create" url="/review/evaluator/create.do"/>
</security:authorize>





