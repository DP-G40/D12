<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<br><br>
<form:form action="${actionURL}" modelAttribute="schedule" method="post">
	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="restaurant"/>
	
    <div class="row">
        <div class="col-xs-12 form-group">
            <acme:textbox path="opening" code="schedule.opening.form"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="close" code="schedule.close.form"/>
        </div>
        <div class="col-xs-12 form-group">
            <spring:message code="schedule.day.form"/>
            <form:select path="day">
                <form:option label="MONDAY" value="MONDAY" />
                <form:option label="THURSDAY" value="THURSDAY" />
                <form:option label="WEDNESDAY" value="WEDNESDAY" />
                <form:option label="TUESDAY" value="TUESDAY" />
                <form:option label="FRIDAY" value="FRIDAY" />
                <form:option label="SATURDAY" value="SATURDAY" />
                <form:option label="SUNDAY" value="SUNDAY"/>
            </form:select>
        </div>

        <br><br>
        <div class="col-xs-12 form-group">
            <acme:submit name="save" code="button.save"/>
            <acme:cancel code="button.cancel" url=""/>
        </div>
    </div>
</form:form>


