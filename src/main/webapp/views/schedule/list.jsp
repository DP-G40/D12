<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<display:table name="schedules" id="schedule" requestURI="${requestURI}">

    <display:column property="opening" titleKey="schedule.opening"/>

    <display:column property="close" titleKey="schedule.close"/>
    
    <display:column property="day" titleKey="schedule.day"/>

	<display:column><a href="schedule/restaurant/edit.do?scheduleId=${schedule.id}"><spring:message code="button.edit"/></a></display:column>
	<display:column><a href="schedule/restaurant/delete.do?scheduleId=${schedule.id}"><spring:message code="button.delete"/></a></display:column>

</display:table>


<security:authorize access="hasRole('RESTAURANT')">
    <a href="schedule/restaurant/create.do"><spring:message code="button.create"/></a>
</security:authorize>




