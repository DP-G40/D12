<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty errorMessage}">
	<div class="resaltar">
    	${errorMessage}
    </div>
</c:if>

<h2><spring:message	code="templates.all" /></h2><br>
<display:table name="templates" id="template" requestURI="template/administrator/all.do">

    <display:column titleKey="template.title" property="title"/>
    <display:column titleKey="template.body" property="body"/>
	
	<!-- Formulario para editar cada plantilla -->
	
	<display:column>
	<c:set var="template" value="${template}" scope="request" />
	
		<form:form action="template/administrator/save.do" modelAttribute="template" method="post">
			<form:hidden path="id"/>
			<form:hidden path="version"/>
			<form:hidden path="code"/>
		
		    <div class="row">
		        <div class="col-xs-12 form-group">
		            <acme:textbox path="title" code="template.title"/>
		        </div>
		        <div class="col-xs-12 form-group">
		            <acme:textbox path="body" code="template.body"/>
		        </div>
		        
		        <br>
		        <div class="col-xs-12 form-group">
		            <acme:submit name="save" code="button.save"/>
		        </div>
		    </div>
		</form:form>
		
	</display:column>
	
</display:table>

