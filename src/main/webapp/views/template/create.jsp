<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${action}" modelAttribute="templateForm">

    <acme:textbox code="template.title" path="title"></acme:textbox>
    <acme:textbox code="template.body" path="body"></acme:textbox>

    <br>
    <br>
    <spring:message code="template.type" />
    <form:select path="type">
        <form:option label="-" value="0" />
        <form:option label="COMPLETED" value="COMPLETED" />
        <form:option label="DELIVERING" value="DELIVERING" />
        <form:option label="REJECTED" value="REJECTED" />
        <form:option label="COOKING" value="COOKING" />
    </form:select>
    <form:errors cssClass="error" path="type"/>

    <br>
    <br>

    <tr>
        <td colspan="3">
            <acme:submit name="save" code="row.save"/>
            <acme:cancel code="row.cancel" url="template/administrator/all.do" />
        </td>
    </tr>

</form:form>
