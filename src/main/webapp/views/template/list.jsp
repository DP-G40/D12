<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<display:table name="templates" id="template" requestURI="template/administrator/all.do">

	<display:column titleKey="template.type" property="type"></display:column>
    <display:column titleKey="template.title" property="title"/>
    <display:column titleKey="template.body" property="body"/>
	<display:column titleKey="template.status">
		<jstl:choose>
			<jstl:when test="${template.active==true}">
				<spring:message code="template.active"></spring:message>
			</jstl:when>
			<jstl:otherwise>
				<acme:cancel code="template.activar" url="template/administrator/activateTemplate.do?templateId=${template.id}"></acme:cancel>
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	


</display:table>

<acme:cancel code="template.create" url="template/administrator/create.do" />
