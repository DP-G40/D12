package usecases;

import domain.*;
import forms.OfferForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.Date;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AdminServiceTest extends AbstractTest {

    private static Restaurant restaurant;

    @Autowired
    private EvaluatorService evaluatorService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AllegenService allegenService;

    @Autowired
    private TemplateService templateService;

    @Override
    public void setUp() {
        startTransaction();
        authenticate("restaurant1");
        restaurant = (Restaurant) actorService.findByPrincipal();
        unauthenticate();
    }

    @Override
    public void tearDown() {
        rollbackTransaction();
    }


    public void registerAsEvaluatorTest(String name, String surname, String email, String phone, String username, String password, String address, Class<?> expected) {
        Class<?> caught = null;
        try {
            unauthenticate();
            Evaluator evaluator = evaluatorService.create();
            evaluator.setName(name);
            evaluator.setSurname(surname);
            evaluator.setEmail(email);
            evaluator.setPhone(phone);
            evaluator.setAddress(address);
            evaluator.getUserAccount().setUsername(username);
            evaluator.getUserAccount().setPassword(new Md5PasswordEncoder().encodePassword(password, null));

            evaluatorService.save(evaluator);
            this.flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test()
    public void registerAsEvaluatorTestDriver() {
        Object[][] cases = {{"name", "surname", "email@mail.com", "+34 666666666", "username1", "password", "address", null},
                {"name", "surname", "email@mail.com", "+34 666666666", "", "password", "address", ConstraintViolationException.class},
                {"name", "surname", "email@mail.com", "+34 666666666", "username3", null, "address", ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            registerAsEvaluatorTest((String) cases[i][0], (String) cases[i][1], (String) cases[i][2], (String) cases[i][3], (String) cases[i][4], (String) cases[i][5], (String) cases[i][6], (Class<?>) cases[i][7]);
        }
    }

    public void editConfigurationTest(Double systemFee, Double bannerFee, Double videoFee, Class<?> expected) {
        Class<?> caught = null;
        try {
            Configuration configuration = configurationService.findAll().iterator().next();
            configuration.setSystemPrice(systemFee);
            configuration.setBannerPrice(bannerFee);
            configuration.setVideoPrice(videoFee);

            configurationService.save(configuration);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void editConfigurationDriver() {
        Object[][] cases = {{1.0, 1.0, 1.0, null},
                {1.0, null, 10.0, ConstraintViolationException.class},
                {1.0, 1.0, null, ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            editConfigurationTest((Double) cases[i][0], (Double) cases[i][1], (Double) cases[i][2], (Class<?>) cases[i][3]);
        }
    }

    public void createPromotionTest(String restaurant, String title, String description, Date startDate, Date endDate, Integer discount, Class<?> expected) {
        Class<?> caught = null;
        try {
            authenticate(restaurant);
            OfferForm offer= new OfferForm();
            offer.setTitle(title);
            offer.setDescription(description);
            offer.setStartDate(startDate);
            offer.setEndDate(endDate);
            offer.setDiscount(discount);
            offerService.savePromotion(offer);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void createOfferDriver() {
        Object[][] cases = {{"restaurant1", "title", "description", new Date(), new Date(), 10, null},
                {"", "title", "description", new Date(), new Date(), 10, IllegalArgumentException.class},
                {"restaurant1", "title", "description", new Date(), new Date(), -1, ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            createPromotionTest((String) cases[i][0], (String) cases[i][1], (String) cases[i][2], (Date) cases[i][3], (Date) cases[i][4], (Integer) cases[i][5], (Class<?>) cases[i][6]);
        }
    }

    public void banRestaurantTest(Restaurant restaurant,Class<?> expected){
        Class<?> caught = null;
        try {
            restaurantService.banRestaurant(restaurant.getId());
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void banRestaurantDriver() {
        Object[][] cases = {{restaurant,null},
                {null,NullPointerException.class}};

        for (int i = 0; i < cases.length; i++) {
            banRestaurantTest((Restaurant) cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    public void unbanRestaurantTest(Restaurant restaurant,Class<?> expected){
        Class<?> caught = null;
        try {
            restaurantService.unbanRestaurant(restaurant.getId());
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void unbanRestaurantDriver() {
        Object[][] cases = {{restaurant,null},
                {null,NullPointerException.class}};

        for (int i = 0; i < cases.length; i++) {
            unbanRestaurantTest((Restaurant) cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    public void manageCategoryTest(String name1,String name2,Class<?> expected){
        Class<?> caught = null;
        try {
            Category category = categoryService.create();
            category.setName(name1);
            category = categoryService.save(category);
            flushTransaction();
            Category category1 = categoryService.create(category.getId());
            category1.setName(name2);
            category1 = categoryService.save(category1);
            flushTransaction();
            categoryService.delete(category1);
            flushTransaction();
            category.setName(name2);
            categoryService.save(category);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void manageCategoryDriver(){
        Object[][] cases = {{"name1","name2",null},
                {null,"name2",ConstraintViolationException.class},
                {"name1",null,ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            manageCategoryTest((String) cases[i][0],(String) cases[i][1],(Class<?>) cases[i][2]);
        }
    }

    public void manageAllergenTest(String name,Class<?> expected){
        Class<?> caught = null;
        try {
            Allergen allergen = allegenService.create();
            allergen.setName(name);
            allergen = allegenService.save(allergen);
            flushTransaction();
            allegenService.delete(allergen);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void manageAllergenDriver(){
        Object[][] cases = {{"name1",null},
                {"",ConstraintViolationException.class},
                {null,ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            manageAllergenTest((String) cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    public void manageTagTest(String name,Class<?> expected){
        Class<?> caught = null;
        try {
            Tag tag = tagService.create();
            tag.setName(name);
            tag = tagService.save(tag);
            flushTransaction();
            tagService.delete(tag);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void manageTagDriver(){
        Object[][] cases = {{"name1",null},
                {"",ConstraintViolationException.class},
                {null,ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            manageTagTest((String) cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    public void manageTemplateTest(String body,String title,String type,Class<?> expected){
        Class<?> caught = null;
        try {
            Template template = templateService.create();
            template.setBody(body);
            template.setTitle(title);
            template.setType(type);
            template.setActive(true);
            template = templateService.save(template);
            flushTransaction();
            template.setActive(false);
            templateService.save(template);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void manageTemplateDriver(){
        Object[][] cases = {{"body","title","COMPLETED",null},
                {"","title","COMPLETED",ConstraintViolationException.class},
                {"body","title","type",ConstraintViolationException.class},};

        for (int i = 0; i < cases.length; i++) {
            manageTemplateTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(Class<?>) cases[i][3]);
        }
    }
}
