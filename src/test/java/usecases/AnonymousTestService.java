package usecases;

import domain.Customer;
import domain.Restaurant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import services.CustomerService;
import services.RestaurantService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collection;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AnonymousTestService extends AbstractTest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private RestaurantService restaurantService;

    /**
     *
     * @param name
     * @param surname
     * @param email
     * @param phone
     * @param username
     * @param password
     * @param address
     * @param expected
     *
     * Test que prueba el registro como customer por parte de un usuario unautenticado
     * El caso positivo consiste en el registro con todos los datos correctos
     * El primer caso negativo simula el intento de registro sin aportar el nombre de usuario
     * El segundo caso negativo simula el intento de registro sin aportar la contraseņa
     */
    public void registerAsCustomerTestCase(String name,String surname,String email,String phone,String username,String password,String address,Class<?> expected){
        Class<?> caught = null;
        try{
            unauthenticate();
            Customer customer = customerService.create();
            customer.setName(name);
            customer.setSurname(surname);
            customer.setEmail(email);
            customer.setPhone(phone);
            customer.setAddress(address);
            customer.getUserAccount().setUsername(username);
            customer.getUserAccount().setPassword(new Md5PasswordEncoder().encodePassword(password,null));

            customerService.save(customer);
            this.flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test()
    public void registerAsCustomerTestDriver(){
        Object[][]cases={{"name","surname","email@mail.com","+34 666666666","username1","password","address",null},
                {"name","surname","email@mail.com","+34 666666666","","password","address",ConstraintViolationException.class},
                {"name","surname","email@mail.com","+34 666666666","username3","password","address",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            registerAsCustomerTestCase((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(String) cases[i][4],(String) cases[i][5],(String) cases[i][6],(Class<?>) cases[i][7]);
        }
    }

    /**
     *
     * @param name
     * @param surname
     * @param email
     * @param phone
     * @param username
     * @param password
     * @param expected
     *
     * Test que prueba el registro como customer por parte de un usuario unautenticado
     * El caso positivo consiste en el registro con todos los datos correctos
     * El primer caso negativo simula el intento de registro sin aportar el nombre de usuario
     * El segundo caso negativo simula el intento de registro sin aportar la contraseņa
     */
    public void registerAsRestaurantTestCase(String name,String surname,String email,String phone,String username,String password,String address,String cif,String description,Double deliveryPrice,Class<?> expected){
        Class<?> caught = null;
        try{
            unauthenticate();
            Restaurant restaurant = restaurantService.create();
            restaurant.setName(name);
            restaurant.setSurname(surname);
            restaurant.setEmail(email);
            restaurant.setPhone(phone);
            restaurant.setAddress(address);
            restaurant.getUserAccount().setUsername(username);
            restaurant.getUserAccount().setPassword(new Md5PasswordEncoder().encodePassword(password,null));
            restaurant.setCIF(cif);
            restaurant.setDeliveryPrice(deliveryPrice);
            restaurant.setDescription(description);

            restaurantService.save(restaurant);
            this.flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test()
    public void registerAsRestaurantTestDriver(){
        Object[][]cases={{"name","surname","email@mail.com","+34 666666666","username1","password","address","D63821375","description",10.0,null},
                {"name","surname","email@mail.com","+34 666666666","","password","address","D63821375","description",10.0,ConstraintViolationException.class},
                {"name","surname","email@mail.com","+34 666666666","username3","password","address","D63821375","description",10.0,ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            registerAsRestaurantTestCase((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(String) cases[i][4],(String) cases[i][5],(String) cases[i][6],(String) cases[i][7],(String) cases[i][8],(Double) cases[i][9],(Class<?>) cases[i][10]);
        }
    }

    @Override
    public void setUp(){
        startTransaction();
    }

    @Override
    public void tearDown() {
        rollbackTransaction();
    }

}
