package usecases;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CustomerServiceTest extends AbstractTest {

    private static Dish dish;

    private static Category category;

    private static Order order;

    private static Restaurant restaurant;

    private static Creditcard creditCard;

    @Autowired
    private DishService dishService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private CommentService commentService;


    @Override
    public void setUp(){
        startTransaction();
        Category cat = new Category();
        cat.setName("category");
        category = categoryService.save(cat);
        flushTransaction();
        authenticate("restaurant3");
        restaurant = (Restaurant) actorService.findByPrincipal();
        Dish d = dishService.create();
        d.setName("dish");
        d.setDescription("description");
        d.setPrice(10.0);
        d.setCategory(category);
        dish = dishService.save(d);
        flushTransaction();
        authenticate("customer1");
        Order o = orderService.create(restaurant);
        o.getDishes().add(dish);
        order = orderService.save(o);
        unauthenticate();
        flushTransaction();
        creditCard = new Creditcard();
        creditCard.setHolderName("holder");
        creditCard.setBrand("brand");
        creditCard.setNumber("4027240459748441");
        creditCard.setCvv(100);
        creditCard.setExpiryMonth(12);
        creditCard.setExpiryYear(2020);
    }

    @Override
    public void tearDown() {
        rollbackTransaction();
    }



    public void addDishToOrderTest(String customer,Dish dish,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(customer);
            orderService.addDishToOrder(dish.getId());
            flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void addDishToOrderDriver(){
        Object[][]cases={{"customer1",dish,null},
                {"",dish,IllegalArgumentException.class},
                {"customer1",null,NullPointerException.class}};

        for(int i = 0; i < cases.length;i++){
            addDishToOrderTest((String) cases[i][0],(Dish) cases[i][1],(Class<?>) cases[i][2]);
        }
    }

    public void createCommentTest(String customer,Commentable commentable,String title,String description,Integer mark,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(customer);
            Comment comment = commentService.create();
            comment.setTitle(title);
            comment.setDescription(description);
            comment.setMark(mark);
            comment.setCommentable(commentable);
            commentService.save(comment);
            flushTransaction();
        }catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }


    @Test
    public void createCommentDriver(){
        Object[][]cases={{"customer1",dish,"title","description",1,null},
                {"customer1",restaurant,"title","description",1,null},
                {"customer1",null,"title","description",1,DataIntegrityViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            createCommentTest((String) cases[i][0],(Commentable) cases[i][1],(String) cases[i][2],(String) cases[i][3],(Integer) cases[i][4],(Class<?>) cases[i][5]);
        }
    }

}
