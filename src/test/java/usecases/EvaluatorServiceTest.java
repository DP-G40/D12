package usecases;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class EvaluatorServiceTest extends AbstractTest {

    private static Order order;

    private static Category category;

    private Dish dish;

    private Restaurant restaurant;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishService dishService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private OrderService orderService;

    @Override
    public void setUp() {
        startTransaction();
        Category cat = new Category();
        cat.setName("category");
        category = categoryService.save(cat);
        flushTransaction();
        authenticate("restaurant1");
        restaurant = (Restaurant) actorService.findByPrincipal();
        Dish d = dishService.create();
        d.setName("dish");
        d.setDescription("description");
        d.setPrice(10.0);
        d.setCategory(category);
        dish = dishService.save(d);
        flushTransaction();
        authenticate("customer1");
        Order o = orderService.create(restaurant);
        o.getDishes().add(dish);
        order = orderService.save(o);
        unauthenticate();
        flushTransaction();
    }

    @Override
    public void tearDown() {
        rollbackTransaction();
    }

    public void createReviewTest(String evaluator, Order order, String evaluation, Double time, Integer mark, Class<?> expected) {
        Class<?> caught = null;
        try {
            authenticate(evaluator);
            Review review = reviewService.create();
            review.setOrder(order);
            review.setTime(time);
            review.setEvaluation(evaluation);
            review.setMark(mark);
            reviewService.save(review);
            unauthenticate();
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void createReviewDriver(){
        Object[][] cases = {{"evaluator1", order, "evaluation", 1.0, 10, null},
                {"evaluator1", order, "evaluation", 1.0, 11, ConstraintViolationException.class},
                {"evaluator1", order, "evaluation", -1.0, 10, ConstraintViolationException.class},};

        for (int i = 0; i < cases.length; i++) {
            createReviewTest((String) cases[i][0], (Order) cases[i][1], (String) cases[i][2], (Double) cases[i][3], (Integer) cases[i][4], (Class<?>) cases[i][5]);
        }
    }
}
