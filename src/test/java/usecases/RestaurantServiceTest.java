package usecases;

import domain.Category;
import domain.DeliveryMan;
import domain.Dish;
import domain.Offer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.Date;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class RestaurantServiceTest extends AbstractTest {

    private static Category category;

    private static DeliveryMan deliveryMan;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishService dishService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private DeliveryManService deliveryManService;

    @Autowired
    private ActorService actorService;


    @Override
    public void setUp() {
        startTransaction();
        Category cat = new Category();
        cat.setName("category");
        category = categoryService.save(cat);
        flushTransaction();
        authenticate("deliveryMan1");
        deliveryMan = (DeliveryMan) actorService.findByPrincipal();
        unauthenticate();
    }

    @Override
    public void tearDown() {
        rollbackTransaction();
    }

    public void createDishTest(String restaurant, String name, String description, Double price, Category category, Class<?> expected) {
        Class<?> caught = null;
        try {
            authenticate(restaurant);
            Dish d = dishService.create();
            d.setName(name);
            d.setDescription(description);
            d.setPrice(price);
            d.setCategory(category);
            dishService.save(d);
            unauthenticate();
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void createDishDriver() {
        Object[][] cases = {{"restaurant1", "name", "description", 10.0, category, null},
                {"", "name", "description", 10.0, category, IllegalArgumentException.class},
                {"restaurant1", "name", "description", -1.0, category, ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            createDishTest((String) cases[i][0], (String) cases[i][1], (String) cases[i][2], (Double) cases[i][3], (Category) cases[i][4], (Class<?>) cases[i][5]);
        }
    }


    public void createOfferTest(String restaurant, String title, String description, Date startDate, Date endDate, Integer discount, Date creationDate, Class<?> expected) {
        Class<?> caught = null;
        try {
            authenticate(restaurant);
            Offer offer = offerService.create();
            offer.setTitle(title);
            offer.setDescription(description);
            offer.setStartDate(startDate);
            offer.setEndDate(endDate);
            offer.setDiscount(discount);
            offer.setCreationDate(creationDate);
            offerService.save(offer);
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void createOfferDriver() {
        Object[][] cases = {{"restaurant1", "title", "description", new Date(), new Date(), 10, new Date(), null},
                {"", "title", "description", new Date(), new Date(), 10, new Date(), IllegalArgumentException.class},
                {"restaurant1", "title", "description", new Date(), new Date(), -1, new Date(), ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            createOfferTest((String) cases[i][0], (String) cases[i][1], (String) cases[i][2], (Date) cases[i][3], (Date) cases[i][4], (Integer) cases[i][5], (Date) cases[i][6], (Class<?>) cases[i][7]);
        }
    }

    public void registerDeliveryManTest(String restaurant, String name, String surname, String email, String phone, String username, String password, String address, Class<?> expected) {
        Class<?> caught = null;
        try {
            authenticate(restaurant);
            DeliveryMan deliveryMan = deliveryManService.create();
            deliveryMan.setName(name);
            deliveryMan.setSurname(surname);
            deliveryMan.setEmail(email);
            deliveryMan.setPhone(phone);
            deliveryMan.setAddress(address);
            deliveryMan.getUserAccount().setUsername(username);
            deliveryMan.getUserAccount().setPassword(new Md5PasswordEncoder().encodePassword(password, null));
            deliveryManService.save(deliveryMan);
            unauthenticate();
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void registerAsCustomerTestDriver() {
        Object[][] cases = {{"restaurant1", "name", "surname", "email@mail.com", "+34 666666666", "username1", "password", "address", null},
                {"restaurant1", "name", "surname", "email@mail.com", "+34 666666666", "", "password", "address", ConstraintViolationException.class},
                {"", "name", "surname", "email@mail.com", "+34 666666666", "username3", "password", "address", ConstraintViolationException.class}};

        for (int i = 0; i < cases.length; i++) {
            registerDeliveryManTest((String) cases[i][0], (String) cases[i][1], (String) cases[i][2], (String) cases[i][3], (String) cases[i][4], (String) cases[i][5], (String) cases[i][6], (String) cases[i][7], (Class<?>) cases[i][8]);
        }
    }

    public void firedDeliveryManTest(String restaurant, DeliveryMan deliveryMan, Class<?> expected) {
        Class<?> caught = null;
        try {
            authenticate(restaurant);
            deliveryManService.fired(deliveryMan.getId());
            flushTransaction();
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected, caught);
    }

    @Test
    public void firedDeliveryManDriver() {
        Object[][] cases = {{"restaurant1", deliveryMan, null},
                {"", deliveryMan, IllegalArgumentException.class},
                {"restaurant1", null, NullPointerException.class}};

        for (int i = 0; i < cases.length; i++) {
            firedDeliveryManTest((String) cases[i][0], (DeliveryMan) cases[i][1], (Class<?>) cases[i][2]);
        }
    }


}
